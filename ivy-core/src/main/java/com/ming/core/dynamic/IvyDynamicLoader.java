package com.ming.core.dynamic;

import com.ming.core.dynamic.spring.core.ClassInfo;
import com.ming.core.dynamic.spring.core.MemoryClassLoader;
import com.ming.core.dynamic.spring.util.SpringBeanUtil;
import groovy.lang.GroovyClassLoader;

public class IvyDynamicLoader {

    /**
     * 动态编译class并注册到spring容器中
     * @param javaCode
     * @return
     */
    public static <T> Class<? extends T> registerSpring(String javaCode){
        MemoryClassLoader loader = new MemoryClassLoader();
        try {
            ClassInfo classInfo = loader.registerJava(null,javaCode);

            GroovyClassLoader groovyClassLoader = new GroovyClassLoader();
            Class clazz = groovyClassLoader.parseClass(javaCode);

            //Class<? extends T> clazz = (Class<? extends T>) loader.findClass(classInfo.getFullClassName());
            SpringBeanUtil.replace(classInfo.getBeanName(), clazz);
            return clazz;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static <T> ClassInfo registerSpringClassInfo(String beanName,String javaCode){
        MemoryClassLoader loader = new MemoryClassLoader();
        try {
            ClassInfo classInfo = loader.registerJava(beanName, javaCode);
            //Class<? extends T> clazz = (Class<? extends T>) loader.findClass(classInfo.getFullClassName());
            GroovyClassLoader groovyClassLoader = new GroovyClassLoader();
            Class clazz = groovyClassLoader.parseClass(javaCode);

            SpringBeanUtil.replace(classInfo.getBeanName(), clazz);
            classInfo.setClazz(clazz);
            return classInfo;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 动态构建组件
     * @param
     * @return
     */
    public static void registerComponent(){

    }

    /**
     * 动态加载jar
     * @param jarPath
     * @return
     */
    public static void loadJar(String jarPath){

    }

}
