package com.ming.core.dynamic.beetlsql;

import org.beetl.core.fun.MethodInvoker;
import org.beetl.core.fun.ObjectUtil;

public class BaseEntity {

    public BaseEntity() {
    }

    public void setValue(String attrName, Object value) {
        MethodInvoker methodInvoker = ObjectUtil.getInvokder(this.getClass(), attrName);
        if (methodInvoker == null) {
            throw new IllegalArgumentException("不存在的属性 " + attrName);
        } else {
            methodInvoker.set(this, value);
        }
    }

    public Object getValue(String attrName) {
        MethodInvoker methodInvoker = ObjectUtil.getInvokder(this.getClass(), attrName);
        if (methodInvoker == null) {
            throw new IllegalArgumentException("不存在的属性 " + attrName);
        } else {
            return methodInvoker.get(this);
        }
    }

    public String getStrValue(String attrName) {
        Object val = getValue(attrName);
        return val == null ? null : String.valueOf(val);
    }
    public Long getLongValue(String attrName) {
        String val = getStrValue(attrName);
        return val == null ? null : Long.valueOf(val);
    }

    public Integer getIntValue(String attrName) {
        String val = getStrValue(attrName);
        return val == null ? null : Integer.valueOf(val);
    }

    public Boolean getBooleanValue(String attrName) {
        String val = getStrValue(attrName);
        return val == null ? null : Boolean.valueOf(val);
    }
}
