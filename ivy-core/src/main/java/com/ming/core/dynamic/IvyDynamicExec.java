package com.ming.core.dynamic;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.StrUtil;
import com.ivy.builder.graph.IvyCmp;
import com.ivy.builder.graph.IvyDynamicClass;
import com.ivy.builder.graph.Node;
import com.ivy.builder.graph.NodeProperties;
import com.ming.core.dynamic.spring.core.ClassInfo;
import com.ming.core.dynamic.spring.core.ExecInfo;
import com.ming.core.liteflow.entity.IvyChain;
import com.ming.core.liteflow.executor.FlowExecutorUtil;
import com.ming.core.liteflow.node.IvyLiteFlowNodeBuilder;
import com.ming.core.log.LogInit;
import com.ming.core.parser.bus.*;
import com.yomahub.liteflow.core.FlowExecutor;
import com.yomahub.liteflow.enums.ScriptTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class IvyDynamicExec {

    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicExec.class);

    public static ExecInfo execComponent(IvyDynamicClass dynamicClass, String el) {
        long ms = System.currentTimeMillis();
        String startKey = ms + LogInit.startKey;
        String endKey = ms + LogInit.endKey;
        LOG.info(startKey);

        el = ivyLiteFlowNodeBuilderNode(null, dynamicClass, StrUtil.isBlank(el), el);

        FlowExecutor flowExecutor = FlowExecutorUtil.getFlowExecutor();

        String returnVal = FlowExecutorUtil.executor(flowExecutor, el, null);

        LOG.info(endKey);
        List<String> list = LogInit.logMap.get(endKey);
        while (list == null || list.isEmpty()) {
            list = LogInit.logMap.get(endKey);// 等待list不为空
            ThreadUtil.sleep(10);
        }
        LogInit.logMap.remove(endKey);
        return ExecInfo.builder().returnVal(returnVal).el(el).logStr(String.join(System.lineSeparator(),list)).build();
    }


    public static ExecInfo execClass(String sourceCode, String methodName, Object[] params) throws Exception {
        long ms = System.currentTimeMillis();
        String startKey = ms + LogInit.startKey;
        String endKey = ms + LogInit.endKey;
        LOG.info(startKey);

        ClassInfo classInfo = IvyDynamicLoader.registerSpringClassInfo(null, sourceCode);
        Object obj = classInfo.getClazz().newInstance();
        Object returnVal = null;
        try {
            Class<?>[] parameterTypes = Arrays.stream(params).map(Object::getClass).collect(Collectors.toList()).toArray(new Class<?>[]{});
            Method method = obj.getClass().getMethod(methodName, parameterTypes);
            method.setAccessible(true);
            returnVal = method.invoke(obj, params);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            throw new RuntimeException(e.getMessage(), e);
        }

        LOG.info(endKey);

        List<String> list = LogInit.logMap.get(endKey);
        while (list == null || list.isEmpty()) {
            list = LogInit.logMap.get(endKey);// 等待list不为空
            ThreadUtil.sleep(100);
        }
        LogInit.logMap.remove(endKey);
        return ExecInfo.builder().returnVal(returnVal != null ? String.valueOf(returnVal) : "null").logStr(String.join(System.lineSeparator(),list)).build();
    }

    public static ExecInfo execEL(IvyCmp info, List<IvyCmp> ivyCmpList, List<IvyDynamicClass> ivyDynamicClassList) {
        String el = info.getEl();
        return execEL(el,ivyCmpList,ivyDynamicClassList);
    }

    public static ExecInfo execEL(String el,List<IvyCmp> ivyCmpList,List<IvyDynamicClass> ivyDynamicClassList) {
        return execEL(el,ivyCmpList,ivyDynamicClassList,null,null,null);
    }

    public static ExecInfo execEL(String el, List<IvyCmp> ivyCmpList, List<IvyDynamicClass> ivyDynamicClassList, List<Node> flowNodeList,List<Node> fallbackList, Object param, Object... contextBeanArray) {
        long ms = System.currentTimeMillis();
        String startKey = ms + LogInit.startKey;
        String endKey = ms + LogInit.endKey;
        LOG.info(startKey);

        List<String> componentIds = getComponentIds(el);
        HashSet<String> componentIdSet = new HashSet<>(componentIds);
        Map<String, IvyCmp> cmpMap = ivyCmpList.stream().collect(Collectors.toMap(IvyCmp::getComponentId, x -> x));
        Map<String, IvyDynamicClass> dynamicClassMap = ivyDynamicClassList.stream().collect(Collectors.toMap(IvyDynamicClass::getClassId, x -> x));

        Map<String, NodeProperties> newCmpMap = null;
        if(CollUtil.isNotEmpty(flowNodeList)){
            newCmpMap = flowNodeList.stream().collect(Collectors.toMap(m -> m.getProperties().getComponentId(), Node::getProperties));
        }
        for (String componentId : componentIdSet) {
            if(CollUtil.isNotEmpty(newCmpMap) && newCmpMap.containsKey(componentId)){
                IvyCmp ivyCmp = newCmpMap.get(componentId);
                String clazz = ivyCmp.getClazz();
                IvyDynamicClass ivyDynamicClass = dynamicClassMap.get(clazz);
                if(ivyDynamicClass != null) {
                    ivyLiteFlowNodeBuilderNode(ivyCmp, ivyDynamicClass, false, null);
                }else{
                    ivyLiteFlowNodeBuilderScript(ivyCmp);
                    ivyLiteFlowNodeBuilderNode(ivyCmp);
                }
            }else if(cmpMap.containsKey(componentId)) {
                IvyCmp ivyCmp = cmpMap.get(componentId);
                String clazz = ivyCmp.getClazz();
                IvyDynamicClass ivyDynamicClass = dynamicClassMap.get(clazz);
                if(ivyDynamicClass != null) {
                    ivyLiteFlowNodeBuilderNode(ivyCmp, ivyDynamicClass, false, null);
                }else{
                    ivyLiteFlowNodeBuilderScript(ivyCmp);
                }
            }else{
                LOG.error("组件ID:【{}】不存在", componentId);
            }
        }

        String returnVal = FlowExecutorUtil.executor(el,null);

        LOG.info(endKey);
        List<String> list = LogInit.logMap.get(endKey);
        while (list == null || list.isEmpty()) {
            list = LogInit.logMap.get(endKey);// 等待list不为空
            ThreadUtil.sleep(10);
        }
        LogInit.logMap.remove(endKey);
        return ExecInfo.builder().returnVal(returnVal).el(el).logStr(String.join(System.lineSeparator(),list)).build();
    }

    public static ExecInfo execChain(IvyChain chain) {

        return null;
    }

    public static String ivyLiteFlowNodeBuilderNode(IvyCmp ivyCmp, IvyDynamicClass dynamicClass, boolean flag, String el){
        String sourceCode = dynamicClass.getSourceCode();
        String cmpId = dynamicClass.getSourceCmpId();
        if(ivyCmp != null){
            cmpId = ivyCmp.getComponentId();
        }
        Integer classType = dynamicClass.getClassType();
        Integer isFallback = dynamicClass.getIsFallback();
        ClassInfo classInfo = IvyDynamicLoader.registerSpringClassInfo(cmpId,sourceCode);
        String key = classType + (isFallback == null ? "" : isFallback.toString());
        IvyLiteFlowNodeBuilder builder = IvyLiteFlowNodeBuilder.createCommonNode();
        switch (key){
            case "11":
            case "10":
                builder = IvyLiteFlowNodeBuilder.createCommonNode();
                if(flag){
                    el = ELBusThen.node(cmpId).toEL();
                }
                break;//普通组件类
            case "21":
            case "20":
                builder = IvyLiteFlowNodeBuilder.createSwitchNode();
                if(flag) {
                    el = ELBusSwitch.NEW().node(cmpId).defaultOpt("CommonCmp").toEL();
                }
                break;//选择组件类
            case "31":
            case "30":
                builder = IvyLiteFlowNodeBuilder.createIfNode();
                if(flag) {
                    el = ELBusIf.NEW().node(cmpId, "CommonCmp", "CommonCmp").toEL();
                }
                break;//条件组件类
            case "41":
            case "40":
                builder = IvyLiteFlowNodeBuilder.createForNode();
                if(flag) {
                    el = ELBusFor.node(cmpId).doOpt("CommonCmp").toEL();
                }
                break;//次数循环组件类
            case "51":
            case "50":
                builder = IvyLiteFlowNodeBuilder.createWhileNode();
                if(flag) {
                    el = ELBusWhile.node(cmpId).doOpt("CommonCmp").toEL();
                }
                break;//条件循环组件类
            case "61":
            case "60":
                builder = IvyLiteFlowNodeBuilder.createIteratorNode();
                if(flag) {
                    el = ELBusIterator.node(cmpId).doOpt("CommonCmp").toEL();
                }
                break;//迭代循环组件类
            case "71":
            case "70":
                builder = IvyLiteFlowNodeBuilder.createBreakNode();
                if(flag) {
                    el = ELBusIf.NEW().node(cmpId, "CommonCmp", "CommonCmp").toEL();
                }
                break;//退出循环组件类
        }

        if(ivyCmp != null){
            builder.setId(ivyCmp.getComponentId()).setName(ivyCmp.getComponentName()).setLanguage(ivyCmp.getLanguage()).setClazz(classInfo.getClazz()).build();
        }else{
            builder.setId(cmpId).setName(dynamicClass.getClassName()).setLanguage("java").setClazz(classInfo.getClazz()).build();
        }
        return el;
    }


    public static boolean ivyLiteFlowNodeBuilderNode(IvyCmp ivyCmp){
        String type = ivyCmp.getType();
        IvyLiteFlowNodeBuilder builder = null;
        switch (type){
            case "common":
                builder = IvyLiteFlowNodeBuilder.createCommonNode();
                break;
            case "switch":
                builder = IvyLiteFlowNodeBuilder.createSwitchNode();
                break;
            case "if":
                builder = IvyLiteFlowNodeBuilder.createIfNode();
                break;
            case "for":
                builder = IvyLiteFlowNodeBuilder.createForNode();
                break;
            case "while":
                builder = IvyLiteFlowNodeBuilder.createWhileNode();
                break;
            case "iterator":
                builder = IvyLiteFlowNodeBuilder.createIteratorNode();
                break;
            case "break":
                builder = IvyLiteFlowNodeBuilder.createBreakNode();
                break;
            default:
                break;
        }
        if(builder != null){
            builder.setId(ivyCmp.getComponentId()).setName(ivyCmp.getComponentName()).setLanguage(ivyCmp.getLanguage() == null ? ScriptTypeEnum.JAVA.getDisplayName() : ivyCmp.getLanguage()).setClazz(ivyCmp.getClazz()).buildClass();
            return true;
        }
        return false;
    }

    public static boolean ivyLiteFlowNodeBuilderScript(IvyCmp ivyCmp){
        String type = ivyCmp.getType();
        IvyLiteFlowNodeBuilder builder = null;
        switch (type){
            case "script":
                builder = IvyLiteFlowNodeBuilder.createScriptNode();
                break;
            case "switch_script":
                builder = IvyLiteFlowNodeBuilder.createScriptSwitchNode();
                break;
            case "if_script":
                builder = IvyLiteFlowNodeBuilder.createScriptIfNode();
                break;
            case "for_script":
                builder = IvyLiteFlowNodeBuilder.createScriptForNode();
                break;
            case "while_script":
                builder = IvyLiteFlowNodeBuilder.createScriptWhileNode();
                break;
            case "break_script":
                builder = IvyLiteFlowNodeBuilder.createScriptBreakNode();
                break;
            default:
                break;
        }
        if(builder != null){
            builder.setId(ivyCmp.getComponentId()).setName(ivyCmp.getComponentName()).setLanguage(ivyCmp.getLanguage()).setScript(ivyCmp.getScript()).build();
            return true;
        }
        return false;
    }

    public static final String pattern = "node\\(\"([^\"]+)\"\\)";

    public static List<String> getComponentIds(String el){
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(el);
        List<String> list = new ArrayList<>();
        while (m.find()) {
            list.add(m.group(1));
        }
        return list;
    }
}
