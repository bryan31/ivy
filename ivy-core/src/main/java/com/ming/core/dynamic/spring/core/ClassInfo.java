package com.ming.core.dynamic.spring.core;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClassInfo {

    private String className;
    private String packageName;
    private String beanName;
    private String fullClassName;
    private Class<?> clazz;

}
