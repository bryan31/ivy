package com.ming.core.builder;

import java.util.Map;

public interface FlowBuilder {

    FlowBuilder context(FlowBuilderContext context);

    FlowBuilder addNode(Map<String,Object> nodeMap);

    FlowBuilder addEdge(Map<String,Object> nodeMap);

    FlowBuilder format(boolean format);

    FlowBuilder flowJson(String flowJson);

    String build();

}
