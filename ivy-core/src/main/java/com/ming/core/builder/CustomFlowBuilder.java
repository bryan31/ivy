package com.ming.core.builder;

import java.util.Map;

public class CustomFlowBuilder implements FlowBuilder{

    private FlowBuilderContext context;

    private boolean format = true;

    @Override
    public FlowBuilder context(FlowBuilderContext context) {
        this.context = context;
        return this;
    }

    @Override
    public FlowBuilder addNode(Map<String,Object> nodeMap) {
        return this;
    }

    @Override
    public FlowBuilder addEdge(Map<String,Object> nodeMap) {
        return this;
    }

    @Override
    public FlowBuilder format(boolean format) {
        this.format = format;
        return this;
    }

    @Override
    public FlowBuilder flowJson(String flowJson) {
        return null;
    }

    @Override
    public String build() {
        return null;
    }
}
