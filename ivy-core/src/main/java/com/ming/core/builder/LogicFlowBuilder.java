package com.ming.core.builder;

import java.util.Map;

public class LogicFlowBuilder implements FlowBuilder {
    @Override
    public FlowBuilder context(FlowBuilderContext context) {
        return null;
    }

    @Override
    public FlowBuilder addNode(Map<String, Object> nodeMap) {
        return null;
    }

    @Override
    public FlowBuilder addEdge(Map<String, Object> nodeMap) {
        return null;
    }

    @Override
    public FlowBuilder format(boolean format) {
        return null;
    }

    @Override
    public FlowBuilder flowJson(String flowJson) {
        return null;
    }

    @Override
    public String build() {
        return null;
    }
}
