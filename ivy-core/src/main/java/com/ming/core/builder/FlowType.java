package com.ming.core.builder;

public enum FlowType {

    LOGIC_FLOW(),
    VUE_FLOW(),
    CUSTOM_FLOW(),
    ;

}
