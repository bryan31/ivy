package com.ming.core.builder;

import cn.hutool.core.map.MapUtil;

import java.util.HashMap;

public class BuilderTest {

    /*public static void main(String[] args) {
        String json = FlowBuilderContext.NEW().flowType(FlowType.VUE_FLOW)
                .addMappingNodes("nodes1")
                .addMappingEdges("edges1")
                .addMappingNodeId("id")
                .addMappingNodeType("type")
                .addMappingNodeLabel("label")
                .addMappingNodeData("data")
                .addMappingEdgeId("id")
                .addMappingEdgeType("type")
                .addMappingEdgeSource("source")
                .addMappingEdgeTarget("target")
                .addMappingEdgeData("data")
                .getFlowBuilder()
                .format(true)
                .flowJson(getFlowJson())
//                .addNode(MapUtil.builder(new HashMap<String,Object>()).put("id", "1").put("type", "common").put("label", "普通组件").put("data", "{}").build())
//                .addNode(MapUtil.builder(new HashMap<String,Object>()).put("id", "2").put("type", "switch").put("label", "选择组件").put("data", "{}").build())
//                .addEdge(MapUtil.builder(new HashMap<String,Object>()).put("id", "3").put("type", "custom").put("source", "1").put("target", "2").put("data", "{}").build())
                .build();


        System.out.println(json);
    }*/


    private static String getFlowJson(){
        return "{\n" +
                "  \"nodes1\": [\n" +
                "    {\n" +
                "      \"id\": \"f5dce90354ae46b99ca63bff6b0a2d9c\",\n" +
                "      \"type\": \"common\",\n" +
                "      \"position\": {\n" +
                "        \"x\": 130.5,\n" +
                "        \"y\": 117.484375\n" +
                "      },\n" +
                "      \"data\": {\n" +
                "        \"id\": \"\",\n" +
                "        \"type\": \"common\",\n" +
                "        \"name\": \"普通组件\",\n" +
                "        \"style\": {\n" +
                "          \"handles\": {\n" +
                "            \"left\": {\n" +
                "              \"show\": true,\n" +
                "              \"type\": \"source\"\n" +
                "            },\n" +
                "            \"right\": {\n" +
                "              \"show\": true,\n" +
                "              \"type\": \"source\"\n" +
                "            },\n" +
                "            \"top\": {\n" +
                "              \"show\": false,\n" +
                "              \"type\": \"source\"\n" +
                "            },\n" +
                "            \"bottom\": {\n" +
                "              \"show\": false,\n" +
                "              \"type\": \"source\"\n" +
                "            }\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      \"label\": \"普通组件\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": \"5a18c8dea2cb4ae58e11963a2e3d0d4a\",\n" +
                "      \"type\": \"switch\",\n" +
                "      \"position\": {\n" +
                "        \"x\": 346.0033261783317,\n" +
                "        \"y\": 50.532903031451156\n" +
                "      },\n" +
                "      \"data\": {\n" +
                "        \"id\": \"\",\n" +
                "        \"type\": \"switch\",\n" +
                "        \"name\": \"选择组件\",\n" +
                "        \"style\": {\n" +
                "          \"handles\": {\n" +
                "            \"left\": {\n" +
                "              \"show\": true,\n" +
                "              \"type\": \"source\"\n" +
                "            },\n" +
                "            \"right\": {\n" +
                "              \"show\": true,\n" +
                "              \"type\": \"source\"\n" +
                "            },\n" +
                "            \"top\": {\n" +
                "              \"show\": false,\n" +
                "              \"type\": \"source\"\n" +
                "            },\n" +
                "            \"bottom\": {\n" +
                "              \"show\": false,\n" +
                "              \"type\": \"source\"\n" +
                "            }\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      \"label\": \"选择组件\",\n" +
                "      \"parentNode\": \"1bc734a45cbc497f96152547ee3f4e20\",\n" +
                "      \"expandParent\": true\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": \"1ebdd428538240398348fa3da050e02a\",\n" +
                "      \"type\": \"boolean\",\n" +
                "      \"position\": {\n" +
                "        \"x\": 167.5,\n" +
                "        \"y\": 374.484375\n" +
                "      },\n" +
                "      \"data\": {\n" +
                "        \"id\": \"\",\n" +
                "        \"type\": \"boolean\",\n" +
                "        \"name\": \"布尔组件\",\n" +
                "        \"style\": {\n" +
                "          \"handles\": {\n" +
                "            \"left\": {\n" +
                "              \"show\": true,\n" +
                "              \"type\": \"source\"\n" +
                "            },\n" +
                "            \"right\": {\n" +
                "              \"show\": true,\n" +
                "              \"type\": \"source\"\n" +
                "            },\n" +
                "            \"top\": {\n" +
                "              \"show\": false,\n" +
                "              \"type\": \"source\"\n" +
                "            },\n" +
                "            \"bottom\": {\n" +
                "              \"show\": false,\n" +
                "              \"type\": \"source\"\n" +
                "            }\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      \"label\": \"布尔组件\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": \"0d0afe5c032a4ef29496532e709649e7\",\n" +
                "      \"type\": \"for\",\n" +
                "      \"position\": {\n" +
                "        \"x\": 547.5,\n" +
                "        \"y\": 375.484375\n" +
                "      },\n" +
                "      \"data\": {\n" +
                "        \"id\": \"\",\n" +
                "        \"type\": \"for\",\n" +
                "        \"name\": \"次数循环组件\",\n" +
                "        \"style\": {\n" +
                "          \"handles\": {\n" +
                "            \"left\": {\n" +
                "              \"show\": true,\n" +
                "              \"type\": \"source\"\n" +
                "            },\n" +
                "            \"right\": {\n" +
                "              \"show\": true,\n" +
                "              \"type\": \"source\"\n" +
                "            },\n" +
                "            \"top\": {\n" +
                "              \"show\": false,\n" +
                "              \"type\": \"source\"\n" +
                "            },\n" +
                "            \"bottom\": {\n" +
                "              \"show\": false,\n" +
                "              \"type\": \"source\"\n" +
                "            }\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      \"label\": \"次数循环组件\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"id\": \"c5d5211dbf2a42ff89e43f6ab29593e2\",\n" +
                "      \"type\": \"iterator\",\n" +
                "      \"position\": {\n" +
                "        \"x\": 1813.234527885277,\n" +
                "        \"y\": 161.93209048566408\n" +
                "      },\n" +
                "      \"data\": {\n" +
                "        \"id\": \"\",\n" +
                "        \"type\": \"iterator\",\n" +
                "        \"name\": \"迭代循环组件\",\n" +
                "        \"style\": {\n" +
                "          \"handles\": {\n" +
                "            \"left\": {\n" +
                "              \"show\": true,\n" +
                "              \"type\": \"source\"\n" +
                "            },\n" +
                "            \"right\": {\n" +
                "              \"show\": true,\n" +
                "              \"type\": \"source\"\n" +
                "            },\n" +
                "            \"top\": {\n" +
                "              \"show\": false,\n" +
                "              \"type\": \"source\"\n" +
                "            },\n" +
                "            \"bottom\": {\n" +
                "              \"show\": false,\n" +
                "              \"type\": \"source\"\n" +
                "            }\n" +
                "          }\n" +
                "        }\n" +
                "      },\n" +
                "      \"label\": \"迭代循环组件\",\n" +
                "      \"parentNode\": \"1bc734a45cbc497f96152547ee3f4e20\",\n" +
                "      \"expandParent\": true\n" +
                "    }\n" +
                "  ],\n" +
                "  \"edges1\": [\n" +
                "    {\n" +
                "      \"id\": \"fed7610c79014251a6a1c4e02171078a\",\n" +
                "      \"type\": \"custom\",\n" +
                "      \"source\": \"5a18c8dea2cb4ae58e11963a2e3d0d4a\",\n" +
                "      \"target\": \"c5d5211dbf2a42ff89e43f6ab29593e2\",\n" +
                "      \"sourceHandle\": \"5a18c8dea2cb4ae58e11963a2e3d0d4a__handle-right\",\n" +
                "      \"targetHandle\": \"c5d5211dbf2a42ff89e43f6ab29593e2__handle-left\",\n" +
                "      \"updatable\": true,\n" +
                "      \"data\": {},\n" +
                "      \"label\": \"\",\n" +
                "      \"animated\": true,\n" +
                "      \"markerEnd\": \"arrowclosed\",\n" +
                "      \"sourceX\": 677.0033261783317,\n" +
                "      \"sourceY\": 151.03290303145116,\n" +
                "      \"targetX\": 1802.234527885277,\n" +
                "      \"targetY\": 262.43209048566405\n" +
                "    }\n" +
                "  ],\n" +
                "  \"position\": [\n" +
                "    -196.50910102282512,\n" +
                "    -241.08257626379452\n" +
                "  ],\n" +
                "  \"zoom\": 1.1367874248827985,\n" +
                "  \"viewport\": {\n" +
                "    \"x\": -196.50910102282512,\n" +
                "    \"y\": -241.08257626379452,\n" +
                "    \"zoom\": 1.1367874248827985\n" +
                "  }\n" +
                "}";
    }
}
