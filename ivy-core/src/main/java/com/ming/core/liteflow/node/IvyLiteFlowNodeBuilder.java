package com.ming.core.liteflow.node;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.util.StrUtil;
import com.yomahub.liteflow.enums.NodeTypeEnum;
import com.yomahub.liteflow.exception.NodeBuildException;
import com.yomahub.liteflow.flow.FlowBus;
import com.yomahub.liteflow.flow.element.Node;
import com.yomahub.liteflow.log.LFLog;
import com.yomahub.liteflow.log.LFLoggerManager;
import com.yomahub.liteflow.monitor.MonitorFile;
import com.yomahub.liteflow.spi.holder.PathContentParserHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class IvyLiteFlowNodeBuilder {

    private final LFLog LOG = LFLoggerManager.getLogger(this.getClass());
    private final Node node = new Node();
    private Class<?> clazz;

    public static IvyLiteFlowNodeBuilder createNode() {
        return new IvyLiteFlowNodeBuilder();
    }

    public static IvyLiteFlowNodeBuilder createCommonNode() {
        return new IvyLiteFlowNodeBuilder(NodeTypeEnum.COMMON);
    }

    public static IvyLiteFlowNodeBuilder createSwitchNode() {
        return new IvyLiteFlowNodeBuilder(NodeTypeEnum.SWITCH);
    }

    public static IvyLiteFlowNodeBuilder createIfNode() {
        return new IvyLiteFlowNodeBuilder(NodeTypeEnum.IF);
    }

    public static IvyLiteFlowNodeBuilder createForNode() {
        return new IvyLiteFlowNodeBuilder(NodeTypeEnum.FOR);
    }

    public static IvyLiteFlowNodeBuilder createWhileNode() {
        return new IvyLiteFlowNodeBuilder(NodeTypeEnum.WHILE);
    }

    public static IvyLiteFlowNodeBuilder createBreakNode() {
        return new IvyLiteFlowNodeBuilder(NodeTypeEnum.BREAK);
    }

    public static IvyLiteFlowNodeBuilder createIteratorNode() {
        return new IvyLiteFlowNodeBuilder(NodeTypeEnum.ITERATOR);
    }

    public static IvyLiteFlowNodeBuilder createScriptNode() {
        return new IvyLiteFlowNodeBuilder(NodeTypeEnum.SCRIPT);
    }

    public static IvyLiteFlowNodeBuilder createScriptSwitchNode() {
        return new IvyLiteFlowNodeBuilder(NodeTypeEnum.SWITCH_SCRIPT);
    }

    public static IvyLiteFlowNodeBuilder createScriptIfNode() {
        return new IvyLiteFlowNodeBuilder(NodeTypeEnum.IF_SCRIPT);
    }

    public static IvyLiteFlowNodeBuilder createScriptForNode() {
        return new IvyLiteFlowNodeBuilder(NodeTypeEnum.FOR_SCRIPT);
    }

    public static IvyLiteFlowNodeBuilder createScriptWhileNode() {
        return new IvyLiteFlowNodeBuilder(NodeTypeEnum.WHILE_SCRIPT);
    }

    public static IvyLiteFlowNodeBuilder createScriptBreakNode() {
        return new IvyLiteFlowNodeBuilder(NodeTypeEnum.BREAK_SCRIPT);
    }

    public IvyLiteFlowNodeBuilder() {
    }

    public IvyLiteFlowNodeBuilder(NodeTypeEnum type) {
        this.node.setType(type);
    }

    public IvyLiteFlowNodeBuilder setId(String nodeId) {
        if (StrUtil.isBlank(nodeId)) {
            return this;
        } else {
            this.node.setId(nodeId.trim());
            return this;
        }
    }

    public IvyLiteFlowNodeBuilder setName(String name) {
        if (StrUtil.isBlank(name)) {
            return this;
        } else {
            this.node.setName(name.trim());
            return this;
        }
    }

    public IvyLiteFlowNodeBuilder setClazz(String clazz) {
        if (StrUtil.isBlank(clazz)) {
            return this;
        } else {
            this.node.setClazz(clazz.trim());
            return this;
        }
    }

    public IvyLiteFlowNodeBuilder setClazz(Class<?> clazz) {
        assert clazz != null;

        this.clazz = clazz;
        this.setClazz(clazz.getName());
        return this;
    }

    public IvyLiteFlowNodeBuilder setType(NodeTypeEnum type) {
        this.node.setType(type);
        return this;
    }

    public IvyLiteFlowNodeBuilder setScript(String script) {
        this.node.setScript(script);
        return this;
    }

    public IvyLiteFlowNodeBuilder setFile(String filePath) {
        if (StrUtil.isBlank(filePath)) {
            return this;
        } else {
            String errMsg;
            try {
                List<String> scriptList = PathContentParserHolder.loadContextAware().parseContent(ListUtil.toList(new String[]{filePath}));
                errMsg = (String)CollUtil.getFirst(scriptList);
                this.setScript(errMsg);
                List<String> fileAbsolutePath = PathContentParserHolder.loadContextAware().getFileAbsolutePath(ListUtil.toList(new String[]{filePath}));
                MonitorFile.getInstance().addMonitorFilePaths(fileAbsolutePath);
                return this;
            } catch (Exception var5) {
                Exception e = var5;
                errMsg = StrUtil.format("An exception occurred while building the node[{}],{}", new Object[]{this.node.getId(), e.getMessage()});
                throw new NodeBuildException(errMsg);
            }
        }
    }

    public IvyLiteFlowNodeBuilder setLanguage(String language) {
        this.node.setLanguage(language);
        return this;
    }

    public void build() {
        this.checkBuild();

        try {
            if (this.node.getType().isScript()) {
                FlowBus.addScriptNode(this.node.getId(), this.node.getName(), this.node.getType(), this.node.getScript(), this.node.getLanguage());
            } else {
                FlowBus.addNode(this.node.getId(), this.node.getName(), this.node.getType(), this.clazz);
            }

        } catch (Exception var3) {
            Exception e = var3;
            String errMsg = StrUtil.format("An exception occurred while building the node[{}],{}", new Object[]{this.node.getId(), e.getMessage()});
            this.LOG.error(errMsg, e);
            throw new NodeBuildException(errMsg);
        }
    }

    public void buildClass() {
        this.checkBuild();

        try {
            if (this.node.getType().isScript()) {
                FlowBus.addScriptNode(this.node.getId(), this.node.getName(), this.node.getType(), this.node.getScript(), this.node.getLanguage());
            } else {
                FlowBus.addNode(this.node.getId(), this.node.getName(), this.node.getType(), this.node.getClazz());
            }

        } catch (Exception var3) {
            Exception e = var3;
            String errMsg = StrUtil.format("An exception occurred while building the node[{}],{}", new Object[]{this.node.getId(), e.getMessage()});
            this.LOG.error(errMsg, e);
            throw new NodeBuildException(errMsg);
        }
    }

    private void checkBuild() {
        List<String> errorList = new ArrayList();
        if (StrUtil.isBlank(this.node.getId())) {
            errorList.add("id is blank");
        }

        if (Objects.isNull(this.node.getType())) {
            errorList.add("type is null");
        }

        if (CollUtil.isNotEmpty(errorList)) {
            throw new NodeBuildException(CollUtil.join(errorList, ",", "[", "]"));
        }
    }
}
