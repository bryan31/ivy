package com.ming.core.liteflow.vo;

import com.ivy.builder.graph.IvyDynamicClass;
import com.ming.core.query.Options;
import lombok.Data;

@Data
public class IvyDynamicClassVo extends IvyDynamicClass {

    private Options options;

    private String methodName;

    private Object[] params;

    private String el;

}
