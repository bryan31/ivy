package com.ming.core.liteflow.chain;

import com.ming.core.util.NumberUtil;
import com.yomahub.liteflow.builder.el.ELWrapper;
import com.yomahub.liteflow.builder.el.LiteFlowChainELBuilder;
import com.yomahub.liteflow.flow.FlowBus;

import java.util.LinkedHashMap;
import java.util.Map;

public class IvyChainUtil {

    public static final Map<String, String> chainMap = new LinkedHashMap<>();

    public static void randomChain(ELWrapper el){
        randomChain(el.toEL());
    }

    public static void randomChain(String el){
        buildChain("default_chain_" + NumberUtil.getNextIndex(), el);
    }

    public static void buildChain(String chainId, ELWrapper el){
        buildChain(chainId, el.toEL());
    }

    public static void buildChain(String chainId, String el){
        LiteFlowChainELBuilder.createChain().setChainId(chainId).setEL(el).build();
        chainMap.put(chainId, el);
    }

    public static void removeChain(String chainId){
        FlowBus.removeChain(chainId);
    }
}
