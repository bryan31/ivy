package com.ming.core.liteflow.entity;

import com.ming.core.anno.Describe;
import com.ming.core.anno.DescribeItem;
import com.ming.core.generate.template.annotation.Generate;
import com.ming.core.generate.template.annotation.database.Column;
import com.ming.core.generate.template.annotation.database.PrimaryKey;
import com.ming.core.generate.template.annotation.database.Text;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

@Data
@Table(name = "ivy_config")
@Generate(isEffective = true, moduleName = "db", desc = "配置项信息")
public class IvyConfig {

    @Column
    @PrimaryKey
    private Long id;

    @Column
    private String configId;

    @Column
    private String configName;

//    @Column(len = 12)
//    @Describe(value = "配置风格",items = {
//        @DescribeItem(value = "default",desc = "默认风格配置"),
//        @DescribeItem(value = "properties",desc = "Properties风格配置"),
//        @DescribeItem(value = "yaml",desc = "Yaml风格配置"),
//        @DescribeItem(value = "xml",desc = "Xml风格配置"),
//        @DescribeItem(value = "json",desc = "Json风格配置"),
//    })
//    private String styleType;

    @Column(len = 12)
    @Describe(value = "规则类型",items = {
            @DescribeItem(value = "ruleSource",desc = "本地规则文件配置"),
            @DescribeItem(value = "ruleSourceZk",desc = "ZK规则文件配置源"),
            @DescribeItem(value = "ruleSourceSql",desc = "SQL数据库配置源"),
            @DescribeItem(value = "ruleSourceNacos",desc = "Nacos配置源"),
            @DescribeItem(value = "ruleSourceEtcd",desc = "Etcd配置源"),
            @DescribeItem(value = "ruleSourceApollo",desc = "Apollo配置源"),
            @DescribeItem(value = "ruleSourceRedisPoll",desc = "Redis配置源-轮询模式"),
            @DescribeItem(value = "ruleSourceRedisSub",desc = "Redis配置源-订阅模式"),
            @DescribeItem(value = "ruleSourceCustom",desc = "自定义配置源"),
    })
    private String ruleType;

    @Column(len = 1)
    @Describe(value = "配置项类型",items = {
        //@DescribeItem(value = "0",desc = "ruleSource"),
        @DescribeItem(value = "1",desc = "ruleSourceExtDataMap"),
        @DescribeItem(value = "2",desc = "ruleSourceExtData")
    })
    private Integer configType;

//    @Column(len = 1)
//    @Describe(value = "数据源类型",items = {
//        @DescribeItem(value = "0",desc = "sql"),
//        @DescribeItem(value = "1",desc = "nacos"),
//        @DescribeItem(value = "2",desc = "redis-poll"),
//        @DescribeItem(value = "3",desc = "redis-sub"),
//        @DescribeItem(value = "4",desc = "zookeeper"),
//        @DescribeItem(value = "5",desc = "etcd"),
//        @DescribeItem(value = "6",desc = "apollo"),
//    })
//    private Integer sourceType;

    @Text
    @Column
    private String ruleSource;

    @Text
    @Column
    private String ruleSourceExtDataMap;

    @Text
    @Column
    private String ruleSourceExtData;

    @Column
    private Boolean enable;

    @Column
    private Boolean printBanner;

    @Column(len = 64)
    private String zkNode;

    @Column
    private Integer slotSize;

    @Column
    private Integer mainExecutorWorks;

    @Column(len = 128)
    private String mainExecutorClass;

    @Column(len = 128)
    private String requestIdGeneratorClass;

    @Column(len = 128)
    private String threadExecutorClass;

    @Column
    private Integer whenMaxWaitTime;
    @Column
    private String whenMaxWaitTimeUnit;
    @Column
    private Integer whenMaxWorkers;
    @Column
    private Integer whenQueueLimit;
    @Column
    private Boolean whenThreadPoolIsolate;
    @Column
    private Integer parallelLoopMaxWorkers;
    @Column
    private Integer parallelLoopQueueLimit;
    @Column(len = 128)
    private String parallelLoopExecutorClass;
    @Column
    private Boolean parseOnStart;
    @Column
    private Integer retryCount;
    @Column
    private Boolean supportMultipleType;
    @Column(len = 128)
    private String nodeExecutorClass;
    @Column
    private Boolean printExecutionLog;
    @Column
    private Boolean enableMonitorFile;
    @Column
    private Boolean fastLoad;
    @Column
    private Boolean enableLog;
    @Column
    private Integer queueLimit;
    @Column
    private Long delay;
    @Column
    private Long period;
    @Column
    private Boolean fallbackCmpEnable;

}
