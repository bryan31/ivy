//package com.ming.core.liteflow.entity;
//
//import com.ming.core.anno.Describe;
//import com.ming.core.anno.DescribeItem;
//import com.ming.core.generate.template.annotation.Generate;
//import com.ming.core.generate.template.annotation.database.Column;
//import com.ming.core.generate.template.annotation.database.PrimaryKey;
//import com.ming.core.generate.template.annotation.database.Text;
//import lombok.Data;
//import org.beetl.sql.annotation.entity.Table;
//
//@Data
//@Table(name = "ivy_dynamic_class")
//@Generate(isEffective = true, moduleName = "db", desc = "动态类")
//public class IvyDynamicClass {
//
//    @Column
//    @PrimaryKey
//    private Long id;
//
//    @Column(len = 128)
//    @Describe(value = "动态类ID")
//    private String classId;
//
//    @Column(len = 2)
//    @Describe(value = "class类型",items = {
//            @DescribeItem(value = "0",desc = "普通类【class】"),
//            @DescribeItem(value = "1",desc = "普通组件类【common】"),
//            @DescribeItem(value = "2",desc = "选择组件类【switch】"),
//            @DescribeItem(value = "3",desc = "条件组件类【if】"),
//            @DescribeItem(value = "4",desc = "次数循环组件类【for】"),
//            @DescribeItem(value = "5",desc = "条件循环组件类【while】"),
//            @DescribeItem(value = "6",desc = "迭代循环组件类【iterator】"),
//            @DescribeItem(value = "7",desc = "退出循环组件类【break】"),
//    })
//    private Integer classType;
//
//    @Column(len = 1)
//    @Describe(value = "是否为降级组件",items = {
//            @DescribeItem(value = "0",desc = "非降级组件"),
//            @DescribeItem(value = "1",desc = "降级组件"),
//    })
//    private Integer isFallback;
//
//    @Column
//    @Describe(value = "包路径")
//    private String packagePath;
//
//    @Column
//    @Describe(value = "动态类名称")
//    private String className;
//
//    @Column(len = 1)
//    @Describe(value = "加载器类型",items = {
//            @DescribeItem(value = "0",desc = "应用程序类加载器"),
//            @DescribeItem(value = "1",desc = "自定义类加载器"),
//    })
//    private Integer classLoaderType;
//
//    @Text
//    @Column
//    @Describe(value = "Java源码")
//    private String sourceCode;
//
//    @Column
//    @Describe(value = "版本号")
//    private Integer version;
//
//    @Column
//    @Describe(value = "组件ID")
//    private String sourceCmpId;
//
//    @Column
//    @Describe(value = "源码类名")
//    private String sourceClassName;
//
//}
