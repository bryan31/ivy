package com.ming.core.liteflow.entity;

import com.ming.core.anno.Describe;
import com.ming.core.generate.template.annotation.Generate;
import com.ming.core.generate.template.annotation.database.Column;
import com.ming.core.generate.template.annotation.database.PrimaryKey;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

@Data
@Table(name = "ivy_chain")
@Generate(isEffective = true, moduleName = "db", desc = "链路")
public class IvyChain {

    @Column
    @PrimaryKey
    private Long id;

    @Column
    @Describe(value = "链路ID")
    private String chainId;

    @Column
    @Describe(value = "链路名称")
    private String chainName;

    @Column
    @Describe(value = "ivy_el表ID")
    private Long ivyElId;

}
