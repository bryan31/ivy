package com.ming.core.generate.template.core;


import cn.hutool.core.util.StrUtil;
import com.ming.core.generate.template.utils.GenerateUtil;
import com.ming.core.generate.template.utils.ScannerAnno;
import org.beetl.sql.core.SQLManager;
import org.beetl.sql.core.SQLReady;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Administrator on 2020/4/13 0013.
 */
public class GenerateTemplate {
    private volatile static GenerateTemplate generate = null;
    private GenerateTemplate(){}
    public static GenerateTemplate getInstance()   {
        if (generate == null)  {
            synchronized (GenerateTemplate.class) {
                if (generate== null)  {
                    generate= new GenerateTemplate();
                }
            }
        }
        return generate;
    }

    public void create(String path) throws Exception {
        String[] paths = {path};
        create(paths);
    }

    public void create(String[] paths) throws Exception {
        System.out.println("generate file...");
        ScannerAnno scanner = new ScannerAnno();
        List<GenerateInfo> list = new ArrayList<GenerateInfo>();
        for (String path : paths){
            list.addAll(scanner.getScannerAnnoList(path));
        }

        for (GenerateInfo info : list) {
            GenerateHelper.generateVo(info);
            GenerateHelper.generateService(info);
            GenerateHelper.generateController(info);
            GenerateHelper.generateServiceImpl(info);
            GenerateHelper.generateMapper(info);
            GenerateHelper.generateMapperXml(info);
            GenerateHelper.generateSql(info);
        }
        System.out.println("generate file complete!");
    }

    public void autoGenSql(SQLManager sqlManager, String[] paths) {
        ScannerAnno scanner = new ScannerAnno();
        Set<Class<?>> classSet = new HashSet<>();
        for (String path : paths){
            try {
                classSet.addAll(scanner.getScannerAnno(path));
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
        for (Class<?> clazz : classSet){
            String tableName = sqlManager.getNc().getTableName(clazz);
            List<FieldColumn> javaColumnList = GenerateUtil.getFieldList(clazz);

            String tb = sqlManager.executeQueryOne(new SQLReady("SHOW TABLES LIKE '"+tableName+"'"), String.class);
            if(StrUtil.isBlank(tb)){
                System.err.println(tableName+"表不存在！");
                continue;
            }

            List<Map> dbColumnList = sqlManager.execute(new SQLReady("DESCRIBE " + tableName), Map.class);
            Map<String,String> dbColumnMap = dbColumnList.stream().collect(Collectors.toMap(m->(String)m.get("field"),m->(String)m.get("type")));
            int size1 = javaColumnList.size();
            int size2 = dbColumnList.size();

            List<FieldColumn> collect = javaColumnList.stream().filter(m ->
                    !(
                        dbColumnMap.containsKey(m.getFieldColumn()) &&
                        dbColumnMap.get(m.getFieldColumn()).toLowerCase().contains(m.getJdbcType().toLowerCase())
                    )
            ).collect(Collectors.toList());
            if(collect.size() > 0){
                System.err.println(tableName+"字段不一致："+collect.stream().map(FieldColumn::getFieldColumn).collect(Collectors.joining(",")));
            }
            /*if(size1 == size2){
                List<FieldColumn> collect = javaColumnList.stream().filter(m ->
                    !(
                        dbColumnMap.containsKey(m.getFieldColumn()) && (
                            dbColumnMap.get(m.getFieldColumn()).toLowerCase().contains(m.getJdbcType().toLowerCase()) ||
                            (dbColumnMap.get(m.getFieldColumn()).equalsIgnoreCase("int") && m.getJdbcType().equalsIgnoreCase("INTEGER"))
                        )
                    )
                ).collect(Collectors.toList());
                if(collect.size() > 0){
                    System.err.println(tableName+"字段不一致："+collect.stream().map(m->m.getFieldColumn()).collect(Collectors.joining(",")));
                }
            }else if(size1 > size2){
                List<FieldColumn> collect = javaColumnList.stream().filter(m ->
                    !(
                        dbColumnMap.containsKey(m.getFieldColumn()) && (
                            dbColumnMap.get(m.getFieldColumn()).toLowerCase().contains(m.getJdbcType().toLowerCase()) ||
                            (dbColumnMap.get(m.getFieldColumn()).equalsIgnoreCase("int") && m.getJdbcType().equalsIgnoreCase("INTEGER"))
                        )
                    )
                ).collect(Collectors.toList());
                if(collect.size() > 0){
                    System.err.println(tableName+"字段缺失："+collect.stream().map(m->m.getFieldColumn()).collect(Collectors.joining(",")));
                }
            }else if(size1 < size2){

            }*/
        }
        System.out.println();
    }


}
