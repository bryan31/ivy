package com.ming.core.generate.template.annotation.database;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by Administrator on 2020/1/6 0006.
 */
@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Column {

    int len() default -1;

    int intLen() default 11;
    int bigIntLen() default 20;

    int varCharLen() default 32;

    int tinyIntLen() default 1;
    int decimalLen() default 11;
    int doubleLen() default 11;

}
