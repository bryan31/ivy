package com.ming.core.parser.entity.node;

import com.ming.core.parser.entity.style.NodeStyle;
import lombok.Data;

@Data
public class NodeData {

    private String id;//组件ID
    private String name;//组件名称
    private String type;//组件类型
    private NodeStyle style;

}
