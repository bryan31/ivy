package com.ming.core.parser.entity.node;

import lombok.Data;

@Data
public class Position {

    private Double x;
    private Double y;

}
