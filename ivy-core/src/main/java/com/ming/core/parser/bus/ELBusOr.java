package com.ming.core.parser.bus;

import com.ming.core.parser.entity.node.Node;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.OrELWrapper;

public class ELBusOr extends BaseELBus {

    public static OrELWrapper node(Object... objects){
        return ELBus.or(objects);
    }

//    public static OrELWrapper node(Node node){
//        OrELWrapper wrapper = ELBus.or(node.getOrEL());
//        setId(wrapper, node);
//        setTag(wrapper, node);
//        setMaxWaitSeconds(wrapper, node);
//        return wrapper;
//    }

}
