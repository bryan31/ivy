package com.ming.core.parser.entity;

import com.ming.core.parser.entity.edge.Edge;
import com.ming.core.parser.entity.node.Node;
import lombok.Data;

import java.util.List;

@Data
public class FlowData {

    private List<Node> nodes;
    private List<Edge> edges;
    private List<Double> position;
    private Double zoom;
    private Viewport viewport;
    private Boolean format = false;

}
