package com.ming.core.parser.bus;

import com.ming.core.parser.entity.node.Node;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.IteratorELWrapper;
import com.yomahub.liteflow.builder.el.NodeELWrapper;

public class ELBusIterator extends BaseELBus {

    public static IteratorELWrapper node(NodeELWrapper nodeElWrapper) {
        return ELBus.iteratorOpt(nodeElWrapper);
    }

    public static IteratorELWrapper node(String nodeElWrapper) {
        return ELBus.iteratorOpt(nodeElWrapper);
    }

//    public static IteratorELWrapper node(Node node){
//        Object doOpt = node.getCmpDoOptEL() != null ? node.getCmpDoOptEL() : node.getCmpDoOpt();
//        IteratorELWrapper wrapper = ELBus.iteratorOpt(ELBusNode.node(node));
//        wrapper.doOpt(doOpt);
//        wrapper.parallel(node.getCmpParallel());
//        setId(wrapper, node);
//        setTag(wrapper, node);
//        setMaxWaitSeconds(wrapper, node);
//        return wrapper;
//    }

}
