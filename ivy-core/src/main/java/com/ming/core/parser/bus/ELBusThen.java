package com.ming.core.parser.bus;

import cn.hutool.core.util.StrUtil;
import com.ming.core.parser.entity.node.Node;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.ThenELWrapper;

public class ELBusThen extends BaseELBus {

    public static ThenELWrapper node(Object... objects){
        return ELBus.then(objects);
    }

//    public static ThenELWrapper node(Node node){
//        ThenELWrapper wrapper = ELBus.then(ELBusNode.node(node));
//        if(node.getPreEL() != null){
//            wrapper.pre(node.getPreEL());
//        }else if(StrUtil.isNotBlank(node.getCmpPre())){
//            wrapper.pre(node.getCmpPre());
//        }
//        if(node.getFinallyEL() != null) {
//            wrapper.finallyOpt(node.getFinallyEL());
//        }else if(StrUtil.isNotBlank(node.getCmpFinallyOpt())){
//            wrapper.finallyOpt(node.getCmpFinallyOpt());
//        }
//        setId(wrapper, node);
//        setTag(wrapper, node);
//        setMaxWaitSeconds(wrapper, node);
//        return wrapper;
//    }

}
