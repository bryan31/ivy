package com.ming.core.parser.bus;

import cn.hutool.core.util.StrUtil;
import com.ming.core.parser.entity.node.Node;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.ELWrapper;
import com.yomahub.liteflow.builder.el.IfELWrapper;
import com.yomahub.liteflow.builder.el.ThenELWrapper;

public class ELBusIf {

    private ELWrapper wrapper;

    public static ELBusIf NEW(){
        return new ELBusIf();
    }

    public IfELWrapper node(String node,String trueOpt, String falseOpt){
        IfELWrapper ifELWrapper = ELBus.ifOpt(ELBusNode.node(node), trueOpt, falseOpt);
        return ifELWrapper;
    }

//    public ELBusIf node(Node node){
//        Object trueOpt = node.getCmpTrueOptEL() != null ? node.getCmpTrueOptEL() : node.getCmpTrueOpt();
//        Object falseOpt = node.getCmpFalseOptEL() != null ? node.getCmpFalseOptEL() : node.getCmpFalseOpt();
//        IfELWrapper ifELWrapper = ELBus.ifOpt(ELBusNode.node(node), trueOpt, falseOpt);
//        if(StrUtil.isNotBlank(node.getCmpId())){
//            ifELWrapper.id(node.getCmpId());
//        }
//        if(StrUtil.isNotBlank(node.getCmpTag())){
//            ifELWrapper.tag(node.getCmpTag());
//        }
//        if(StrUtil.isNotBlank(node.getCmpPre()) || StrUtil.isNotBlank(node.getCmpFinallyOpt())){
//            ThenELWrapper then = ELBus.then(ifELWrapper);
//            if(StrUtil.isNotBlank(node.getCmpPre())){
//                then.pre(node.getCmpPre());
//            }
//            if(StrUtil.isNotBlank(node.getCmpFinallyOpt())){
//                then.finallyOpt(node.getCmpFinallyOpt());
//            }
//            if(node.getCmpMaxWaitSeconds() != null){
//                then.maxWaitSeconds(node.getCmpMaxWaitSeconds());
//            }
//            wrapper = then;
//        }else{
//            if(node.getCmpMaxWaitSeconds() != null){
//                ifELWrapper.maxWaitSeconds(node.getCmpMaxWaitSeconds());
//            }
//            wrapper = ifELWrapper;
//        }
//        return this;
//    }

    public String toEL(){
        return wrapper.toEL();
    }

    public String toEL(boolean format){
        return wrapper.toEL(format);
    }

    public ELWrapper toELWrapper(){
        return wrapper;
    }

}
