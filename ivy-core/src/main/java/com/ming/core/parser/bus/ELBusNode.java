package com.ming.core.parser.bus;

import com.ming.core.parser.entity.node.Node;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.NodeELWrapper;

public class ELBusNode extends BaseELBus {

    public static NodeELWrapper node(String nodeId){
        return ELBus.node(nodeId);
    }

//    public static NodeELWrapper node(Node node){
//        NodeELWrapper wrapper = ELBus.node(node.getComponentId());
//        setData(wrapper, node);
//        return wrapper;
//    }

}
