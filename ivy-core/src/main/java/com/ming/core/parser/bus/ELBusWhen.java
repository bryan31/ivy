package com.ming.core.parser.bus;

import com.ming.core.parser.entity.node.Node;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.WhenELWrapper;

public class ELBusWhen extends BaseELBus {

    public static WhenELWrapper node(Object... objects){
        return ELBus.when(objects);
    }

//    public static WhenELWrapper node(Node node){
//        WhenELWrapper wrapper = ELBus.when();
//        wrapper.must(node.getWhenMust());
//        wrapper.any(node.getWhenAny());
//        wrapper.ignoreError(node.getWhenIgnoreError());
//        setId(wrapper, node);
//        setTag(wrapper, node);
//        setMaxWaitSeconds(wrapper, node);
//        return wrapper;
//    }

}
