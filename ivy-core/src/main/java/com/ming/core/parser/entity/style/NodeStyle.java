package com.ming.core.parser.entity.style;

import lombok.Data;

@Data
public class NodeStyle {

    private NodeStyleHandles handles;

}
