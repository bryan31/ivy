package com.ming.core.parser.bus;

import com.ming.core.parser.entity.node.Node;
import com.yomahub.liteflow.builder.el.*;

public class ELBusWhile extends BaseELBus {

    public static WhileELWrapper node(NodeELWrapper nodeElWrapper) {
        return ELBus.whileOpt(nodeElWrapper);
    }

    public static WhileELWrapper node(String nodeElWrapper) {
        return ELBus.whileOpt(nodeElWrapper);
    }

    public static WhileELWrapper node(AndELWrapper andElWrapper) {
        return ELBus.whileOpt(andElWrapper);
    }

    public static WhileELWrapper node(OrELWrapper orElWrapper) {
        return ELBus.whileOpt(orElWrapper);
    }

    public static WhileELWrapper node(NotELWrapper notElWrapper) {
        return ELBus.whileOpt(notElWrapper);
    }

//    public static WhileELWrapper node(Node node){
//        Object doOpt = node.getCmpDoOptEL() != null ? node.getCmpDoOptEL() : node.getCmpDoOpt();
//        Object breakOpt = node.getCmpBreakOptEL() != null ? node.getCmpBreakOptEL() : node.getCmpBreakOpt();
//        WhileELWrapper wrapper = ELBus.whileOpt(ELBusNode.node(node));
//        wrapper.doOpt(doOpt);
//        wrapper.breakOpt(breakOpt);
//        wrapper.parallel(node.getCmpParallel());
//        setId(wrapper, node);
//        setTag(wrapper, node);
//        setMaxWaitSeconds(wrapper, node);
//        return wrapper;
//    }

}
