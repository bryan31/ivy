package com.ming.core.parser.entity;

import lombok.Data;

@Data
public class Viewport {

    private Double x;
    private Double y;
    private Double zoom;

}
