package com.ming.core.parser.entity.style;

import lombok.Data;

@Data
public class NodeStyleHandlesBottom {

    private String type;
    private Boolean show;

}
