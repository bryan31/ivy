package com.ming.core.parser.entity.style;

import lombok.Data;

@Data
public class NodeStyleHandles {

    private NodeStyleHandlesLeft left;
    private NodeStyleHandlesRight right;
    private NodeStyleHandlesTop top;
    private NodeStyleHandlesBottom bottom;

}
