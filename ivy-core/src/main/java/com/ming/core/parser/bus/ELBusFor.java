package com.ming.core.parser.bus;

import com.ming.core.parser.entity.node.Node;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.ForELWrapper;
import com.yomahub.liteflow.builder.el.NodeELWrapper;

public class ELBusFor extends BaseELBus {

    public static ForELWrapper node(NodeELWrapper nodeElWrapper) {
        return ELBus.forOpt(nodeElWrapper);
    }

    public static ForELWrapper node(String node) {
        return ELBus.forOpt(node);
    }

//    public static ForELWrapper node(Node node){
//        Object doOpt = node.getCmpDoOptEL() != null ? node.getCmpDoOptEL() : node.getCmpDoOpt();
//        Object breakOpt = node.getCmpBreakOptEL() != null ? node.getCmpBreakOptEL() : node.getCmpBreakOpt();
//        ForELWrapper wrapper = ELBus.forOpt(ELBusNode.node(node));
//        wrapper.doOpt(doOpt);
//        wrapper.breakOpt(breakOpt);
//        wrapper.parallel(node.getCmpParallel());
//        setId(wrapper, node);
//        setTag(wrapper, node);
//        setMaxWaitSeconds(wrapper, node);
//        return wrapper;
//    }

}
