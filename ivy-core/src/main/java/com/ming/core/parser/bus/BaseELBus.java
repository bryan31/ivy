package com.ming.core.parser.bus;


import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ming.core.parser.entity.node.Node;
import com.yomahub.liteflow.builder.el.*;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

public class BaseELBus {

    protected static void setDoOpt(ELWrapper wrapper, Node node){
//        Object doOpt = node.getCmpDoOptEL() != null ? node.getCmpDoOptEL() : node.getCmpDoOpt();
//        setDoOpt(wrapper, doOpt);
    }

    protected static void setDoOpt(ELWrapper wrapper, Object doOpt){
        if(wrapper instanceof CatchELWrapper){
            CatchELWrapper elWrapper = (CatchELWrapper) wrapper;
            elWrapper.doOpt(doOpt);
        }
    }

    protected static void setId(ELWrapper wrapper, Node node){
//        setId(wrapper, node.getData().getCmpId());
    }

    protected static void setId(ELWrapper wrapper, String id){
        if(wrapper instanceof NodeELWrapper){
            ((NodeELWrapper) wrapper).id(id);
        }else if(wrapper instanceof AndELWrapper){
            ((AndELWrapper) wrapper).id(id);
        }else if(wrapper instanceof CatchELWrapper){
            ((CatchELWrapper) wrapper).id(id);
        }else if(wrapper instanceof FinallyELWrapper){
            ((FinallyELWrapper) wrapper).id(id);
        }else if(wrapper instanceof PreELWrapper){
            ((PreELWrapper) wrapper).id(id);
        }else if(wrapper instanceof NotELWrapper){
            ((NotELWrapper) wrapper).id(id);
        }else if(wrapper instanceof OrELWrapper){
            ((OrELWrapper) wrapper).id(id);
        }else if(wrapper instanceof WhenELWrapper){
            ((WhenELWrapper) wrapper).id(id);
        }else if(wrapper instanceof ThenELWrapper){
            ((ThenELWrapper) wrapper).id(id);
        }else if(wrapper instanceof WhileELWrapper){
            ((WhileELWrapper) wrapper).id(id);
        }else if(wrapper instanceof ForELWrapper){
            ((ForELWrapper) wrapper).id(id);
        }else if(wrapper instanceof IteratorELWrapper){
            ((IteratorELWrapper) wrapper).id(id);
        }
    }

    protected static void setTag(ELWrapper wrapper, Node node){
//        setTag(wrapper, node.getData().getCmpTag());
    }

    protected static void setTag(ELWrapper wrapper, String tag){
        if(wrapper instanceof NodeELWrapper){
            ((NodeELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof AndELWrapper){
            ((AndELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof CatchELWrapper){
            ((CatchELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof FinallyELWrapper){
            ((FinallyELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof PreELWrapper){
            ((PreELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof NotELWrapper){
            ((NotELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof OrELWrapper){
            ((OrELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof WhenELWrapper){
            ((WhenELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof ThenELWrapper){
            ((ThenELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof WhileELWrapper){
            ((WhileELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof ForELWrapper){
            ((ForELWrapper) wrapper).tag(tag);
        }else if(wrapper instanceof IteratorELWrapper){
            ((IteratorELWrapper) wrapper).tag(tag);
        }
    }

    protected static void setMaxWaitSeconds(ELWrapper wrapper, Node node){
//        setMaxWaitSeconds(wrapper, node.getData().getCmpMaxWaitSeconds());
    }

    protected static void setMaxWaitSeconds(ELWrapper wrapper, Integer maxWaitSeconds){
        if(wrapper instanceof NodeELWrapper){
            ((NodeELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof AndELWrapper){
            ((AndELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof CatchELWrapper){
            ((CatchELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof PreELWrapper){
            ((PreELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof NotELWrapper){
            ((NotELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof OrELWrapper){
            ((OrELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof WhenELWrapper){
            ((WhenELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof ThenELWrapper){
            ((ThenELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof WhileELWrapper){
            ((WhileELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof ForELWrapper){
            ((ForELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }else if(wrapper instanceof IteratorELWrapper){
            ((IteratorELWrapper) wrapper).maxWaitSeconds(maxWaitSeconds);
        }
    }

    protected static void setData(NodeELWrapper wrapper, Node node){
//        setData(wrapper, node.getData().getCmpDataName(), node.getData().getCmpData());
    }

    protected static void setData(NodeELWrapper wrapper, String dataName, String jsonData){
        if(StrUtil.isNotEmpty(jsonData)){
            try {
                Gson gson = new Gson();
                Type type;
                Object obj = null;
                if (jsonData.startsWith("{") && jsonData.endsWith("}")) {
                    type = new TypeToken<Map<String, Object>>(){}.getType(); // 获取Map的Type
                    obj = gson.fromJson(jsonData, type);
                } else if (jsonData.startsWith("[") && jsonData.endsWith("]")) {
                    type = new TypeToken<List<Map<String, Object>>>(){}.getType(); // 获取List的Type
                    obj = gson.fromJson(jsonData, type);
                }
            } catch (Exception e) {
                System.err.println("ELBusNode: Invalid JSON format.");
                System.err.println("dataName: " + dataName);
                System.err.println("jsonData: " + jsonData);
            }
            wrapper.data(dataName, jsonData);
        }
    }

    protected static void setData(NodeELWrapper wrapper, String dataName, Object data){
        if(data != null){
            wrapper.data(dataName, data);
        }
    }

    protected static void setData(NodeELWrapper wrapper, String dataName, Map<String, Object> jsonMap){
        if(CollUtil.isNotEmpty(jsonMap)){
            wrapper.data(dataName, jsonMap);
        }
    }
}
