package com.ming.core.query;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Options {

    private Integer page;

    private Integer itemsPerPage;

    private List<SortBy> sortBy;

    private List<Object> groupBy;

}
