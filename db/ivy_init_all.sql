/*
 Navicat Premium Data Transfer

 Source Server         : localhost@root@123456
 Source Server Type    : MySQL
 Source Server Version : 80032
 Source Host           : localhost:3306
 Source Schema         : ivy

 Target Server Type    : MySQL
 Target Server Version : 80032
 File Encoding         : 65001

 Date: 29/01/2024 09:53:28
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for ivy_chain
-- ----------------------------
DROP TABLE IF EXISTS `ivy_chain`;
CREATE TABLE `ivy_chain`  (
                              `id` bigint(0) NOT NULL AUTO_INCREMENT,
                              `chain_id` varchar(32)  NULL DEFAULT NULL COMMENT '链路ID',
                              `chain_name` varchar(32)  NULL DEFAULT NULL COMMENT '链路名称',
                              `ivy_el_id` bigint(0) NULL DEFAULT NULL COMMENT 'ivy_el表ID',
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ivy_chain
-- ----------------------------
INSERT INTO `ivy_chain` VALUES (1, 'ivy_chain', '自定义执行链路', 3);

-- ----------------------------
-- Table structure for ivy_cmp
-- ----------------------------
DROP TABLE IF EXISTS `ivy_cmp`;
CREATE TABLE `ivy_cmp`  (
                            `id` bigint(0) NOT NULL AUTO_INCREMENT,
                            `component_id` varchar(64)  NULL DEFAULT NULL,
                            `component_name` varchar(64)  NULL DEFAULT NULL,
                            `type` varchar(32)  NULL DEFAULT NULL,
                            `script` text  NULL,
                            `language` varchar(32)  NULL DEFAULT NULL,
                            `clazz` varchar(256)  NULL DEFAULT NULL,
                            `el` varchar(256)  NULL DEFAULT NULL,
                            `cmp_pre` varchar(256)  NULL DEFAULT NULL,
                            `cmp_finally_opt` varchar(256)  NULL DEFAULT NULL,
                            `cmp_id` varchar(32)  NULL DEFAULT NULL,
                            `cmp_tag` varchar(32)  NULL DEFAULT NULL,
                            `cmp_max_wait_seconds` int(0) NULL DEFAULT NULL,
                            `el_format` varchar(256)  NULL DEFAULT NULL,
                            `cmp_default_opt` varchar(256)  NULL DEFAULT NULL,
                            `cmp_to` varchar(256)  NULL DEFAULT NULL,
                            `cmp_true_opt` varchar(256)  NULL DEFAULT NULL,
                            `cmp_false_opt` varchar(256)  NULL DEFAULT NULL,
                            `cmp_parallel` tinyint(1) NULL DEFAULT NULL,
                            `cmp_do_opt` varchar(256)  NULL DEFAULT NULL,
                            `cmp_break_opt` varchar(256)  NULL DEFAULT NULL,
                            `cmp_data` text  NULL,
                            `cmp_data_name` varchar(32)  NULL DEFAULT NULL,
                            `fallback_id` bigint(0) NULL DEFAULT NULL,
                            PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ivy_cmp
-- ----------------------------
INSERT INTO `ivy_cmp` VALUES (11, 'CommonCmp', '普通组件', 'common', NULL, 'java', 'com.ming.liteflow.cmp.node.CommonCmp', 'THEN(node(\"CommonCmp\"));', NULL, NULL, NULL, NULL, NULL, 'THEN(\n	node(\"CommonCmp\")\n);', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ivy_cmp` VALUES (12, 'SwitchCmp', '选择组件', 'switch', '', '', 'com.ming.liteflow.cmp.node.SwitchCmp', 'THEN(PRE(node(\"a\")),SWITCH(node(\"SwitchCmp\")).TO(node(\"to\")).DEFAULT(node(\"default\")).id(\"id\").tag(\"tag\"),FINALLY(node(\"b\"))).maxWaitSeconds(10);', 'a', 'b', 'id', 'tag', NULL, 'THEN(\n	PRE(\n		node(\"a\")\n	),\n	SWITCH(node(\"SwitchCmp\")).TO(\n		node(\"to\")\n	).DEFAULT(\n		node(\"default\")\n	).id(\"id\").tag(\"tag\"),\n	FINALLY(\n		node(\"b\")\n	)\n).maxWaitSeconds(10);', 'default', 'to', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ivy_cmp` VALUES (13, 'IfCmp', '条件组件', 'if', '', '', 'com.ming.liteflow.cmp.node.IfCmp', 'THEN(PRE(node(\"a\")),IF(node(\"IfCmp\"),node(\"trueCmp\"),node(\"falseCmp\")).id(\"id\").tag(\"tag\"),FINALLY(node(\"b\"))).maxWaitSeconds(10);', 'a', 'b', 'id', 'tag', NULL, 'THEN(\n	PRE(\n		node(\"a\")\n	),\n	IF(\n		node(\"IfCmp\"),\n		node(\"trueCmp\"),\n		node(\"falseCmp\")\n	).id(\"id\").tag(\"tag\"),\n	FINALLY(\n		node(\"b\")\n	)\n).maxWaitSeconds(10);', NULL, NULL, 'trueCmp', 'falseCmp', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ivy_cmp` VALUES (14, 'ForCmp', '次数循环组件', 'for', NULL, NULL, 'com.ming.liteflow.cmp.node.ForCmp', 'FOR(node(\"ForCmp\")).parallel(true).DO(node(\"CommonCmp\")).BREAK(node(\"BreakCmp\")).id(\"id\").tag(\"tag\").maxWaitSeconds(10);', 'a', 'b', 'id', 'tag', NULL, 'FOR(\n	node(\"ForCmp\")\n).parallel(true).DO(\n	node(\"CommonCmp\")\n).BREAK(\n	node(\"BreakCmp\")\n).id(\"id\").tag(\"tag\").maxWaitSeconds(10);', NULL, NULL, NULL, NULL, 1, 'CommonCmp', 'BreakCmp', NULL, NULL, NULL);
INSERT INTO `ivy_cmp` VALUES (15, 'WhileCmp', '条件循环组件', 'while', NULL, NULL, 'com.ming.liteflow.cmp.node.WhileCmp', 'WHILE(node(\"WhileCmp\")).parallel(true).DO(node(\"doCmp\")).BREAK(node(\"BreakCmp\")).id(\"id\").tag(\"tag\").maxWaitSeconds(10);', 'a', 'b', 'id', 'tag', NULL, 'WHILE(\n	node(\"WhileCmp\")\n).parallel(true).DO(\n	node(\"doCmp\")\n).BREAK(\n	node(\"BreakCmp\")\n).id(\"id\").tag(\"tag\").maxWaitSeconds(10);', NULL, NULL, NULL, NULL, 1, 'doCmp', 'BreakCmp', NULL, NULL, NULL);
INSERT INTO `ivy_cmp` VALUES (16, 'IteratorCmp', '循环迭代组件', 'iterator', NULL, 'java', 'com.ming.liteflow.cmp.node.IteratorCmp', 'ITERATOR(node(\"IteratorCmp\")).parallel(true).DO(node(\"doCmp\")).id(\"id\").tag(\"tag\");', 'a', 'b', 'id', 'tag', NULL, 'ITERATOR(\n	node(\"IteratorCmp\")\n).parallel(true).DO(\n	node(\"doCmp\")\n).id(\"id\").tag(\"tag\");', NULL, NULL, NULL, NULL, 1, 'doCmp', NULL, NULL, NULL, NULL);
INSERT INTO `ivy_cmp` VALUES (17, 'BreakCmp', '循环跳出组件', 'break', '', '', 'com.ming.liteflow.cmp.node.BreakCmp', 'THEN(node(\"BreakCmp\"));', '', '', NULL, NULL, NULL, 'THEN(\n	node(\"BreakCmp\")\n);', '', '', '', '', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ivy_cmp` VALUES (19, 'ScriptCmp', '脚本组件', 'script', 'import com.yomahub.liteflow.script.ScriptExecuteWrap;\nimport com.yomahub.liteflow.script.body.JaninoCommonScriptBody;\n\npublic class ScriptNode implements JaninoCommonScriptBody {\n    @Override\n    public Void body(ScriptExecuteWrap scriptExecuteWrap) {\n        System.out.println(\"ScriptCmp executed!\");\n        return null;\n    }\n}\n', 'java', NULL, 'THEN(node(\"ScriptCmp\"));', NULL, NULL, NULL, NULL, NULL, 'THEN(\n	node(\"ScriptCmp\")\n);', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL);
INSERT INTO `ivy_cmp` VALUES (20, 'ScriptSwitchCmp', '选择脚本组件', 'switch_script', 'import com.yomahub.liteflow.script.ScriptExecuteWrap;\nimport com.yomahub.liteflow.script.body.JaninoSwitchScriptBody;\n\npublic class ScriptSwitchNode implements JaninoSwitchScriptBody {\n    @Override\n    public String body(ScriptExecuteWrap scriptExecuteWrap) {\n        System.out.println(\"ScriptSwitchCmp executed!\");\n        return \"a\";\n    }\n}', 'java', NULL, 'SWITCH(node(\"ScriptSwitchCmp\")).TO(node(\"a\"),node(\"b\")).DEFAULT(node(\"b\"));', NULL, NULL, NULL, NULL, NULL, 'SWITCH(node(\"ScriptSwitchCmp\")).TO(\n	node(\"a\"),\n	node(\"b\")\n).DEFAULT(\n	node(\"b\")\n);', 'b', 'a,b', NULL, NULL, NULL, NULL, NULL, '', '', NULL);
INSERT INTO `ivy_cmp` VALUES (21, 'ScriptIfCmp', '条件脚本组件', 'if_script', 'import com.yomahub.liteflow.script.ScriptExecuteWrap;\nimport com.yomahub.liteflow.script.body.JaninoIfScriptBody;\n\npublic class ScriptIfNode implements JaninoIfScriptBody {\n    @Override\n    public Boolean body(ScriptExecuteWrap scriptExecuteWrap) {\n        return false;\n    }\n}', 'java', NULL, 'IF(node(\"ScriptIfCmp\"),node(\"a\"),node(\"b\"));', NULL, NULL, NULL, NULL, NULL, 'IF(\n	node(\"ScriptIfCmp\"),\n	node(\"a\"),\n	node(\"b\")\n);', NULL, NULL, 'a', 'b', NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ivy_cmp` VALUES (22, 'ScriptForCmp', '次数循环脚本组件', 'for_script', 'import com.yomahub.liteflow.script.ScriptExecuteWrap;\nimport com.yomahub.liteflow.script.body.JaninoForScriptBody;\n\npublic class ScriptForNode implements JaninoForScriptBody {\n    @Override\n    public Integer body(ScriptExecuteWrap scriptExecuteWrap) {\n        return 5;\n    }\n}', 'java', NULL, 'FOR(node(\"ScriptForCmp\")).parallel(true).DO(node(\"a\")).BREAK(node(\"BreakCmp\"));', NULL, NULL, NULL, NULL, NULL, 'FOR(\n	node(\"ScriptForCmp\")\n).parallel(true).DO(\n	node(\"a\")\n).BREAK(\n	node(\"BreakCmp\")\n);', NULL, NULL, NULL, NULL, 1, 'a', 'BreakCmp', NULL, NULL, NULL);
INSERT INTO `ivy_cmp` VALUES (23, 'ScriptWhileCmp', '条件循环脚本组件', 'while_script', 'import com.yomahub.liteflow.script.ScriptExecuteWrap;\nimport com.yomahub.liteflow.script.body.JaninoWhileScriptBody;\n\npublic class ScriptWhileNode implements JaninoWhileScriptBody {\n    @Override\n    public Boolean body(ScriptExecuteWrap scriptExecuteWrap) {\n        return true;\n    }\n}', 'java', NULL, 'WHILE(node(\"ScriptWhileCmp\")).parallel(true).DO(node(\"a\")).BREAK(node(\"BreakCmp\"));', NULL, NULL, NULL, NULL, NULL, 'WHILE(\n	node(\"ScriptWhileCmp\")\n).parallel(true).DO(\n	node(\"a\")\n).BREAK(\n	node(\"BreakCmp\")\n);', NULL, NULL, NULL, NULL, 1, 'a', 'BreakCmp', NULL, NULL, NULL);
INSERT INTO `ivy_cmp` VALUES (24, 'ScriptBreakCmp', '循环跳出脚本组件', 'break_script', 'import com.yomahub.liteflow.script.ScriptExecuteWrap;\nimport com.yomahub.liteflow.script.body.JaninoBreakScriptBody;\n\npublic class ScriptBreakNode implements JaninoBreakScriptBody {\n    @Override\n    public Boolean body(ScriptExecuteWrap scriptExecuteWrap) {\n        return true;\n    }\n}', 'java', NULL, 'THEN(node(\"ScriptBreakCmp\"));', NULL, NULL, NULL, NULL, NULL, 'THEN(\n	node(\"ScriptBreakCmp\")\n);', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ivy_cmp` VALUES (30, 'FallbackCmp', '降级组件', 'fallback', NULL, 'java', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 33);
INSERT INTO `ivy_cmp` VALUES (32, 'CommonFallback2Cmp', '降级组件2', 'fallback', NULL, 'java', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', 34);
INSERT INTO `ivy_cmp` VALUES (33, 'CommonFallbackCmp1', '普通降级组件1', 'common', NULL, 'java', 'com.ming.liteflow.cmp.fallback.CommonFallbackCmp', 'THEN(node(\"CommonFallbackCmp1\"));', NULL, NULL, NULL, NULL, NULL, 'THEN(\n	node(\"CommonFallbackCmp1\")\n);', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL);
INSERT INTO `ivy_cmp` VALUES (34, 'CommonFallbackCmp2', '普通降级组件2', 'common', NULL, 'java', 'com.ming.liteflow.cmp.fallback.CommonFallback2Cmp', 'THEN(node(\"CommonFallbackCmp2\"));', NULL, NULL, NULL, NULL, NULL, 'THEN(\n	node(\"CommonFallbackCmp2\")\n);', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', '', NULL);
INSERT INTO `ivy_cmp` VALUES (35, 'CommonCmp2', '普通组件2', 'common', NULL, 'java', 'com.ming.liteflow.cmp.node.TestCmp', 'THEN(node(\"CommonCmp2\"));', NULL, NULL, NULL, NULL, NULL, 'THEN(\n	node(\"CommonCmp2\")\n);', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ivy_cmp` VALUES (36, 'IteratorCmp2', '循环迭代组件2', 'iterator', NULL, 'java', 'com.ming.liteflow.cmp.node.IteratorCmp2', 'ITERATOR(node(\"IteratorCmp2\")).parallel(true).DO(node(\"CommonCmp\"));', NULL, NULL, NULL, NULL, NULL, 'ITERATOR(\n	node(\"IteratorCmp2\")\n).parallel(true).DO(\n	node(\"CommonCmp\")\n);', NULL, NULL, NULL, NULL, 1, 'CommonCmp', NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for ivy_config
-- ----------------------------
DROP TABLE IF EXISTS `ivy_config`;
CREATE TABLE `ivy_config`  (
                               `id` bigint(0) NOT NULL AUTO_INCREMENT,
                               `config_id` varchar(32)  NULL DEFAULT NULL,
                               `config_name` varchar(32)  NULL DEFAULT NULL,
                               `config_type` int(0) NULL DEFAULT NULL,
                               `rule_source` text  NULL,
                               `rule_source_ext_data_map` text  NULL,
                               `rule_source_ext_data` text  NULL,
                               `enable` tinyint(1) NULL DEFAULT NULL,
                               `print_banner` tinyint(1) NULL DEFAULT NULL,
                               `zk_node` varchar(64)  NULL DEFAULT NULL,
                               `slot_size` int(0) NULL DEFAULT NULL,
                               `main_executor_works` int(0) NULL DEFAULT NULL,
                               `main_executor_class` varchar(128)  NULL DEFAULT NULL,
                               `request_id_generator_class` varchar(128)  NULL DEFAULT NULL,
                               `thread_executor_class` varchar(128)  NULL DEFAULT NULL,
                               `when_max_wait_time` int(0) NULL DEFAULT NULL,
                               `when_max_wait_time_unit` varchar(32)  NULL DEFAULT NULL,
                               `when_max_workers` int(0) NULL DEFAULT NULL,
                               `when_queue_limit` int(0) NULL DEFAULT NULL,
                               `parallel_loop_max_workers` int(0) NULL DEFAULT NULL,
                               `parallel_loop_queue_limit` int(0) NULL DEFAULT NULL,
                               `parallel_loop_executor_class` varchar(128)  NULL DEFAULT NULL,
                               `parse_on_start` tinyint(1) NULL DEFAULT NULL,
                               `retry_count` int(0) NULL DEFAULT NULL,
                               `support_multiple_type` tinyint(1) NULL DEFAULT NULL,
                               `node_executor_class` varchar(128)  NULL DEFAULT NULL,
                               `print_execution_log` tinyint(1) NULL DEFAULT NULL,
                               `enable_monitor_file` tinyint(1) NULL DEFAULT NULL,
                               `enable_log` tinyint(1) NULL DEFAULT NULL,
                               `queue_limit` int(0) NULL DEFAULT NULL,
                               `delay` bigint(0) NULL DEFAULT NULL,
                               `period` bigint(0) NULL DEFAULT NULL,
                               `fallback_cmp_enable` tinyint(1) NULL DEFAULT NULL,
                               `when_thread_pool_isolate` tinyint(1) NULL DEFAULT NULL,
                               `style_type` varchar(20)  NULL DEFAULT NULL,
                               `fast_load` tinyint(1) NULL DEFAULT NULL,
                               `rule_type` varchar(20)  NULL DEFAULT NULL COMMENT '规则类型【ruleSource:本地规则文件配置,ruleSourceZk:ZK规则文件配置源,ruleSourceSql:SQL数据库配置源,ruleSourceNacos:Nacos配置源,ruleSourceEtcd:Etcd配置源,ruleSourceApollo:Apollo配置源,ruleSourceRedisPoll:Redis配置源-轮询模式,ruleSourceRedisSub:Redis配置源-订阅模式,ruleSourceCustom:自定义配置源】',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 11  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ivy_config
-- ----------------------------
INSERT INTO `ivy_config` VALUES (9, 'ivy_config_sql_ext_data_map', 'SQL配置源1', 1, '', '{\"url\":\"jdbc:mysql://127.0.0.1:3306/ivy\",\"driverClassName\":\"com.mysql.cj.jdbc.Driver\",\"username\":\"root\",\"password\":\"123456\",\"applicationName\":\"demo1\",\"sqlLogEnabled\":false,\"pollingEnabled\":false,\"pollingIntervalSeconds\":60,\"pollingStartSeconds\":60,\"chainTableName\":\"ivy_lf_el_table\",\"chainApplicationNameField\":\"application_name\",\"chainNameField\":\"chain_name\",\"elDataField\":\"el_data\",\"chainEnableField\":true,\"scriptTableName\":\"ivy_lf_script_node_table\",\"scriptApplicationNameField\":\"application_name\",\"scriptIdField\":\"script_node_id\",\"scriptNameField\":\"script_node_name\",\"scriptDataField\":\"script_node_data\",\"scriptTypeField\":\"script_node_type\",\"scriptLanguageField\":\"script_language\",\"scriptEnableField\":true}', '', 1, 1, '', 1024, 64, 'com.yomahub.liteflow.thread.LiteFlowDefaultMainExecutorBuilder', 'com.yomahub.liteflow.flow.id.DefaultRequestIdGenerator', 'com.yomahub.liteflow.thread.LiteFlowDefaultWhenExecutorBuilder', 15000, 'MILLISECONDS', 16, 512, 16, 512, 'com.yomahub.liteflow.thread.LiteFlowDefaultParallelLoopExecutorBuilder', 1, 0, 0, 'com.yomahub.liteflow.flow.executor.DefaultNodeExecutor', 1, 0, 0, 200, 300000, 300000, 0, 0, NULL, 0, 'ruleSourceSql');
INSERT INTO `ivy_config` VALUES (10, 'ivy_config_sql_ext_data', 'SQL配置源2', 2, '', '{\"url\":\"jdbc:mysql://127.0.0.1:3306/ivy\",\"driverClassName\":\"com.mysql.cj.jdbc.Driver\",\"username\":\"root\",\"password\":\"123456\",\"applicationName\":\"demo\",\"sqlLogEnabled\":false,\"pollingEnabled\":false,\"pollingIntervalSeconds\":60,\"pollingStartSeconds\":60,\"chainTableName\":\"ivy_lf_el_table\",\"chainApplicationNameField\":\"application_name\",\"chainNameField\":\"chain_name\",\"elDataField\":\"el_data\",\"chainEnableField\":true,\"scriptTableName\":\"ivy_lf_script_node_table\",\"scriptApplicationNameField\":\"application_name\",\"scriptIdField\":\"script_node_id\",\"scriptNameField\":\"script_node_name\",\"scriptDataField\":\"script_node_data\",\"scriptTypeField\":\"script_node_type\",\"scriptLanguageField\":\"script_language\",\"scriptEnableField\":true}', '{\"url\":\"jdbc:mysql://127.0.0.1:3306/ivy\",\"driverClassName\":\"com.mysql.cj.jdbc.Driver\",\"username\":\"root\",\"password\":\"123456\",\"applicationName\":\"demo\",\"sqlLogEnabled\":false,\"pollingEnabled\":false,\"pollingIntervalSeconds\":60,\"pollingStartSeconds\":60,\"chainTableName\":\"ivy_lf_el_table\",\"chainApplicationNameField\":\"application_name\",\"chainNameField\":\"chain_name\",\"elDataField\":\"el_data\",\"chainEnableField\":true,\"scriptTableName\":\"ivy_lf_script_node_table\",\"scriptApplicationNameField\":\"application_name\",\"scriptIdField\":\"script_node_id\",\"scriptNameField\":\"script_node_name\",\"scriptDataField\":\"script_node_data\",\"scriptTypeField\":\"script_node_type\",\"scriptLanguageField\":\"script_language\",\"scriptEnableField\":true}', 1, 1, '', 1024, 64, 'com.yomahub.liteflow.thread.LiteFlowDefaultMainExecutorBuilder', 'com.yomahub.liteflow.flow.id.DefaultRequestIdGenerator', 'com.yomahub.liteflow.thread.LiteFlowDefaultWhenExecutorBuilder', 15000, 'MILLISECONDS', 16, 512, 16, 512, 'com.yomahub.liteflow.thread.LiteFlowDefaultParallelLoopExecutorBuilder', 1, 0, 0, 'com.yomahub.liteflow.flow.executor.DefaultNodeExecutor', 1, 0, 0, 200, 300000, 300000, 0, 0, NULL, 0, 'ruleSourceSql');

-- ----------------------------
-- Table structure for ivy_db_datasource
-- ----------------------------
DROP TABLE IF EXISTS `ivy_db_datasource`;
CREATE TABLE `ivy_db_datasource`  (
                                      `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                      `ds_bean_name` varchar(255)  NULL DEFAULT NULL COMMENT 'Bean名称，如：ds0',
                                      `ds_name` varchar(255)  NULL DEFAULT NULL COMMENT '数据源中文名称',
                                      `ds_desc` varchar(255)  NULL DEFAULT NULL COMMENT '描述信息',
                                      `driver_class_name` varchar(255)  NULL DEFAULT NULL COMMENT '数据库驱动路径',
                                      `jdbc_url` varchar(255)  NULL DEFAULT NULL COMMENT '数据库连接路径',
                                      `username` varchar(32)  NULL DEFAULT NULL COMMENT '用户名',
                                      `password` varchar(255)  NULL DEFAULT NULL COMMENT '密码',
                                      `disabled` tinyint(1) NULL DEFAULT NULL COMMENT '是否禁用:[0:启用,1:禁用]',
                                      `maximum_pool_size` int(0) NULL DEFAULT NULL COMMENT '连接池的最大连接数',
                                      `minimum_idle` int(0) NULL DEFAULT NULL COMMENT '连接池的最小空闲连接数',
                                      `idle_timeout` bigint(0) NULL DEFAULT NULL COMMENT '连接在池中保持空闲的最大时间，超过这个时间会被释放',
                                      `connection_timeout` bigint(0) NULL DEFAULT NULL COMMENT '获取连接的超时时间',
                                      `validation_timeout` bigint(0) NULL DEFAULT NULL COMMENT '在连接池中进行连接有效性验证的超时时间',
                                      `max_lifetime` bigint(0) NULL DEFAULT NULL COMMENT '连接在池中允许存活的最长时间',
                                      `connection_test_query` varchar(32)  NULL DEFAULT NULL COMMENT '用于测试连接是否有效的SQL查询语句',
                                      `connection_init_sql` varchar(32)  NULL DEFAULT NULL COMMENT '在连接被添加到连接池之前执行的 SQL 语句',
                                      `pool_name` varchar(32)  NULL DEFAULT NULL COMMENT '连接池的名称',
                                      `allow_pool_suspension` tinyint(1) NULL DEFAULT NULL COMMENT '是否允许连接池暂停',
                                      `read_only` tinyint(1) NULL DEFAULT NULL COMMENT '连接是否只读',
                                      `auto_commit` tinyint(1) NULL DEFAULT NULL COMMENT '是否自动提交',
                                      `register_mbeans` tinyint(1) NULL DEFAULT NULL COMMENT '是否注册MBeans',
                                      PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ivy_db_datasource
-- ----------------------------
INSERT INTO `ivy_db_datasource` VALUES (6, 'ivy_ds_1', '自定义数据源1', '自定义数据源1', 'com.mysql.cj.jdbc.Driver', 'jdbc:mysql://127.0.0.1:3306/ivy', 'root', '123456', 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `ivy_db_datasource` VALUES (8, 'ivy_ds_2', '自定义数据源2', '自定义数据源2', 'com.mysql.cj.jdbc.Driver', 'jdbc:mysql://127.0.0.1:3306/ivy', 'root', '123456', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- ----------------------------
-- Table structure for ivy_db_sql
-- ----------------------------
DROP TABLE IF EXISTS `ivy_db_sql`;
CREATE TABLE `ivy_db_sql`  (
                               `id` bigint(0) NOT NULL AUTO_INCREMENT,
                               `sql_manager_id` bigint(0) NULL DEFAULT NULL COMMENT 'ivy_db_sql_manager表ID',
                               `sql_language` varchar(20)  NULL DEFAULT NULL COMMENT 'SQL语言【DDL:数据定义语言,DML:数据操作语言,DCL:数据控制语言,TCL:事务控制语言】',
                               `table_name` varchar(32)  NULL DEFAULT NULL COMMENT '表名',
                               `table_type` varchar(20)  NULL DEFAULT NULL COMMENT '操作类型【single:单表操作,multiple:多表操作】',
                               `sql_id` varchar(32)  NULL DEFAULT NULL COMMENT 'sqlId,如：selectUserById',
                               `sql_name` varchar(32)  NULL DEFAULT NULL COMMENT 'sql名称',
                               `sql_desc` varchar(255)  NULL DEFAULT NULL COMMENT '描述信息',
                               `sql_type` varchar(12)  NULL DEFAULT NULL COMMENT 'SQL类型【select:select,insert:insert,update:update,delete:delete】',
                               `sql_template` text  NULL COMMENT 'sql模板',
                               `sql_json` text  NULL COMMENT 'sql配置',
                               `disabled` tinyint(1) NULL DEFAULT NULL COMMENT '是否禁用【0:启用,1:禁用】',
                               PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ivy_db_sql
-- ----------------------------
INSERT INTO `ivy_db_sql` VALUES (1, 1, 'DML', 'ivy_cmp', 'single', 'selectIvyCmpById', '根据ID查询IvyCmp', NULL, 'select', 'SELECT id,count(component_id) as component_id FROM ivy_cmp WHERE component_id IS NULL AND id NOT LIKE  ? GROUP BY  id  ORDER BY   cmp_break_opt  ASC   limit 0 , 10', '{\"column\":[{\"column\":\"id\",\"index\":1,\"type\":\"column\"},{\"column\":\"component_id\",\"index\":2,\"type\":\"count\"}],\"limit\":[{\"index\":1,\"pageSize\":10,\"startRow\":1,\"type\":\"limit\"}],\"orderBy\":[{\"column\":\"cmp_break_opt\",\"index\":1,\"type\":\"asc\"}],\"where\":[{\"column\":\"component_id\",\"index\":1,\"type\":\"andIsNull\"},{\"column\":\"id\",\"index\":2,\"type\":\"andNotLike\"}],\"groupBy\":[{\"column\":\"id\",\"index\":1,\"type\":\"groupBy\"}]}', 0);

-- ----------------------------
-- Table structure for ivy_db_sql_exec
-- ----------------------------
DROP TABLE IF EXISTS `ivy_db_sql_exec`;
CREATE TABLE `ivy_db_sql_exec`  (
                                    `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                    `sql_manager_id` varchar(255)  NULL DEFAULT NULL COMMENT 'ivy_db_sql_manager表ID',
                                    `table_name` varchar(255)  NULL DEFAULT NULL COMMENT '操作的表',
                                    `sql_id` varchar(255)  NULL DEFAULT NULL COMMENT 'sqlId,如：selectUserById',
                                    `sql_name` varchar(255)  NULL DEFAULT NULL COMMENT 'sql名称',
                                    `sql_desc` varchar(255)  NULL DEFAULT NULL COMMENT '描述信息',
                                    `sql_type` varchar(32)  NULL DEFAULT NULL COMMENT '操作类型：select，insert，update，delete',
                                    `sql_column` varchar(255)  NULL DEFAULT NULL COMMENT '查询的列',
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ivy_db_sql_exec_log
-- ----------------------------
DROP TABLE IF EXISTS `ivy_db_sql_exec_log`;
CREATE TABLE `ivy_db_sql_exec_log`  (
                                        `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                        PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ivy_db_sql_manager
-- ----------------------------
DROP TABLE IF EXISTS `ivy_db_sql_manager`;
CREATE TABLE `ivy_db_sql_manager`  (
                                       `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                       `datasource_id` bigint(0) NULL DEFAULT NULL COMMENT 'ivy_db_datasource表ID',
                                       `slave_datasource_ids` varchar(255)  NULL DEFAULT NULL COMMENT '从库数据源',
                                       `db_style` varchar(32)  NULL DEFAULT NULL COMMENT '数据库类型，默认mysql',
                                       `type` varchar(12)  NULL DEFAULT NULL COMMENT '管理器类型【single，masterSlave】',
                                       `sm_bean_name` varchar(255)  NULL DEFAULT NULL COMMENT 'Bean名称，如：mysqlManager',
                                       `sm_name` varchar(255)  NULL DEFAULT NULL COMMENT 'sql管理器中文名称',
                                       `sm_desc` varchar(255)  NULL DEFAULT NULL COMMENT '描述信息',
                                       `sql_interceptors` varchar(255)  NULL DEFAULT NULL COMMENT 'sql拦截器,可选多个',
                                       `name_conversion` varchar(32)  NULL DEFAULT NULL COMMENT '命名风格',
                                       `offset_start_zero` tinyint(1) NULL DEFAULT NULL COMMENT '分页是否从0开始【0：否，1：是】',
                                       `disabled` tinyint(1) NULL DEFAULT NULL COMMENT '是否禁用:[0:启用,1:禁用]',
                                       PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ivy_db_sql_manager
-- ----------------------------
INSERT INTO `ivy_db_sql_manager` VALUES (1, 6, NULL, 'MySqlStyle', 'single', 'mysqlSqlManager', 'mysql管理器', 'mysql管理器', 'SimpleDebugInterceptor,DebugInterceptor', 'UnderlinedNameConversion', 1, 0);
INSERT INTO `ivy_db_sql_manager` VALUES (2, 6, '6,8', 'MySqlStyle', 'masterSlave', 'masterSlaveSqlManager', '主从管理器', '主从管理器', 'DebugInterceptor,SimpleDebugInterceptor', 'UnderlinedNameConversion', 1, 0);

-- ----------------------------
-- Table structure for ivy_dict
-- ----------------------------
DROP TABLE IF EXISTS `ivy_dict`;
CREATE TABLE `ivy_dict`  (
                             `id` bigint(0) NOT NULL AUTO_INCREMENT,
                             `code` varchar(32)  NULL DEFAULT NULL,
                             `value` text  NULL,
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ivy_dict
-- ----------------------------
INSERT INTO `ivy_dict` VALUES (1, 'ivy_config', '{\"default\":{},\"properties\":\"#规则文件路径\\nliteflow.rule-source=config/flow.el.xml\\n#-----------------以下非必须-----------------\\n#liteflow是否开启，默认为true\\nliteflow.enable=true\\n#liteflow的banner是否开启，默认为true\\nliteflow.print-banner=true\\n#zkNode的节点，只有使用zk作为配置源的时候才起作用\\nliteflow.zk-node=/lite-flow/flow\\n#上下文的最大数量槽，默认值为1024\\nliteflow.slot-size=1024\\n#FlowExecutor的execute2Future的线程数，默认为64\\nliteflow.main-executor-works=64\\n#FlowExecutor的execute2Future的自定义线程池Builder，LiteFlow提供了默认的Builder\\nliteflow.main-executor-class=com.yomahub.liteflow.thread.LiteFlowDefaultMainExecutorBuilder\\n#自定义请求ID的生成类，LiteFlow提供了默认的生成类\\nliteflow.request-id-generator-class=com.yomahub.liteflow.flow.id.DefaultRequestIdGenerator\\n#并行节点的线程池Builder，LiteFlow提供了默认的Builder\\nliteflow.thread-executor-class=com.yomahub.liteflow.thread.LiteFlowDefaultWhenExecutorBuilder\\n#异步线程最长的等待时间(只用于when)，默认值为15000\\nliteflow.when-max-wait-time=15000\\n#异步线程最长的等待时间单位(只用于when)，默认值为MILLISECONDS，毫秒\\nliteflow.when-max-wait-time-unit=MILLISECONDS\\n#when节点全局异步线程池最大线程数，默认为16\\nliteflow.when-max-workers=16\\n#when节点全局异步线程池等待队列数，默认为512\\nliteflow.when-queue-limit=512\\n#并行循环子项线程池最大线程数，默认为16\\nliteflow.parallelLoop-max-workers=16\\n#并行循环子项线程池等待队列数，默认为512\\nliteflow.parallelLoop-queue-limit=512\\n#并行循环子项的线程池Builder，LiteFlow提供了默认的Builder\\nliteflow.parallelLoop-executor-class=com.yomahub.liteflow.test.customThreadPool.CustomThreadBuilder\\n#是否在启动的时候就解析规则，默认为true\\nliteflow.parse-on-start=true\\n#全局重试次数，默认为0\\nliteflow.retry-count=0\\n#是否支持不同类型的加载方式混用，默认为false\\nliteflow.support-multiple-type=false\\n#全局默认节点执行器\\nliteflow.node-executor-class=com.yomahub.liteflow.flow.executor.DefaultNodeExecutor\\n#是否打印执行中过程中的日志，默认为true\\nliteflow.print-execution-log=true\\n#是否开启本地文件监听，默认为false\\nliteflow.enable-monitor-file=false\\n#是否开启快速解析模式，默认为false\\nliteflow.fast-load=false\\n#监控是否开启，默认不开启\\nliteflow.monitor.enable-log=false\\n#监控队列存储大小，默认值为200\\nliteflow.monitor.queue-limit=200\\n#监控一开始延迟多少执行，默认值为300000毫秒，也就是5分钟\\nliteflow.monitor.delay=300000\\n#监控日志打印每过多少时间执行一次，默认值为300000毫秒，也就是5分钟\\nliteflow.monitor.period=300000\\n\",\"yaml\":\"liteflow:\\n  #规则文件路径\\n  rule-source: config/flow.el.xml\\n  #-----------------以下非必须-----------------\\n  #liteflow是否开启，默认为true\\n  enable: true\\n  #liteflow的banner打印是否开启，默认为true\\n  print-banner: true\\n  #zkNode的节点，只有使用zk作为配置源的时候才起作用，默认为/lite-flow/flow\\n  zk-node: /lite-flow/flow\\n  #上下文的最大数量槽，默认值为1024\\n  slot-size: 1024\\n  #FlowExecutor的execute2Future的线程数，默认为64\\n  main-executor-works: 64\\n  #FlowExecutor的execute2Future的自定义线程池Builder，LiteFlow提供了默认的Builder\\n  main-executor-class: com.yomahub.liteflow.thread.LiteFlowDefaultMainExecutorBuilder\\n  #自定义请求ID的生成类，LiteFlow提供了默认的生成类\\n  request-id-generator-class: com.yomahub.liteflow.flow.id.DefaultRequestIdGenerator\\n  #并行节点的线程池Builder，LiteFlow提供了默认的Builder\\n  thread-executor-class: com.yomahub.liteflow.thread.LiteFlowDefaultWhenExecutorBuilder\\n  #异步线程最长的等待时间(只用于when)，默认值为15000\\n  when-max-wait-time: 15000\\n  #异步线程最长的等待时间(只用于when)，默认值为MILLISECONDS，毫秒\\n  when-max-wait-time-unit: MILLISECONDS\\n  #when节点全局异步线程池最大线程数，默认为16\\n  when-max-workers: 16\\n  #并行循环子项线程池最大线程数，默认为16\\n  parallelLoop-max-workers: 16\\n  #并行循环子项线程池等待队列数，默认为512\\n  parallelLoop-queue-limit: 512\\n  #并行循环子项的线程池Builder，LiteFlow提供了默认的Builder\\n  parallelLoop-executor-class: com.yomahub.liteflow.thread.LiteFlowDefaultParallelLoopExecutorBuilder\\n  #when节点全局异步线程池等待队列数，默认为512\\n  when-queue-limit: 512\\n  #是否在启动的时候就解析规则，默认为true\\n  parse-on-start: true\\n  #全局重试次数，默认为0\\n  retry-count: 0\\n  #是否支持不同类型的加载方式混用，默认为false\\n  support-multiple-type: false\\n  #全局默认节点执行器\\n  node-executor-class: com.yomahub.liteflow.flow.executor.DefaultNodeExecutor\\n  #是否打印执行中过程中的日志，默认为true\\n  print-execution-log: true\\n  #是否开启本地文件监听，默认为false\\n  enable-monitor-file: false\\n  #是否开启快速解析模式，默认为false\\n  fast-load: false\\n  #简易监控配置选项\\n  monitor:\\n    #监控是否开启，默认不开启\\n    enable-log: false\\n    #监控队列存储大小，默认值为200\\n    queue-limit: 200\\n    #监控一开始延迟多少执行，默认值为300000毫秒，也就是5分钟\\n    delay: 300000\\n    #监控日志打印每过多少时间执行一次，默认值为300000毫秒，也就是5分钟\\n    period: 300000\\n\",\"xml\":\"<bean id=\\\"liteflowConfig\\\" class=\\\"com.yomahub.liteflow.property.LiteflowConfig\\\">\\n    <property name=\\\"ruleSource\\\" value=\\\"config/flow.el.xml\\\"/>\\n    <!-- ***********以下都不是必须的，都有默认值*********** -->\\n    <!-- liteflow是否开启,默认为true -->\\n    <property name=\\\"enable\\\" value=\\\"true\\\"/> \\n    <!-- liteflow的banner是否开启，默认为true -->\\n    <property name=\\\"printBanner\\\" value=\\\"true\\\"/> \\n    <!-- zkNode的节点，只有使用zk作为配置源的时候才起作用 -->\\n    <property name=\\\"zkNode\\\" value=\\\"/lite-flow/flow.xml\\\"/> \\n    <!-- 上下文的最大数量槽，默认值为1024 -->\\n    <property name=\\\"slotSize\\\" value=\\\"1024\\\"/> \\n    <!-- FlowExecutor的execute2Future的线程数，默认为64 -->\\n    <property name=\\\"mainExecutorWorks\\\" value=\\\"64\\\"/> \\n    <!-- FlowExecutor的execute2Future的自定义线程池Builder，LiteFlow提供了默认的Builder -->\\n    <property name=\\\"mainExecutorClass\\\" value=\\\"com.yomahub.liteflow.thread.LiteFlowDefaultMainExecutorBuilder\\\"/>\\n    <!-- 自定义请求ID的生成类，LiteFlow提供了默认的生成类 -->\\n    <property name=\\\"requestIdGeneratorClass\\\" value=\\\"com.yomahub.liteflow.flow.id.DefaultRequestIdGenerator\\\"/>\\n    <!-- 并行节点的线程池Builder，LiteFlow提供了默认的Builder -->\\n    <property name=\\\"threadExecutorClass\\\" value=\\\"com.yomahub.liteflow.thread.LiteFlowDefaultWhenExecutorBuilder\\\"/> \\n    <!-- 异步线程最长的等待时间(只用于when)，默认值为15000 -->\\n    <property name=\\\"whenMaxWaitTime\\\" value=\\\"15000\\\"/>\\n    <!-- 异步线程最长的等待时间(只用于when)，默认值为MILLISECONDS，毫秒 -->\\n    <property name=\\\"whenMaxWaitTimeUnit\\\" value=\\\"MILLISECONDS\\\"/>\\n    <!-- when节点全局异步线程池最大线程数，默认为16 -->\\n    <property name=\\\"whenMaxWorkers\\\" value=\\\"16\\\"/> \\n    <!-- when节点全局异步线程池等待队列数，默认为512 -->\\n    <property name=\\\"whenQueueLimit\\\" value=\\\"512\\\"/>\\n    <!-- 并行循环子项线程池最大线程数，默认为16-->\\n    <property name=\\\"parallelMaxWorkers\\\" value=\\\"16\\\"/>\\n    <!-- 并行循环子项线程池等待队列数，默认为512-->\\n    <property name=\\\"parallelQueueLimit\\\" value=\\\"512\\\"/>\\n    <!-- 并行循环子项的线程池Builder，LiteFlow提供了默认的Builder-->\\n    <property name=\\\"parallelLoopExecutorClass\\\" value=\\\"com.yomahub.liteflow.thread.LiteFlowDefaultParallelLoopExecutorBuilder\\\"/>\\n    <!-- 是否在启动的时候就解析规则，默认为true -->\\n    <property name=\\\"parseOnStart\\\" value=\\\"true\\\"/>\\n    <!-- 全局重试次数，默认为0 -->\\n    <property name=\\\"retryCount\\\" value=\\\"0\\\"/>\\n    <!-- 是否支持不同类型的加载方式混用，默认为false -->\\n    <property name=\\\"supportMultipleType\\\" value=\\\"false\\\"/>\\n    <!-- 全局默认节点执行器 -->\\n    <property name=\\\"nodeExecutorClass\\\" value=\\\"com.yomahub.liteflow.flow.executor.DefaultNodeExecutor\\\"/>\\n    <!-- 是否打印执行中过程中的日志，默认为true -->\\n    <property name=\\\"printExecutionLog\\\" value=\\\"true\\\"/>\\n    <!-- 是否开启本地文件监听，默认为false -->\\n    <property name=\\\"enableMonitorFile\\\" value=\\\"false\\\"/>\\n    <!-- 是否开启快速解析模式，默认为false -->\\n    <property name=\\\"fastLoad\\\" value=\\\"false\\\"/>\\n    <!-- 监控是否开启，默认不开启 -->\\n    <property name=\\\"enableLog\\\" value=\\\"false\\\"/>\\n    <!-- 监控队列存储大小，默认值为200 -->\\n    <property name=\\\"queueLimit\\\" value=\\\"200\\\"/>\\n    <!-- 监控一开始延迟多少执行，默认值为300000毫秒，也就是5分钟 -->\\n    <property name=\\\"period\\\" value=\\\"300000\\\"/>\\n    <!-- 监控日志打印每过多少时间执行一次，默认值为300000毫秒，也就是5分钟 -->\\n    <property name=\\\"delay\\\" value=\\\"300000\\\"/>\\n</bean>\\n\",\"json\":\"\"}');

-- ----------------------------
-- Table structure for ivy_dynamic_class
-- ----------------------------
DROP TABLE IF EXISTS `ivy_dynamic_class`;
CREATE TABLE `ivy_dynamic_class`  (
                                      `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                      `class_id` varchar(128)  NULL DEFAULT NULL COMMENT '动态类ID',
                                      `package_path` varchar(32)  NULL DEFAULT NULL COMMENT '包路径',
                                      `class_name` varchar(32)  NULL DEFAULT NULL COMMENT '动态类名称',
                                      `class_loader_type` int(0) NULL DEFAULT NULL COMMENT '加载器类型',
                                      `source_code` text  NULL COMMENT 'Java源码',
                                      `version` int(0) NULL DEFAULT NULL COMMENT '版本号',
                                      `source_cmp_id` varchar(32)  NULL DEFAULT NULL COMMENT '组件ID',
                                      `source_class_name` varchar(32)  NULL DEFAULT NULL COMMENT '源码类名',
                                      `class_type` int(0) NULL DEFAULT NULL COMMENT 'class类型【0:普通类,1:普通组件类,2:选择组件类,3:条件组件类,4:次数循环组件类,5:条件循环组件类,6:迭代循环组件类,7:退出循环组件类】',
                                      `is_fallback` int(0) NULL DEFAULT NULL COMMENT '是否为降级组件【0:非降级组件,1:降级组件】',
                                      PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ivy_dynamic_class
-- ----------------------------
INSERT INTO `ivy_dynamic_class` VALUES (1, 'com.ivy.cla.IvyDynamicClass', 'com.ivy.cla', '普通类', 0, 'package com.ivy.cla;\n\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;\n\npublic class IvyDynamicClass {\n\n	private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicClass.class);\n\n	public String processTest(String str) {\n		LOG.info(\"IvyDynamicClass executed!\");\n		System.out.println(\"IvyDynamicClass executed!\");\n		return str;\n	}\n\n}', 15, NULL, 'IvyDynamicClass', 0, NULL);
INSERT INTO `ivy_dynamic_class` VALUES (2, 'com.ivy.cla.IvyDynamicCmpCommon', 'com.ivy.cla', '普通组件类', 0, 'package com.ivy.cla;\n\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;\nimport com.yomahub.liteflow.annotation.LiteflowComponent;\nimport com.yomahub.liteflow.core.NodeComponent;\n\n@LiteflowComponent(\"IvyDynamicCmpCommon\")\npublic class IvyDynamicCmpCommon extends NodeComponent {\n\n	private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpCommon.class);\n\n	@Override\n	public void process() {\n		LOG.info(\"IvyDynamicCmpCommon executed!\");\n		System.out.println(\"IvyDynamicCmpCommon executed!\");\n	}\n\n}', 12, 'IvyDynamicCmpCommon', 'IvyDynamicCmpCommon', 1, 0);
INSERT INTO `ivy_dynamic_class` VALUES (4, 'com.ivy.cla.IvyDynamicCmpCommonFallback', 'com.ivy.cla', '普通降级组件类', 0, 'package com.ivy.cla;\n\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;\nimport com.yomahub.liteflow.annotation.FallbackCmp;\nimport com.yomahub.liteflow.annotation.LiteflowComponent;\nimport com.yomahub.liteflow.core.NodeComponent;\n\n@FallbackCmp\n@LiteflowComponent(\"IvyDynamicCmpCommonFallback\")\npublic class IvyDynamicCmpCommonFallback extends NodeComponent {\n\n	private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpCommonFallback.class);\n\n	@Override\n	public void process() {\n		LOG.info(\"IvyDynamicCmpCommonFallback executed!\");\n		System.out.println(\"IvyDynamicCmpCommonFallback executed!\");\n	}\n\n}', 3, 'IvyDynamicCmpCommonFallback', 'IvyDynamicCmpCommonFallback', 1, 1);
INSERT INTO `ivy_dynamic_class` VALUES (5, 'com.ivy.cla.IvyDynamicCmpSwitch', 'com.ivy.cla', '选择组件类', 0, 'package com.ivy.cla;\n\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;\nimport com.yomahub.liteflow.annotation.LiteflowComponent;\nimport com.yomahub.liteflow.core.NodeSwitchComponent;\n\n@LiteflowComponent(\"IvyDynamicCmpSwitch\")\npublic class IvyDynamicCmpSwitch extends NodeSwitchComponent {\n\n    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpSwitch.class);\n\n    @Override\n    public String processSwitch() throws Exception {\n        LOG.info(\"IvyDynamicCmpSwitch executed!\");\n        System.out.println(\"IvyDynamicCmpSwitch executed!\");\n        return \"a\";\n    }\n}', 2, 'IvyDynamicCmpSwitch', 'IvyDynamicCmpSwitch', 2, 0);
INSERT INTO `ivy_dynamic_class` VALUES (6, 'com.ivy.cla.IvyDynamicCmpSwitchFallback', 'com.ivy.cla', '选择降级组件类', 0, 'package com.ivy.cla;\n\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;\nimport com.yomahub.liteflow.annotation.FallbackCmp;\nimport com.yomahub.liteflow.annotation.LiteflowComponent;\nimport com.yomahub.liteflow.core.NodeSwitchComponent;\n\n@FallbackCmp\n@LiteflowComponent(\"IvyDynamicCmpSwitchFallback\")\npublic class IvyDynamicCmpSwitchFallback extends NodeSwitchComponent {\n\n    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpSwitchFallback.class);\n\n    @Override\n    public String processSwitch() throws Exception {\n        LOG.info(\"IvyDynamicCmpSwitchFallback executed!\");\n        System.out.println(\"IvyDynamicCmpSwitchFallback executed!\");\n        return \"a\";\n    }\n}', 2, 'IvyDynamicCmpSwitchFallback', 'IvyDynamicCmpSwitchFallback', 2, 1);
INSERT INTO `ivy_dynamic_class` VALUES (7, 'com.ivy.cla.IvyDynamicCmpIf', 'com.ivy.cla', '条件组件类', 0, 'package com.ivy.cla;\n\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;\nimport com.yomahub.liteflow.annotation.LiteflowComponent;\nimport com.yomahub.liteflow.core.NodeIfComponent;\n\n@LiteflowComponent(\"IvyDynamicCmpIf\")\npublic class IvyDynamicCmpIf extends NodeIfComponent {\n\n    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpIf.class);\n\n    @Override\n    public boolean processIf() throws Exception {\n        LOG.info(\"IvyDynamicCmpIf executed3!\");\n        System.out.println(\"IvyDynamicCmpIf executed3!\");\n        return true;\n    }\n}', 2, 'IvyDynamicCmpIf', 'IvyDynamicCmpIf', 3, 0);
INSERT INTO `ivy_dynamic_class` VALUES (8, 'com.ivy.cla.IvyDynamicCmpIfFallback', 'com.ivy.cla', '条件降级组件类', 0, 'package com.ivy.cla;\n\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;\nimport com.yomahub.liteflow.annotation.FallbackCmp;\nimport com.yomahub.liteflow.annotation.LiteflowComponent;\nimport com.yomahub.liteflow.core.NodeIfComponent;\n\n@FallbackCmp\n@LiteflowComponent(\"IvyDynamicCmpIfFallback\")\npublic class IvyDynamicCmpIfFallback extends NodeIfComponent {\n\n    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpIfFallback.class);\n\n    @Override\n    public boolean processIf() throws Exception {\n        LOG.info(\"IvyDynamicCmpIfFallback executed!\");\n        System.out.println(\"IvyDynamicCmpIfFallback executed!\");\n        return true;\n    }\n}', 1, 'IvyDynamicCmpIfFallback', 'IvyDynamicCmpIfFallback', 3, 1);
INSERT INTO `ivy_dynamic_class` VALUES (9, 'com.ivy.cla.IvyDynamicCmpFor', 'com.ivy.cla', '次数循环组件类', 0, 'package com.ivy.cla;\n\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;\nimport com.yomahub.liteflow.annotation.LiteflowComponent;\nimport com.yomahub.liteflow.core.NodeForComponent;\n\n@LiteflowComponent(\"IvyDynamicCmpFor\")\npublic class IvyDynamicCmpFor extends NodeForComponent {\n\n    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpFor.class);\n\n    @Override\n    public int processFor() throws Exception {\n        LOG.info(\"IvyDynamicCmpFor executed!\");\n        System.out.println(\"IvyDynamicCmpFor executed!\");\n        return 3;\n    }\n}', 1, 'IvyDynamicCmpFor', 'IvyDynamicCmpFor', 4, 0);
INSERT INTO `ivy_dynamic_class` VALUES (10, 'com.ivy.cla.IvyDynamicCmpForFallback', 'com.ivy.cla', '次数循环降级组件类', 0, 'package com.ivy.cla;\n\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;\nimport com.yomahub.liteflow.annotation.FallbackCmp;\nimport com.yomahub.liteflow.annotation.LiteflowComponent;\nimport com.yomahub.liteflow.core.NodeForComponent;\n\n@FallbackCmp\n@LiteflowComponent(\"IvyDynamicCmpForFallback\")\npublic class IvyDynamicCmpForFallback extends NodeForComponent {\n\n    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpForFallback.class);\n\n    @Override\n    public int processFor() throws Exception {\n        LOG.info(\"IvyDynamicCmpForFallback executed!\");\n        System.out.println(\"IvyDynamicCmpForFallback executed!\");\n        return 3;\n    }\n}', 1, 'IvyDynamicCmpForFallback', 'IvyDynamicCmpForFallback', 4, 1);
INSERT INTO `ivy_dynamic_class` VALUES (11, 'com.ivy.cla.IvyDynamicCmpWhile', 'com.ivy.cla', '条件循环组件类', 0, 'package com.ivy.cla;\n\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;\nimport com.yomahub.liteflow.annotation.LiteflowComponent;\nimport com.yomahub.liteflow.core.NodeWhileComponent;\n\n@LiteflowComponent(\"IvyDynamicCmpWhile\")\npublic class IvyDynamicCmpWhile extends NodeWhileComponent {\n\n    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpWhile.class);\n\n    @Override\n    public boolean processWhile() throws Exception {\n        LOG.info(\"IvyDynamicCmpWhile executed!\");\n        System.out.println(\"IvyDynamicCmpWhile executed!\");\n        return false;\n    }\n}', 1, 'IvyDynamicCmpWhile', 'IvyDynamicCmpWhile', 5, 0);
INSERT INTO `ivy_dynamic_class` VALUES (12, 'com.ivy.cla.IvyDynamicCmpWhileFallback', 'com.ivy.cla', '条件循环降级组件类', 0, 'package com.ivy.cla;\n\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;\nimport com.yomahub.liteflow.annotation.FallbackCmp;\nimport com.yomahub.liteflow.annotation.LiteflowComponent;\nimport com.yomahub.liteflow.core.NodeWhileComponent;\n\n@FallbackCmp\n@LiteflowComponent(\"IvyDynamicCmpWhileFallback\")\npublic class IvyDynamicCmpWhileFallback extends NodeWhileComponent {\n\n    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpWhileFallback.class);\n\n    @Override\n    public boolean processWhile() throws Exception {\n        LOG.info(\"IvyDynamicCmpWhileFallback executed!\");\n        System.out.println(\"IvyDynamicCmpWhileFallback executed!\");\n        return false;\n    }\n}', 1, 'IvyDynamicCmpWhileFallback', 'IvyDynamicCmpWhileFallback', 5, 1);
INSERT INTO `ivy_dynamic_class` VALUES (13, 'com.ivy.cla.IvyDynamicCmpIterator', 'com.ivy.cla', '迭代循环组件类', 0, 'package com.ivy.cla;\n\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;\nimport com.yomahub.liteflow.annotation.LiteflowComponent;\nimport com.yomahub.liteflow.core.NodeIteratorComponent;\n\nimport java.util.ArrayList;\nimport java.util.Iterator;\nimport java.util.List;\n\n@LiteflowComponent(\"IvyDynamicCmpIterator\")\npublic class IvyDynamicCmpIterator extends NodeIteratorComponent {\n\n    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpIterator.class);\n\n    @Override\n    public Iterator<?> processIterator() throws Exception {\n        LOG.info(\"IvyDynamicCmpIterator executed!\");\n        System.out.println(\"IvyDynamicCmpIterator executed!\");\n        List<String> list = new ArrayList<>(){{\n            add(\"jack\");add(\"mary\");add(\"tom\");\n        }};\n        return list.iterator();\n    }\n}', 1, 'IvyDynamicCmpIterator', 'IvyDynamicCmpIterator', 6, 0);
INSERT INTO `ivy_dynamic_class` VALUES (14, 'com.ivy.cla.IvyDynamicCmpIteratorFallback', 'com.ivy.cla', '迭代循环降级组件类', 0, 'package com.ivy.cla;\n\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;\nimport com.yomahub.liteflow.annotation.FallbackCmp;\nimport com.yomahub.liteflow.annotation.LiteflowComponent;\nimport com.yomahub.liteflow.core.NodeIteratorComponent;\n\nimport java.util.ArrayList;\nimport java.util.Iterator;\nimport java.util.List;\n\n@FallbackCmp\n@LiteflowComponent(\"IvyDynamicCmpIteratorFallback\")\npublic class IvyDynamicCmpIteratorFallback extends NodeIteratorComponent {\n\n    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpIteratorFallback.class);\n\n    @Override\n    public Iterator<?> processIterator() throws Exception {\n        LOG.info(\"IvyDynamicCmpIteratorFallback executed!\");\n        System.out.println(\"IvyDynamicCmpIteratorFallback executed!\");\n        List<String> list = new ArrayList<>(){{\n            add(\"jack\");add(\"mary\");add(\"tom\");\n        }};\n        return list.iterator();\n    }\n}', 1, 'IvyDynamicCmpIteratorFallback', 'IvyDynamicCmpIteratorFallback', 6, 1);
INSERT INTO `ivy_dynamic_class` VALUES (15, 'com.ivy.cla.IvyDynamicCmpBreak', 'com.ivy.cla', '退出循环组件类', 0, 'package com.ivy.cla;\n\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;\nimport com.yomahub.liteflow.annotation.LiteflowComponent;\nimport com.yomahub.liteflow.core.NodeBreakComponent;\n\n@LiteflowComponent(\"IvyDynamicCmpBreak\")\npublic class IvyDynamicCmpBreak extends NodeBreakComponent {\n\n	private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpBreak.class);\n\n	@Override\n	public boolean processBreak() {\n		LOG.info(\"IvyDynamicCmpBreak executed!\");\n		System.out.println(\"IvyDynamicCmpBreak executed!\");\n		return true;\n	}\n}', 1, 'IvyDynamicCmpBreak', 'IvyDynamicCmpBreak', 7, 0);
INSERT INTO `ivy_dynamic_class` VALUES (16, 'com.ivy.cla.IvyDynamicCmpBreakFallback', 'com.ivy.cla', '退出循环降级组件类', 0, 'package com.ivy.cla;\n\nimport org.slf4j.Logger;\nimport org.slf4j.LoggerFactory;\nimport com.yomahub.liteflow.annotation.FallbackCmp;\nimport com.yomahub.liteflow.annotation.LiteflowComponent;\nimport com.yomahub.liteflow.core.NodeBreakComponent;\n\n@FallbackCmp\n@LiteflowComponent(\"IvyDynamicCmpBreakFallback\")\npublic class IvyDynamicCmpBreakFallback extends NodeBreakComponent {\n\n	private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpBreakFallback.class);\n\n	@Override\n	public boolean processBreak() {\n		LOG.info(\"IvyDynamicCmpBreakFallback executed!\");\n		System.out.println(\"IvyDynamicCmpBreakFallback executed!\");\n		return true;\n	}\n}', 1, 'IvyDynamicCmpBreakFallback', 'IvyDynamicCmpBreakFallback', 7, 1);

-- ----------------------------
-- Table structure for ivy_el
-- ----------------------------
DROP TABLE IF EXISTS `ivy_el`;
CREATE TABLE `ivy_el`  (
                           `id` bigint(0) NOT NULL AUTO_INCREMENT,
                           `el_id` varchar(32)  NULL DEFAULT NULL COMMENT 'EL表达式ID',
                           `el_name` varchar(32)  NULL DEFAULT NULL COMMENT 'EL表达式名称',
                           `el` text  NULL COMMENT 'EL表达式',
                           `flow_json` text  NULL COMMENT '流程数据',
                           `source_json` text  NULL COMMENT '原始数据',
                           `executor_id` bigint(0) NULL DEFAULT NULL COMMENT 'Executor执行器ID',
                           PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 10  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ivy_el
-- ----------------------------
INSERT INTO `ivy_el` VALUES (5, 'el3', '测试流程3', NULL, '{\"nodes\":[{\"id\":\"31a87d18-f744-4bda-b8e6-1500b4ccc842\",\"type\":\"NodeScriptComponent\",\"properties\":{\"id\":20,\"cmpData\":\"\",\"cmpDataName\":\"\",\"cmpBreakOpt\":null,\"cmpDoOpt\":null,\"cmpParallel\":null,\"cmpFalseOpt\":null,\"cmpTrueOpt\":null,\"cmpDefaultOpt\":\"b\",\"cmpTo\":\"a,b\",\"cmpMaxWaitSeconds\":null,\"cmpTag\":null,\"cmpId\":null,\"cmpFinallyOpt\":null,\"cmpPre\":null,\"fallbackId\":null,\"clazz\":null,\"language\":\"java\",\"script\":\"import com.yomahub.liteflow.script.ScriptExecuteWrap;\\nimport com.yomahub.liteflow.script.body.JaninoSwitchScriptBody;\\n\\npublic class ScriptSwitchNode implements JaninoSwitchScriptBody {\\n    @Override\\n    public String body(ScriptExecuteWrap scriptExecuteWrap) {\\n        System.out.println(\\\"ScriptSwitchCmp executed!\\\");\\n        return \\\"a\\\";\\n    }\\n}\",\"type\":\"switch_script\",\"componentName\":\"选择脚本组件\",\"componentId\":\"ScriptSwitchCmp\",\"ui\":\"node-red\"},\"text\":\"选择脚本组件\"},{\"id\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"type\":\"common\",\"properties\":{\"componentId\":\"aa\",\"componentName\":\"aa\",\"ui\":\"node-red\"},\"text\":\"aa\"},{\"id\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"type\":\"common\",\"properties\":{\"componentId\":\"bb\",\"componentName\":\"bb\",\"ui\":\"node-red\"},\"text\":\"bb\"},{\"id\":\"f2a79e6a-a42f-4518-ab8f-66eea4989d6e\",\"type\":\"NodeScriptComponent\",\"properties\":{\"id\":21,\"cmpData\":null,\"cmpDataName\":null,\"cmpBreakOpt\":null,\"cmpDoOpt\":null,\"cmpParallel\":null,\"cmpFalseOpt\":\"b\",\"cmpTrueOpt\":\"a\",\"cmpDefaultOpt\":null,\"cmpTo\":null,\"cmpMaxWaitSeconds\":null,\"cmpTag\":null,\"cmpId\":null,\"cmpFinallyOpt\":null,\"cmpPre\":null,\"fallbackId\":null,\"clazz\":null,\"language\":\"java\",\"script\":\"import com.yomahub.liteflow.script.ScriptExecuteWrap;\\nimport com.yomahub.liteflow.script.body.JaninoIfScriptBody;\\n\\npublic class ScriptIfNode implements JaninoIfScriptBody {\\n    @Override\\n    public Boolean body(ScriptExecuteWrap scriptExecuteWrap) {\\n        return false;\\n    }\\n}\",\"type\":\"if_script\",\"componentName\":\"条件脚本组件\",\"componentId\":\"ScriptIfCmp\",\"ui\":\"node-red\"},\"text\":\"条件脚本组件\"},{\"id\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"type\":\"common\",\"properties\":{\"componentId\":\"cc\",\"componentName\":\"cc\",\"ui\":\"node-red\"},\"text\":\"cc\"},{\"id\":\"44faa8fe-16a2-4f09-b992-2d0234b01563\",\"type\":\"common\",\"properties\":{\"componentId\":\"dd\",\"componentName\":\"dd\",\"ui\":\"node-red\"},\"text\":\"dd\"},{\"id\":\"c9f90d12-0738-4db0-89f7-db427ad7976d\",\"type\":\"common\",\"properties\":{\"componentId\":\"ff\",\"componentName\":\"ff\",\"ui\":\"node-red\"},\"text\":\"ff\"},{\"id\":\"6c8d7aad-4fbf-4db2-a5c4-0c0531ac82c9\",\"type\":\"common\",\"properties\":{\"componentId\":\"gg\",\"componentName\":\"gg\",\"ui\":\"node-red\"},\"text\":\"gg\"}],\"edges\":[{\"sourceNodeId\":\"31a87d18-f744-4bda-b8e6-1500b4ccc842\",\"targetNodeId\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"31a87d18-f744-4bda-b8e6-1500b4ccc842\",\"targetNodeId\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"targetNodeId\":\"f2a79e6a-a42f-4518-ab8f-66eea4989d6e\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"f2a79e6a-a42f-4518-ab8f-66eea4989d6e\",\"targetNodeId\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"properties\":{\"linkType\":1,\"ifType\":\"true\"},\"text\":\"true\"},{\"sourceNodeId\":\"f2a79e6a-a42f-4518-ab8f-66eea4989d6e\",\"targetNodeId\":\"44faa8fe-16a2-4f09-b992-2d0234b01563\",\"properties\":{\"linkType\":1,\"ifType\":\"false\"},\"text\":\"false\"},{\"sourceNodeId\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"targetNodeId\":\"c9f90d12-0738-4db0-89f7-db427ad7976d\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"targetNodeId\":\"c9f90d12-0738-4db0-89f7-db427ad7976d\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"c9f90d12-0738-4db0-89f7-db427ad7976d\",\"targetNodeId\":\"6c8d7aad-4fbf-4db2-a5c4-0c0531ac82c9\",\"properties\":{},\"text\":\"\"}]}', '{\"nodes\":[{\"id\":\"31a87d18-f744-4bda-b8e6-1500b4ccc842\",\"type\":\"NodeScriptComponent\",\"x\":240,\"y\":120,\"properties\":{\"id\":20,\"cmpData\":\"\",\"cmpDataName\":\"\",\"cmpBreakOpt\":null,\"cmpDoOpt\":null,\"cmpParallel\":null,\"cmpFalseOpt\":null,\"cmpTrueOpt\":null,\"cmpDefaultOpt\":\"b\",\"cmpTo\":\"a,b\",\"cmpMaxWaitSeconds\":null,\"cmpTag\":null,\"cmpId\":null,\"cmpFinallyOpt\":null,\"cmpPre\":null,\"fallbackId\":null,\"clazz\":null,\"language\":\"java\",\"script\":\"import com.yomahub.liteflow.script.ScriptExecuteWrap;\\nimport com.yomahub.liteflow.script.body.JaninoSwitchScriptBody;\\n\\npublic class ScriptSwitchNode implements JaninoSwitchScriptBody {\\n    @Override\\n    public String body(ScriptExecuteWrap scriptExecuteWrap) {\\n        System.out.println(\\\"ScriptSwitchCmp executed!\\\");\\n        return \\\"a\\\";\\n    }\\n}\",\"type\":\"switch_script\",\"componentName\":\"选择脚本组件\",\"componentId\":\"ScriptSwitchCmp\",\"ui\":\"node-red\"},\"text\":{\"x\":260,\"y\":122,\"value\":\"选择脚本组件\"}},{\"id\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"type\":\"common\",\"x\":420,\"y\":80,\"properties\":{\"componentId\":\"aa\",\"componentName\":\"aa\",\"ui\":\"node-red\"},\"text\":{\"x\":440,\"y\":82,\"value\":\"aa\"}},{\"id\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"type\":\"common\",\"x\":420,\"y\":180,\"properties\":{\"componentId\":\"bb\",\"componentName\":\"bb\",\"ui\":\"node-red\"},\"text\":{\"x\":440,\"y\":182,\"value\":\"bb\"}},{\"id\":\"f2a79e6a-a42f-4518-ab8f-66eea4989d6e\",\"type\":\"NodeScriptComponent\",\"x\":600,\"y\":180,\"properties\":{\"id\":21,\"cmpData\":null,\"cmpDataName\":null,\"cmpBreakOpt\":null,\"cmpDoOpt\":null,\"cmpParallel\":null,\"cmpFalseOpt\":\"b\",\"cmpTrueOpt\":\"a\",\"cmpDefaultOpt\":null,\"cmpTo\":null,\"cmpMaxWaitSeconds\":null,\"cmpTag\":null,\"cmpId\":null,\"cmpFinallyOpt\":null,\"cmpPre\":null,\"fallbackId\":null,\"clazz\":null,\"language\":\"java\",\"script\":\"import com.yomahub.liteflow.script.ScriptExecuteWrap;\\nimport com.yomahub.liteflow.script.body.JaninoIfScriptBody;\\n\\npublic class ScriptIfNode implements JaninoIfScriptBody {\\n    @Override\\n    public Boolean body(ScriptExecuteWrap scriptExecuteWrap) {\\n        return false;\\n    }\\n}\",\"type\":\"if_script\",\"componentName\":\"条件脚本组件\",\"componentId\":\"ScriptIfCmp\",\"ui\":\"node-red\"},\"text\":{\"x\":620,\"y\":182,\"value\":\"条件脚本组件\"}},{\"id\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"type\":\"common\",\"x\":820,\"y\":140,\"properties\":{\"componentId\":\"cc\",\"componentName\":\"cc\",\"ui\":\"node-red\"},\"text\":{\"x\":840,\"y\":142,\"value\":\"cc\"}},{\"id\":\"44faa8fe-16a2-4f09-b992-2d0234b01563\",\"type\":\"common\",\"x\":820,\"y\":220,\"properties\":{\"componentId\":\"dd\",\"componentName\":\"dd\",\"ui\":\"node-red\"},\"text\":{\"x\":840,\"y\":222,\"value\":\"dd\"}},{\"id\":\"c9f90d12-0738-4db0-89f7-db427ad7976d\",\"type\":\"common\",\"x\":1030,\"y\":90,\"properties\":{\"componentId\":\"ff\",\"componentName\":\"ff\",\"ui\":\"node-red\"},\"text\":{\"x\":1050,\"y\":92,\"value\":\"ff\"}},{\"id\":\"6c8d7aad-4fbf-4db2-a5c4-0c0531ac82c9\",\"type\":\"common\",\"x\":1030,\"y\":180,\"properties\":{\"componentId\":\"gg\",\"componentName\":\"gg\",\"ui\":\"node-red\"},\"text\":{\"x\":1050,\"y\":182,\"value\":\"gg\"}}],\"edges\":[{\"id\":\"bf8ec95a-7923-4bf2-96f0-3efd19879f11\",\"type\":\"flow-link\",\"sourceNodeId\":\"31a87d18-f744-4bda-b8e6-1500b4ccc842\",\"targetNodeId\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"startPoint\":{\"x\":310,\"y\":120},\"endPoint\":{\"x\":360,\"y\":80},\"properties\":{},\"pointsList\":[{\"x\":310,\"y\":120},{\"x\":410,\"y\":120},{\"x\":260,\"y\":80},{\"x\":360,\"y\":80}]},{\"id\":\"2d5b090f-5004-483c-8ac6-a104772d08e3\",\"type\":\"flow-link\",\"sourceNodeId\":\"31a87d18-f744-4bda-b8e6-1500b4ccc842\",\"targetNodeId\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"startPoint\":{\"x\":310,\"y\":120},\"endPoint\":{\"x\":360,\"y\":180},\"properties\":{},\"pointsList\":[{\"x\":310,\"y\":120},{\"x\":410,\"y\":120},{\"x\":260,\"y\":180},{\"x\":360,\"y\":180}]},{\"id\":\"948de668-31b3-45c0-b5ca-c6bb54ab485c\",\"type\":\"flow-link\",\"sourceNodeId\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"targetNodeId\":\"f2a79e6a-a42f-4518-ab8f-66eea4989d6e\",\"startPoint\":{\"x\":480,\"y\":180},\"endPoint\":{\"x\":540,\"y\":180},\"properties\":{},\"pointsList\":[{\"x\":480,\"y\":180},{\"x\":580,\"y\":180},{\"x\":440,\"y\":180},{\"x\":540,\"y\":180}]},{\"id\":\"2de2bc4b-998f-454f-92cd-0cac70f2c763\",\"type\":\"flow-link\",\"sourceNodeId\":\"f2a79e6a-a42f-4518-ab8f-66eea4989d6e\",\"targetNodeId\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"startPoint\":{\"x\":670,\"y\":180},\"endPoint\":{\"x\":760,\"y\":140},\"properties\":{\"linkType\":1,\"ifType\":\"true\"},\"text\":{\"x\":715,\"y\":160,\"value\":\"true\"},\"pointsList\":[{\"x\":670,\"y\":180},{\"x\":770,\"y\":180},{\"x\":660,\"y\":140},{\"x\":760,\"y\":140}]},{\"id\":\"9a84a001-5f94-4be9-b9b9-1a6d35341e5d\",\"type\":\"flow-link\",\"sourceNodeId\":\"f2a79e6a-a42f-4518-ab8f-66eea4989d6e\",\"targetNodeId\":\"44faa8fe-16a2-4f09-b992-2d0234b01563\",\"startPoint\":{\"x\":670,\"y\":180},\"endPoint\":{\"x\":760,\"y\":220},\"properties\":{\"linkType\":1,\"ifType\":\"false\"},\"text\":{\"x\":715,\"y\":200,\"value\":\"false\"},\"pointsList\":[{\"x\":670,\"y\":180},{\"x\":770,\"y\":180},{\"x\":660,\"y\":220},{\"x\":760,\"y\":220}]},{\"id\":\"e4a22e74-005f-4a07-8b68-d00996edd260\",\"type\":\"flow-link\",\"sourceNodeId\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"targetNodeId\":\"c9f90d12-0738-4db0-89f7-db427ad7976d\",\"startPoint\":{\"x\":480,\"y\":80},\"endPoint\":{\"x\":970,\"y\":90},\"properties\":{},\"pointsList\":[{\"x\":480,\"y\":80},{\"x\":580,\"y\":80},{\"x\":870,\"y\":90},{\"x\":970,\"y\":90}]},{\"id\":\"a67b9934-000b-4de5-87b5-fbaad9893735\",\"type\":\"flow-link\",\"sourceNodeId\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"targetNodeId\":\"c9f90d12-0738-4db0-89f7-db427ad7976d\",\"startPoint\":{\"x\":880,\"y\":140},\"endPoint\":{\"x\":970,\"y\":90},\"properties\":{},\"pointsList\":[{\"x\":880,\"y\":140},{\"x\":980,\"y\":140},{\"x\":870,\"y\":90},{\"x\":970,\"y\":90}]},{\"id\":\"5f165700-a179-4809-a627-875b2680d340\",\"type\":\"flow-link\",\"sourceNodeId\":\"c9f90d12-0738-4db0-89f7-db427ad7976d\",\"targetNodeId\":\"6c8d7aad-4fbf-4db2-a5c4-0c0531ac82c9\",\"startPoint\":{\"x\":1010,\"y\":110},\"endPoint\":{\"x\":1010,\"y\":160},\"properties\":{},\"pointsList\":[{\"x\":1010,\"y\":110},{\"x\":1010,\"y\":210},{\"x\":1010,\"y\":60},{\"x\":1010,\"y\":160}]}]}', NULL);
INSERT INTO `ivy_el` VALUES (3, 'el_id_1', '测试表达式', 'THEN(THEN(node(\"ScriptCmp\")),THEN(node(\"ScriptCmp1\")));', '{\"nodes\":[{\"id\":\"6d56c7b0-5c44-45a9-9a55-3c0848177a38\",\"type\":\"NodeScriptComponent\",\"properties\":{\"id\":19,\"cmpData\":\"\",\"cmpDataName\":\"\",\"cmpBreakOpt\":null,\"cmpDoOpt\":null,\"cmpParallel\":null,\"cmpFalseOpt\":null,\"cmpTrueOpt\":null,\"cmpDefaultOpt\":null,\"cmpTo\":null,\"cmpMaxWaitSeconds\":null,\"cmpTag\":null,\"cmpId\":null,\"cmpFinallyOpt\":null,\"cmpPre\":null,\"fallbackId\":null,\"clazz\":null,\"language\":\"java\",\"script\":\"import com.yomahub.liteflow.script.ScriptExecuteWrap;\\nimport com.yomahub.liteflow.script.body.JaninoCommonScriptBody;\\n\\npublic class ScriptNode implements JaninoCommonScriptBody {\\n    @Override\\n    public Void body(ScriptExecuteWrap scriptExecuteWrap) {\\n        System.out.println(\\\"ScriptCmp executed!\\\");\\n        return null;\\n    }\\n}\\n\",\"type\":\"script\",\"componentName\":\"脚本组件\",\"componentId\":\"ScriptCmp\",\"ui\":\"node-red\"},\"text\":\"脚本组件\"},{\"id\":\"47021ab6-3414-4b91-9a86-5fc36c5c2fb3\",\"type\":\"NodeScriptComponent\",\"properties\":{\"id\":19,\"cmpData\":\"\",\"cmpDataName\":\"\",\"cmpBreakOpt\":null,\"cmpDoOpt\":null,\"cmpParallel\":null,\"cmpFalseOpt\":null,\"cmpTrueOpt\":null,\"cmpDefaultOpt\":null,\"cmpTo\":null,\"cmpMaxWaitSeconds\":null,\"cmpTag\":null,\"cmpId\":null,\"cmpFinallyOpt\":null,\"cmpPre\":null,\"fallbackId\":null,\"clazz\":null,\"language\":\"java\",\"script\":\"import com.yomahub.liteflow.script.ScriptExecuteWrap;\\nimport com.yomahub.liteflow.script.body.JaninoCommonScriptBody;\\n\\npublic class ScriptNode implements JaninoCommonScriptBody {\\n    @Override\\n    public Void body(ScriptExecuteWrap scriptExecuteWrap) {\\n        System.out.println(\\\"ScriptCmp executed!\\\");\\n        return null;\\n    }\\n}\\n\",\"type\":\"script\",\"componentName\":\"脚本组件1\",\"componentId\":\"ScriptCmp1\",\"ui\":\"node-red\"},\"text\":\"脚本组件1\"}],\"edges\":[{\"sourceNodeId\":\"6d56c7b0-5c44-45a9-9a55-3c0848177a38\",\"targetNodeId\":\"47021ab6-3414-4b91-9a86-5fc36c5c2fb3\",\"properties\":{},\"text\":\"\"}]}', '{\"nodes\":[{\"id\":\"6d56c7b0-5c44-45a9-9a55-3c0848177a38\",\"type\":\"NodeScriptComponent\",\"x\":270,\"y\":60,\"properties\":{\"id\":19,\"cmpData\":\"\",\"cmpDataName\":\"\",\"cmpBreakOpt\":null,\"cmpDoOpt\":null,\"cmpParallel\":null,\"cmpFalseOpt\":null,\"cmpTrueOpt\":null,\"cmpDefaultOpt\":null,\"cmpTo\":null,\"cmpMaxWaitSeconds\":null,\"cmpTag\":null,\"cmpId\":null,\"cmpFinallyOpt\":null,\"cmpPre\":null,\"fallbackId\":null,\"clazz\":null,\"language\":\"java\",\"script\":\"import com.yomahub.liteflow.script.ScriptExecuteWrap;\\nimport com.yomahub.liteflow.script.body.JaninoCommonScriptBody;\\n\\npublic class ScriptNode implements JaninoCommonScriptBody {\\n    @Override\\n    public Void body(ScriptExecuteWrap scriptExecuteWrap) {\\n        System.out.println(\\\"ScriptCmp executed!\\\");\\n        return null;\\n    }\\n}\\n\",\"type\":\"script\",\"componentName\":\"脚本组件\",\"componentId\":\"ScriptCmp\",\"ui\":\"node-red\"},\"text\":{\"x\":290,\"y\":62,\"value\":\"脚本组件\"}},{\"id\":\"47021ab6-3414-4b91-9a86-5fc36c5c2fb3\",\"type\":\"NodeScriptComponent\",\"x\":270,\"y\":160,\"properties\":{\"id\":19,\"cmpData\":\"\",\"cmpDataName\":\"\",\"cmpBreakOpt\":null,\"cmpDoOpt\":null,\"cmpParallel\":null,\"cmpFalseOpt\":null,\"cmpTrueOpt\":null,\"cmpDefaultOpt\":null,\"cmpTo\":null,\"cmpMaxWaitSeconds\":null,\"cmpTag\":null,\"cmpId\":null,\"cmpFinallyOpt\":null,\"cmpPre\":null,\"fallbackId\":null,\"clazz\":null,\"language\":\"java\",\"script\":\"import com.yomahub.liteflow.script.ScriptExecuteWrap;\\nimport com.yomahub.liteflow.script.body.JaninoCommonScriptBody;\\n\\npublic class ScriptNode implements JaninoCommonScriptBody {\\n    @Override\\n    public Void body(ScriptExecuteWrap scriptExecuteWrap) {\\n        System.out.println(\\\"ScriptCmp executed!\\\");\\n        return null;\\n    }\\n}\\n\",\"type\":\"script\",\"componentName\":\"脚本组件1\",\"componentId\":\"ScriptCmp1\",\"ui\":\"node-red\"},\"text\":{\"x\":290,\"y\":162,\"value\":\"脚本组件1\"}}],\"edges\":[{\"id\":\"2d2adbef-a56c-493b-9d3b-46a8211373f4\",\"type\":\"flow-link\",\"sourceNodeId\":\"6d56c7b0-5c44-45a9-9a55-3c0848177a38\",\"targetNodeId\":\"47021ab6-3414-4b91-9a86-5fc36c5c2fb3\",\"startPoint\":{\"x\":250,\"y\":80},\"endPoint\":{\"x\":250,\"y\":140},\"properties\":{},\"pointsList\":[{\"x\":250,\"y\":80},{\"x\":250,\"y\":180},{\"x\":250,\"y\":40},{\"x\":250,\"y\":140}]}]}', NULL);
INSERT INTO `ivy_el` VALUES (4, 'el_id_2', '测试表达式2', NULL, '{\"nodes\":[{\"id\":\"dfb65cce-b82e-445b-8aa8-049d6ffde985\",\"type\":\"common\",\"properties\":{\"componentId\":\"a\",\"componentName\":\"a\",\"ui\":\"node-red\"},\"text\":\"a\"},{\"id\":\"087c47be-d734-4059-9a00-1ed296610e1d\",\"type\":\"common\",\"properties\":{\"componentId\":\"b\",\"componentName\":\"b\",\"ui\":\"node-red\"},\"text\":\"b\"},{\"id\":\"007824b4-313b-4ae6-91e6-dc50b65e920a\",\"type\":\"common\",\"properties\":{\"componentId\":\"c\",\"componentName\":\"c\",\"ui\":\"node-red\"},\"text\":\"c\"}],\"edges\":[{\"sourceNodeId\":\"dfb65cce-b82e-445b-8aa8-049d6ffde985\",\"targetNodeId\":\"087c47be-d734-4059-9a00-1ed296610e1d\",\"properties\":{\"linkType\":\"0\"},\"text\":\"\"},{\"sourceNodeId\":\"087c47be-d734-4059-9a00-1ed296610e1d\",\"targetNodeId\":\"007824b4-313b-4ae6-91e6-dc50b65e920a\",\"properties\":{\"linkType\":\"0\"},\"text\":\"\"}]}', '{\"nodes\":[{\"id\":\"dfb65cce-b82e-445b-8aa8-049d6ffde985\",\"type\":\"common\",\"x\":230,\"y\":80,\"properties\":{\"componentId\":\"a\",\"componentName\":\"a\",\"ui\":\"node-red\"},\"text\":{\"x\":250,\"y\":82,\"value\":\"a\"}},{\"id\":\"087c47be-d734-4059-9a00-1ed296610e1d\",\"type\":\"common\",\"x\":420,\"y\":80,\"properties\":{\"componentId\":\"b\",\"componentName\":\"b\",\"ui\":\"node-red\"},\"text\":{\"x\":440,\"y\":82,\"value\":\"b\"}},{\"id\":\"007824b4-313b-4ae6-91e6-dc50b65e920a\",\"type\":\"common\",\"x\":420,\"y\":160,\"properties\":{\"componentId\":\"c\",\"componentName\":\"c\",\"ui\":\"node-red\"},\"text\":{\"x\":440,\"y\":162,\"value\":\"c\"}}],\"edges\":[{\"id\":\"ad0b3d53-a7f8-458a-8c27-2cb2b1a79843\",\"type\":\"flow-link\",\"sourceNodeId\":\"dfb65cce-b82e-445b-8aa8-049d6ffde985\",\"targetNodeId\":\"087c47be-d734-4059-9a00-1ed296610e1d\",\"startPoint\":{\"x\":290,\"y\":80},\"endPoint\":{\"x\":360,\"y\":80},\"properties\":{\"linkType\":\"0\"},\"pointsList\":[{\"x\":290,\"y\":80},{\"x\":390,\"y\":80},{\"x\":260,\"y\":80},{\"x\":360,\"y\":80}]},{\"id\":\"7e7ab938-820b-4a1e-aeb8-aadc6437b10b\",\"type\":\"flow-link\",\"sourceNodeId\":\"087c47be-d734-4059-9a00-1ed296610e1d\",\"targetNodeId\":\"007824b4-313b-4ae6-91e6-dc50b65e920a\",\"startPoint\":{\"x\":400,\"y\":100},\"endPoint\":{\"x\":400,\"y\":140},\"properties\":{\"linkType\":\"0\"},\"pointsList\":[{\"x\":400,\"y\":100},{\"x\":400,\"y\":200},{\"x\":400,\"y\":40},{\"x\":400,\"y\":140}]}]}', NULL);
INSERT INTO `ivy_el` VALUES (6, 'el4', '测试流程4', NULL, '{\"nodes\":[{\"id\":\"31a87d18-f744-4bda-b8e6-1500b4ccc842\",\"type\":\"NodeScriptComponent\",\"properties\":{\"id\":20,\"cmpData\":\"\",\"cmpDataName\":\"\",\"cmpBreakOpt\":null,\"cmpDoOpt\":null,\"cmpParallel\":null,\"cmpFalseOpt\":null,\"cmpTrueOpt\":null,\"cmpDefaultOpt\":\"b\",\"cmpTo\":\"a,b\",\"cmpMaxWaitSeconds\":null,\"cmpTag\":null,\"cmpId\":null,\"cmpFinallyOpt\":null,\"cmpPre\":null,\"fallbackId\":null,\"clazz\":null,\"language\":\"java\",\"script\":\"import com.yomahub.liteflow.script.ScriptExecuteWrap;\\nimport com.yomahub.liteflow.script.body.JaninoSwitchScriptBody;\\n\\npublic class ScriptSwitchNode implements JaninoSwitchScriptBody {\\n    @Override\\n    public String body(ScriptExecuteWrap scriptExecuteWrap) {\\n        System.out.println(\\\"ScriptSwitchCmp executed!\\\");\\n        return \\\"a\\\";\\n    }\\n}\",\"type\":\"switch_script\",\"componentName\":\"选择脚本组件\",\"componentId\":\"ScriptSwitchCmp\",\"ui\":\"node-red\"},\"text\":\"选择脚本组件\"},{\"id\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"type\":\"common\",\"properties\":{\"componentId\":\"aa\",\"componentName\":\"aa\",\"ui\":\"node-red\"},\"text\":\"aa\"},{\"id\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"type\":\"common\",\"properties\":{\"componentId\":\"bb\",\"componentName\":\"bb\",\"ui\":\"node-red\"},\"text\":\"bb\"},{\"id\":\"f2a79e6a-a42f-4518-ab8f-66eea4989d6e\",\"type\":\"NodeScriptComponent\",\"properties\":{\"id\":21,\"cmpData\":null,\"cmpDataName\":null,\"cmpBreakOpt\":null,\"cmpDoOpt\":null,\"cmpParallel\":null,\"cmpFalseOpt\":\"b\",\"cmpTrueOpt\":\"a\",\"cmpDefaultOpt\":null,\"cmpTo\":null,\"cmpMaxWaitSeconds\":null,\"cmpTag\":null,\"cmpId\":null,\"cmpFinallyOpt\":null,\"cmpPre\":null,\"fallbackId\":null,\"clazz\":null,\"language\":\"java\",\"script\":\"import com.yomahub.liteflow.script.ScriptExecuteWrap;\\nimport com.yomahub.liteflow.script.body.JaninoIfScriptBody;\\n\\npublic class ScriptIfNode implements JaninoIfScriptBody {\\n    @Override\\n    public Boolean body(ScriptExecuteWrap scriptExecuteWrap) {\\n        return false;\\n    }\\n}\",\"type\":\"if_script\",\"componentName\":\"条件脚本组件\",\"componentId\":\"ScriptIfCmp\",\"ui\":\"node-red\"},\"text\":\"条件脚本组件\"},{\"id\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"type\":\"common\",\"properties\":{\"componentId\":\"cc\",\"componentName\":\"cc\",\"ui\":\"node-red\"},\"text\":\"cc\"},{\"id\":\"44faa8fe-16a2-4f09-b992-2d0234b01563\",\"type\":\"common\",\"properties\":{\"componentId\":\"dd\",\"componentName\":\"dd\",\"ui\":\"node-red\"},\"text\":\"dd\"},{\"id\":\"c9f90d12-0738-4db0-89f7-db427ad7976d\",\"type\":\"common\",\"properties\":{\"componentId\":\"ff\",\"componentName\":\"ff\",\"ui\":\"node-red\"},\"text\":\"ff\"},{\"id\":\"6c8d7aad-4fbf-4db2-a5c4-0c0531ac82c9\",\"type\":\"common\",\"properties\":{\"componentId\":\"gg\",\"componentName\":\"gg\",\"ui\":\"node-red\"},\"text\":\"gg\"}],\"edges\":[{\"sourceNodeId\":\"31a87d18-f744-4bda-b8e6-1500b4ccc842\",\"targetNodeId\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"31a87d18-f744-4bda-b8e6-1500b4ccc842\",\"targetNodeId\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"targetNodeId\":\"f2a79e6a-a42f-4518-ab8f-66eea4989d6e\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"f2a79e6a-a42f-4518-ab8f-66eea4989d6e\",\"targetNodeId\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"properties\":{\"linkType\":1,\"ifType\":\"true\"},\"text\":\"true\"},{\"sourceNodeId\":\"f2a79e6a-a42f-4518-ab8f-66eea4989d6e\",\"targetNodeId\":\"44faa8fe-16a2-4f09-b992-2d0234b01563\",\"properties\":{\"linkType\":1,\"ifType\":\"false\"},\"text\":\"false\"},{\"sourceNodeId\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"targetNodeId\":\"c9f90d12-0738-4db0-89f7-db427ad7976d\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"targetNodeId\":\"c9f90d12-0738-4db0-89f7-db427ad7976d\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"c9f90d12-0738-4db0-89f7-db427ad7976d\",\"targetNodeId\":\"6c8d7aad-4fbf-4db2-a5c4-0c0531ac82c9\",\"properties\":{},\"text\":\"\"}]}', '{\"nodes\":[{\"id\":\"31a87d18-f744-4bda-b8e6-1500b4ccc842\",\"type\":\"NodeScriptComponent\",\"x\":240,\"y\":120,\"properties\":{\"id\":20,\"cmpData\":\"\",\"cmpDataName\":\"\",\"cmpBreakOpt\":null,\"cmpDoOpt\":null,\"cmpParallel\":null,\"cmpFalseOpt\":null,\"cmpTrueOpt\":null,\"cmpDefaultOpt\":\"b\",\"cmpTo\":\"a,b\",\"cmpMaxWaitSeconds\":null,\"cmpTag\":null,\"cmpId\":null,\"cmpFinallyOpt\":null,\"cmpPre\":null,\"fallbackId\":null,\"clazz\":null,\"language\":\"java\",\"script\":\"import com.yomahub.liteflow.script.ScriptExecuteWrap;\\nimport com.yomahub.liteflow.script.body.JaninoSwitchScriptBody;\\n\\npublic class ScriptSwitchNode implements JaninoSwitchScriptBody {\\n    @Override\\n    public String body(ScriptExecuteWrap scriptExecuteWrap) {\\n        System.out.println(\\\"ScriptSwitchCmp executed!\\\");\\n        return \\\"a\\\";\\n    }\\n}\",\"type\":\"switch_script\",\"componentName\":\"选择脚本组件\",\"componentId\":\"ScriptSwitchCmp\",\"ui\":\"node-red\"},\"text\":{\"x\":260,\"y\":122,\"value\":\"选择脚本组件\"}},{\"id\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"type\":\"common\",\"x\":420,\"y\":80,\"properties\":{\"componentId\":\"aa\",\"componentName\":\"aa\",\"ui\":\"node-red\"},\"text\":{\"x\":440,\"y\":82,\"value\":\"aa\"}},{\"id\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"type\":\"common\",\"x\":420,\"y\":180,\"properties\":{\"componentId\":\"bb\",\"componentName\":\"bb\",\"ui\":\"node-red\"},\"text\":{\"x\":440,\"y\":182,\"value\":\"bb\"}},{\"id\":\"f2a79e6a-a42f-4518-ab8f-66eea4989d6e\",\"type\":\"NodeScriptComponent\",\"x\":600,\"y\":180,\"properties\":{\"id\":21,\"cmpData\":null,\"cmpDataName\":null,\"cmpBreakOpt\":null,\"cmpDoOpt\":null,\"cmpParallel\":null,\"cmpFalseOpt\":\"b\",\"cmpTrueOpt\":\"a\",\"cmpDefaultOpt\":null,\"cmpTo\":null,\"cmpMaxWaitSeconds\":null,\"cmpTag\":null,\"cmpId\":null,\"cmpFinallyOpt\":null,\"cmpPre\":null,\"fallbackId\":null,\"clazz\":null,\"language\":\"java\",\"script\":\"import com.yomahub.liteflow.script.ScriptExecuteWrap;\\nimport com.yomahub.liteflow.script.body.JaninoIfScriptBody;\\n\\npublic class ScriptIfNode implements JaninoIfScriptBody {\\n    @Override\\n    public Boolean body(ScriptExecuteWrap scriptExecuteWrap) {\\n        return false;\\n    }\\n}\",\"type\":\"if_script\",\"componentName\":\"条件脚本组件\",\"componentId\":\"ScriptIfCmp\",\"ui\":\"node-red\"},\"text\":{\"x\":620,\"y\":182,\"value\":\"条件脚本组件\"}},{\"id\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"type\":\"common\",\"x\":820,\"y\":140,\"properties\":{\"componentId\":\"cc\",\"componentName\":\"cc\",\"ui\":\"node-red\"},\"text\":{\"x\":840,\"y\":142,\"value\":\"cc\"}},{\"id\":\"44faa8fe-16a2-4f09-b992-2d0234b01563\",\"type\":\"common\",\"x\":820,\"y\":220,\"properties\":{\"componentId\":\"dd\",\"componentName\":\"dd\",\"ui\":\"node-red\"},\"text\":{\"x\":840,\"y\":222,\"value\":\"dd\"}},{\"id\":\"c9f90d12-0738-4db0-89f7-db427ad7976d\",\"type\":\"common\",\"x\":1030,\"y\":90,\"properties\":{\"componentId\":\"ff\",\"componentName\":\"ff\",\"ui\":\"node-red\"},\"text\":{\"x\":1050,\"y\":92,\"value\":\"ff\"}},{\"id\":\"6c8d7aad-4fbf-4db2-a5c4-0c0531ac82c9\",\"type\":\"common\",\"x\":1030,\"y\":180,\"properties\":{\"componentId\":\"gg\",\"componentName\":\"gg\",\"ui\":\"node-red\"},\"text\":{\"x\":1050,\"y\":182,\"value\":\"gg\"}}],\"edges\":[{\"id\":\"bf8ec95a-7923-4bf2-96f0-3efd19879f11\",\"type\":\"flow-link\",\"sourceNodeId\":\"31a87d18-f744-4bda-b8e6-1500b4ccc842\",\"targetNodeId\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"startPoint\":{\"x\":310,\"y\":120},\"endPoint\":{\"x\":360,\"y\":80},\"properties\":{},\"pointsList\":[{\"x\":310,\"y\":120},{\"x\":410,\"y\":120},{\"x\":260,\"y\":80},{\"x\":360,\"y\":80}]},{\"id\":\"2d5b090f-5004-483c-8ac6-a104772d08e3\",\"type\":\"flow-link\",\"sourceNodeId\":\"31a87d18-f744-4bda-b8e6-1500b4ccc842\",\"targetNodeId\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"startPoint\":{\"x\":310,\"y\":120},\"endPoint\":{\"x\":360,\"y\":180},\"properties\":{},\"pointsList\":[{\"x\":310,\"y\":120},{\"x\":410,\"y\":120},{\"x\":260,\"y\":180},{\"x\":360,\"y\":180}]},{\"id\":\"948de668-31b3-45c0-b5ca-c6bb54ab485c\",\"type\":\"flow-link\",\"sourceNodeId\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"targetNodeId\":\"f2a79e6a-a42f-4518-ab8f-66eea4989d6e\",\"startPoint\":{\"x\":480,\"y\":180},\"endPoint\":{\"x\":540,\"y\":180},\"properties\":{},\"pointsList\":[{\"x\":480,\"y\":180},{\"x\":580,\"y\":180},{\"x\":440,\"y\":180},{\"x\":540,\"y\":180}]},{\"id\":\"2de2bc4b-998f-454f-92cd-0cac70f2c763\",\"type\":\"flow-link\",\"sourceNodeId\":\"f2a79e6a-a42f-4518-ab8f-66eea4989d6e\",\"targetNodeId\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"startPoint\":{\"x\":670,\"y\":180},\"endPoint\":{\"x\":760,\"y\":140},\"properties\":{\"linkType\":1,\"ifType\":\"true\"},\"text\":{\"x\":715,\"y\":160,\"value\":\"true\"},\"pointsList\":[{\"x\":670,\"y\":180},{\"x\":770,\"y\":180},{\"x\":660,\"y\":140},{\"x\":760,\"y\":140}]},{\"id\":\"9a84a001-5f94-4be9-b9b9-1a6d35341e5d\",\"type\":\"flow-link\",\"sourceNodeId\":\"f2a79e6a-a42f-4518-ab8f-66eea4989d6e\",\"targetNodeId\":\"44faa8fe-16a2-4f09-b992-2d0234b01563\",\"startPoint\":{\"x\":670,\"y\":180},\"endPoint\":{\"x\":760,\"y\":220},\"properties\":{\"linkType\":1,\"ifType\":\"false\"},\"text\":{\"x\":715,\"y\":200,\"value\":\"false\"},\"pointsList\":[{\"x\":670,\"y\":180},{\"x\":770,\"y\":180},{\"x\":660,\"y\":220},{\"x\":760,\"y\":220}]},{\"id\":\"e4a22e74-005f-4a07-8b68-d00996edd260\",\"type\":\"flow-link\",\"sourceNodeId\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"targetNodeId\":\"c9f90d12-0738-4db0-89f7-db427ad7976d\",\"startPoint\":{\"x\":480,\"y\":80},\"endPoint\":{\"x\":970,\"y\":90},\"properties\":{},\"pointsList\":[{\"x\":480,\"y\":80},{\"x\":580,\"y\":80},{\"x\":870,\"y\":90},{\"x\":970,\"y\":90}]},{\"id\":\"a67b9934-000b-4de5-87b5-fbaad9893735\",\"type\":\"flow-link\",\"sourceNodeId\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"targetNodeId\":\"c9f90d12-0738-4db0-89f7-db427ad7976d\",\"startPoint\":{\"x\":880,\"y\":140},\"endPoint\":{\"x\":970,\"y\":90},\"properties\":{},\"pointsList\":[{\"x\":880,\"y\":140},{\"x\":980,\"y\":140},{\"x\":870,\"y\":90},{\"x\":970,\"y\":90}]},{\"id\":\"5f165700-a179-4809-a627-875b2680d340\",\"type\":\"flow-link\",\"sourceNodeId\":\"c9f90d12-0738-4db0-89f7-db427ad7976d\",\"targetNodeId\":\"6c8d7aad-4fbf-4db2-a5c4-0c0531ac82c9\",\"startPoint\":{\"x\":1010,\"y\":110},\"endPoint\":{\"x\":1010,\"y\":160},\"properties\":{},\"pointsList\":[{\"x\":1010,\"y\":110},{\"x\":1010,\"y\":210},{\"x\":1010,\"y\":60},{\"x\":1010,\"y\":160}]}]}', NULL);
INSERT INTO `ivy_el` VALUES (7, 'el5', '测试流程5', NULL, '{\"nodes\":[{\"id\":\"ea956e6e-3b83-4912-a3a8-3dfd4a4f8675\",\"type\":\"common\",\"properties\":{\"componentId\":\"a\",\"componentName\":\"a\",\"ui\":\"node-red\"},\"text\":\"a\"},{\"id\":\"b22a58a8-e330-441e-86fd-0aa8e779fb35\",\"type\":\"common\",\"properties\":{\"componentId\":\"b\",\"componentName\":\"b\",\"ui\":\"node-red\"},\"text\":\"b\"},{\"id\":\"2bcc0bbf-97f4-40b8-bf85-7667d951b4b5\",\"type\":\"common\",\"properties\":{\"componentId\":\"c\",\"componentName\":\"c\",\"ui\":\"node-red\"},\"text\":\"c\"},{\"id\":\"77c98511-9355-483d-af09-02f1107d898f\",\"type\":\"common\",\"properties\":{\"componentId\":\"d\",\"componentName\":\"d\",\"ui\":\"node-red\"},\"text\":\"d\"},{\"id\":\"8875305a-d51d-47ed-98d0-1a43079398db\",\"type\":\"common\",\"properties\":{\"componentId\":\"e\",\"componentName\":\"e\",\"ui\":\"node-red\"},\"text\":\"e\"},{\"id\":\"e2788bbb-8cb7-473c-95d9-f932a663b610\",\"type\":\"common\",\"properties\":{\"componentId\":\"f\",\"componentName\":\"f\",\"ui\":\"node-red\"},\"text\":\"f\"},{\"id\":\"abaa9db0-04f8-4c78-bbc4-091c255a4ebd\",\"type\":\"common\",\"properties\":{\"componentId\":\"g\",\"componentName\":\"g\",\"ui\":\"node-red\"},\"text\":\"g\"},{\"id\":\"1d7240dc-4e70-4d3a-84b1-3f0168a91541\",\"type\":\"common\",\"properties\":{\"componentId\":\"h\",\"componentName\":\"h\",\"ui\":\"node-red\"},\"text\":\"h\"}],\"edges\":[{\"sourceNodeId\":\"ea956e6e-3b83-4912-a3a8-3dfd4a4f8675\",\"targetNodeId\":\"b22a58a8-e330-441e-86fd-0aa8e779fb35\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"ea956e6e-3b83-4912-a3a8-3dfd4a4f8675\",\"targetNodeId\":\"2bcc0bbf-97f4-40b8-bf85-7667d951b4b5\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"ea956e6e-3b83-4912-a3a8-3dfd4a4f8675\",\"targetNodeId\":\"77c98511-9355-483d-af09-02f1107d898f\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"b22a58a8-e330-441e-86fd-0aa8e779fb35\",\"targetNodeId\":\"8875305a-d51d-47ed-98d0-1a43079398db\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"2bcc0bbf-97f4-40b8-bf85-7667d951b4b5\",\"targetNodeId\":\"8875305a-d51d-47ed-98d0-1a43079398db\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"8875305a-d51d-47ed-98d0-1a43079398db\",\"targetNodeId\":\"e2788bbb-8cb7-473c-95d9-f932a663b610\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"8875305a-d51d-47ed-98d0-1a43079398db\",\"targetNodeId\":\"abaa9db0-04f8-4c78-bbc4-091c255a4ebd\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"e2788bbb-8cb7-473c-95d9-f932a663b610\",\"targetNodeId\":\"1d7240dc-4e70-4d3a-84b1-3f0168a91541\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"abaa9db0-04f8-4c78-bbc4-091c255a4ebd\",\"targetNodeId\":\"1d7240dc-4e70-4d3a-84b1-3f0168a91541\",\"properties\":{},\"text\":\"\"}]}', '{\"nodes\":[{\"id\":\"ea956e6e-3b83-4912-a3a8-3dfd4a4f8675\",\"type\":\"common\",\"x\":240,\"y\":80,\"properties\":{\"componentId\":\"a\",\"componentName\":\"a\",\"ui\":\"node-red\"},\"text\":{\"x\":260,\"y\":82,\"value\":\"a\"}},{\"id\":\"b22a58a8-e330-441e-86fd-0aa8e779fb35\",\"type\":\"common\",\"x\":430,\"y\":50,\"properties\":{\"componentId\":\"b\",\"componentName\":\"b\",\"ui\":\"node-red\"},\"text\":{\"x\":450,\"y\":52,\"value\":\"b\"}},{\"id\":\"2bcc0bbf-97f4-40b8-bf85-7667d951b4b5\",\"type\":\"common\",\"x\":430,\"y\":130,\"properties\":{\"componentId\":\"c\",\"componentName\":\"c\",\"ui\":\"node-red\"},\"text\":{\"x\":450,\"y\":132,\"value\":\"c\"}},{\"id\":\"77c98511-9355-483d-af09-02f1107d898f\",\"type\":\"common\",\"x\":430,\"y\":220,\"properties\":{\"componentId\":\"d\",\"componentName\":\"d\",\"ui\":\"node-red\"},\"text\":{\"x\":450,\"y\":222,\"value\":\"d\"}},{\"id\":\"8875305a-d51d-47ed-98d0-1a43079398db\",\"type\":\"common\",\"x\":640,\"y\":80,\"properties\":{\"componentId\":\"e\",\"componentName\":\"e\",\"ui\":\"node-red\"},\"text\":{\"x\":660,\"y\":82,\"value\":\"e\"}},{\"id\":\"e2788bbb-8cb7-473c-95d9-f932a663b610\",\"type\":\"common\",\"x\":880,\"y\":70,\"properties\":{\"componentId\":\"f\",\"componentName\":\"f\",\"ui\":\"node-red\"},\"text\":{\"x\":900,\"y\":72,\"value\":\"f\"}},{\"id\":\"abaa9db0-04f8-4c78-bbc4-091c255a4ebd\",\"type\":\"common\",\"x\":880,\"y\":140,\"properties\":{\"componentId\":\"g\",\"componentName\":\"g\",\"ui\":\"node-red\"},\"text\":{\"x\":900,\"y\":142,\"value\":\"g\"}},{\"id\":\"1d7240dc-4e70-4d3a-84b1-3f0168a91541\",\"type\":\"common\",\"x\":1080,\"y\":110,\"properties\":{\"componentId\":\"h\",\"componentName\":\"h\",\"ui\":\"node-red\"},\"text\":{\"x\":1100,\"y\":112,\"value\":\"h\"}}],\"edges\":[{\"id\":\"b47aa467-0e7a-462f-9af7-18632aad110a\",\"type\":\"flow-link\",\"sourceNodeId\":\"ea956e6e-3b83-4912-a3a8-3dfd4a4f8675\",\"targetNodeId\":\"b22a58a8-e330-441e-86fd-0aa8e779fb35\",\"startPoint\":{\"x\":300,\"y\":80},\"endPoint\":{\"x\":370,\"y\":50},\"properties\":{},\"pointsList\":[{\"x\":300,\"y\":80},{\"x\":400,\"y\":80},{\"x\":270,\"y\":50},{\"x\":370,\"y\":50}]},{\"id\":\"82d7d9cb-f031-486d-bd73-73b52fda3b45\",\"type\":\"flow-link\",\"sourceNodeId\":\"ea956e6e-3b83-4912-a3a8-3dfd4a4f8675\",\"targetNodeId\":\"2bcc0bbf-97f4-40b8-bf85-7667d951b4b5\",\"startPoint\":{\"x\":300,\"y\":80},\"endPoint\":{\"x\":370,\"y\":130},\"properties\":{},\"pointsList\":[{\"x\":300,\"y\":80},{\"x\":400,\"y\":80},{\"x\":270,\"y\":130},{\"x\":370,\"y\":130}]},{\"id\":\"616fbb42-b36a-45fa-9c63-f4384e480de8\",\"type\":\"flow-link\",\"sourceNodeId\":\"ea956e6e-3b83-4912-a3a8-3dfd4a4f8675\",\"targetNodeId\":\"77c98511-9355-483d-af09-02f1107d898f\",\"startPoint\":{\"x\":300,\"y\":80},\"endPoint\":{\"x\":370,\"y\":220},\"properties\":{},\"pointsList\":[{\"x\":300,\"y\":80},{\"x\":400,\"y\":80},{\"x\":270,\"y\":220},{\"x\":370,\"y\":220}]},{\"id\":\"ed7795b0-4516-440c-900f-4b827e6905fe\",\"type\":\"flow-link\",\"sourceNodeId\":\"b22a58a8-e330-441e-86fd-0aa8e779fb35\",\"targetNodeId\":\"8875305a-d51d-47ed-98d0-1a43079398db\",\"startPoint\":{\"x\":490,\"y\":50},\"endPoint\":{\"x\":580,\"y\":80},\"properties\":{},\"pointsList\":[{\"x\":490,\"y\":50},{\"x\":590,\"y\":50},{\"x\":480,\"y\":80},{\"x\":580,\"y\":80}]},{\"id\":\"7d09cccf-174e-43cf-b2d7-dce5e679e427\",\"type\":\"flow-link\",\"sourceNodeId\":\"2bcc0bbf-97f4-40b8-bf85-7667d951b4b5\",\"targetNodeId\":\"8875305a-d51d-47ed-98d0-1a43079398db\",\"startPoint\":{\"x\":490,\"y\":130},\"endPoint\":{\"x\":580,\"y\":80},\"properties\":{},\"pointsList\":[{\"x\":490,\"y\":130},{\"x\":590,\"y\":130},{\"x\":480,\"y\":80},{\"x\":580,\"y\":80}]},{\"id\":\"d7d955b9-6bbd-40dc-8cfd-80894d4d775e\",\"type\":\"flow-link\",\"sourceNodeId\":\"8875305a-d51d-47ed-98d0-1a43079398db\",\"targetNodeId\":\"e2788bbb-8cb7-473c-95d9-f932a663b610\",\"startPoint\":{\"x\":700,\"y\":80},\"endPoint\":{\"x\":820,\"y\":70},\"properties\":{},\"pointsList\":[{\"x\":700,\"y\":80},{\"x\":800,\"y\":80},{\"x\":720,\"y\":70},{\"x\":820,\"y\":70}]},{\"id\":\"2f192401-31f2-431b-93f9-09a7e85862c0\",\"type\":\"flow-link\",\"sourceNodeId\":\"8875305a-d51d-47ed-98d0-1a43079398db\",\"targetNodeId\":\"abaa9db0-04f8-4c78-bbc4-091c255a4ebd\",\"startPoint\":{\"x\":700,\"y\":80},\"endPoint\":{\"x\":820,\"y\":140},\"properties\":{},\"pointsList\":[{\"x\":700,\"y\":80},{\"x\":800,\"y\":80},{\"x\":720,\"y\":140},{\"x\":820,\"y\":140}]},{\"id\":\"767265ce-5e08-4d0a-9ed6-1b1a1111b75d\",\"type\":\"flow-link\",\"sourceNodeId\":\"e2788bbb-8cb7-473c-95d9-f932a663b610\",\"targetNodeId\":\"1d7240dc-4e70-4d3a-84b1-3f0168a91541\",\"startPoint\":{\"x\":940,\"y\":70},\"endPoint\":{\"x\":1020,\"y\":110},\"properties\":{},\"pointsList\":[{\"x\":940,\"y\":70},{\"x\":1040,\"y\":70},{\"x\":920,\"y\":110},{\"x\":1020,\"y\":110}]},{\"id\":\"ae5d3841-c9a8-4edf-9048-228eda6019d4\",\"type\":\"flow-link\",\"sourceNodeId\":\"abaa9db0-04f8-4c78-bbc4-091c255a4ebd\",\"targetNodeId\":\"1d7240dc-4e70-4d3a-84b1-3f0168a91541\",\"startPoint\":{\"x\":940,\"y\":140},\"endPoint\":{\"x\":1020,\"y\":110},\"properties\":{},\"pointsList\":[{\"x\":940,\"y\":140},{\"x\":1040,\"y\":140},{\"x\":920,\"y\":110},{\"x\":1020,\"y\":110}]}]}', NULL);
INSERT INTO `ivy_el` VALUES (8, 'el6', '测试流程6', NULL, '{\"nodes\":[{\"id\":\"46c09860-15b6-46a6-b70a-020344c8de38\",\"type\":\"common\",\"properties\":{\"componentId\":\"a\",\"componentName\":\"a\",\"ui\":\"node-red\"},\"text\":\"a\"},{\"id\":\"a6741cab-ca3f-44d5-a22e-98d067da9a43\",\"type\":\"common\",\"properties\":{\"componentId\":\"b\",\"componentName\":\"b\",\"ui\":\"node-red\"},\"text\":\"b\"},{\"id\":\"d70c8a96-f8aa-45a2-ab5e-c4e6e8268721\",\"type\":\"common\",\"properties\":{\"componentId\":\"c\",\"componentName\":\"c\",\"ui\":\"node-red\"},\"text\":\"c\"},{\"id\":\"c0889832-ff85-43fc-83b3-930b9674e062\",\"type\":\"common\",\"properties\":{\"componentId\":\"d\",\"componentName\":\"d\",\"ui\":\"node-red\"},\"text\":\"d\"},{\"id\":\"bf08e701-854a-4adf-aa8b-a02ab181cd99\",\"type\":\"common\",\"properties\":{\"componentId\":\"e\",\"componentName\":\"e\",\"ui\":\"node-red\"},\"text\":\"e\"},{\"id\":\"169c4767-bddd-49b0-8ca4-734cf987c553\",\"type\":\"common\",\"properties\":{\"componentId\":\"f\",\"componentName\":\"f\",\"ui\":\"node-red\"},\"text\":\"f\"},{\"id\":\"70dabb08-d7fb-463a-9208-04aae985a5b1\",\"type\":\"common\",\"properties\":{\"componentId\":\"g\",\"componentName\":\"g\",\"ui\":\"node-red\"},\"text\":\"g\"},{\"id\":\"4624733c-4023-4e0a-8c20-2ca7d1c1ceb6\",\"type\":\"common\",\"properties\":{\"componentId\":\"h\",\"componentName\":\"h\",\"ui\":\"node-red\"},\"text\":\"h\"}],\"edges\":[{\"sourceNodeId\":\"46c09860-15b6-46a6-b70a-020344c8de38\",\"targetNodeId\":\"d70c8a96-f8aa-45a2-ab5e-c4e6e8268721\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"a6741cab-ca3f-44d5-a22e-98d067da9a43\",\"targetNodeId\":\"d70c8a96-f8aa-45a2-ab5e-c4e6e8268721\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"d70c8a96-f8aa-45a2-ab5e-c4e6e8268721\",\"targetNodeId\":\"c0889832-ff85-43fc-83b3-930b9674e062\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"d70c8a96-f8aa-45a2-ab5e-c4e6e8268721\",\"targetNodeId\":\"bf08e701-854a-4adf-aa8b-a02ab181cd99\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"d70c8a96-f8aa-45a2-ab5e-c4e6e8268721\",\"targetNodeId\":\"169c4767-bddd-49b0-8ca4-734cf987c553\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"c0889832-ff85-43fc-83b3-930b9674e062\",\"targetNodeId\":\"70dabb08-d7fb-463a-9208-04aae985a5b1\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"bf08e701-854a-4adf-aa8b-a02ab181cd99\",\"targetNodeId\":\"70dabb08-d7fb-463a-9208-04aae985a5b1\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"70dabb08-d7fb-463a-9208-04aae985a5b1\",\"targetNodeId\":\"4624733c-4023-4e0a-8c20-2ca7d1c1ceb6\",\"properties\":{},\"text\":\"\"}]}', '{\"nodes\":[{\"id\":\"46c09860-15b6-46a6-b70a-020344c8de38\",\"type\":\"common\",\"x\":270,\"y\":100,\"properties\":{\"componentId\":\"a\",\"componentName\":\"a\",\"ui\":\"node-red\"},\"text\":{\"x\":290,\"y\":102,\"value\":\"a\"}},{\"id\":\"a6741cab-ca3f-44d5-a22e-98d067da9a43\",\"type\":\"common\",\"x\":270,\"y\":180,\"properties\":{\"componentId\":\"b\",\"componentName\":\"b\",\"ui\":\"node-red\"},\"text\":{\"x\":290,\"y\":182,\"value\":\"b\"}},{\"id\":\"d70c8a96-f8aa-45a2-ab5e-c4e6e8268721\",\"type\":\"common\",\"x\":460,\"y\":150,\"properties\":{\"componentId\":\"c\",\"componentName\":\"c\",\"ui\":\"node-red\"},\"text\":{\"x\":480,\"y\":152,\"value\":\"c\"}},{\"id\":\"c0889832-ff85-43fc-83b3-930b9674e062\",\"type\":\"common\",\"x\":660,\"y\":100,\"properties\":{\"componentId\":\"d\",\"componentName\":\"d\",\"ui\":\"node-red\"},\"text\":{\"x\":680,\"y\":102,\"value\":\"d\"}},{\"id\":\"bf08e701-854a-4adf-aa8b-a02ab181cd99\",\"type\":\"common\",\"x\":660,\"y\":190,\"properties\":{\"componentId\":\"e\",\"componentName\":\"e\",\"ui\":\"node-red\"},\"text\":{\"x\":680,\"y\":192,\"value\":\"e\"}},{\"id\":\"169c4767-bddd-49b0-8ca4-734cf987c553\",\"type\":\"common\",\"x\":660,\"y\":280,\"properties\":{\"componentId\":\"f\",\"componentName\":\"f\",\"ui\":\"node-red\"},\"text\":{\"x\":680,\"y\":282,\"value\":\"f\"}},{\"id\":\"70dabb08-d7fb-463a-9208-04aae985a5b1\",\"type\":\"common\",\"x\":870,\"y\":140,\"properties\":{\"componentId\":\"g\",\"componentName\":\"g\",\"ui\":\"node-red\"},\"text\":{\"x\":890,\"y\":142,\"value\":\"g\"}},{\"id\":\"4624733c-4023-4e0a-8c20-2ca7d1c1ceb6\",\"type\":\"common\",\"x\":1080,\"y\":140,\"properties\":{\"componentId\":\"h\",\"componentName\":\"h\",\"ui\":\"node-red\"},\"text\":{\"x\":1100,\"y\":142,\"value\":\"h\"}}],\"edges\":[{\"id\":\"360d0c61-e517-4f5e-9422-450d6cd909ae\",\"type\":\"flow-link\",\"sourceNodeId\":\"46c09860-15b6-46a6-b70a-020344c8de38\",\"targetNodeId\":\"d70c8a96-f8aa-45a2-ab5e-c4e6e8268721\",\"startPoint\":{\"x\":330,\"y\":100},\"endPoint\":{\"x\":400,\"y\":150},\"properties\":{},\"pointsList\":[{\"x\":330,\"y\":100},{\"x\":430,\"y\":100},{\"x\":300,\"y\":150},{\"x\":400,\"y\":150}]},{\"id\":\"b87fb883-81ef-4031-bd52-29158050fc29\",\"type\":\"flow-link\",\"sourceNodeId\":\"a6741cab-ca3f-44d5-a22e-98d067da9a43\",\"targetNodeId\":\"d70c8a96-f8aa-45a2-ab5e-c4e6e8268721\",\"startPoint\":{\"x\":330,\"y\":180},\"endPoint\":{\"x\":400,\"y\":150},\"properties\":{},\"pointsList\":[{\"x\":330,\"y\":180},{\"x\":430,\"y\":180},{\"x\":300,\"y\":150},{\"x\":400,\"y\":150}]},{\"id\":\"7c460aec-386c-4f91-a741-adca9eb06f44\",\"type\":\"flow-link\",\"sourceNodeId\":\"d70c8a96-f8aa-45a2-ab5e-c4e6e8268721\",\"targetNodeId\":\"c0889832-ff85-43fc-83b3-930b9674e062\",\"startPoint\":{\"x\":520,\"y\":150},\"endPoint\":{\"x\":600,\"y\":100},\"properties\":{},\"pointsList\":[{\"x\":520,\"y\":150},{\"x\":620,\"y\":150},{\"x\":500,\"y\":100},{\"x\":600,\"y\":100}]},{\"id\":\"33a151cf-ca6a-42c1-b168-f32a531a98cf\",\"type\":\"flow-link\",\"sourceNodeId\":\"d70c8a96-f8aa-45a2-ab5e-c4e6e8268721\",\"targetNodeId\":\"bf08e701-854a-4adf-aa8b-a02ab181cd99\",\"startPoint\":{\"x\":520,\"y\":150},\"endPoint\":{\"x\":600,\"y\":190},\"properties\":{},\"pointsList\":[{\"x\":520,\"y\":150},{\"x\":620,\"y\":150},{\"x\":500,\"y\":190},{\"x\":600,\"y\":190}]},{\"id\":\"ddebdb13-f70f-4540-a616-1c800977f1b7\",\"type\":\"flow-link\",\"sourceNodeId\":\"d70c8a96-f8aa-45a2-ab5e-c4e6e8268721\",\"targetNodeId\":\"169c4767-bddd-49b0-8ca4-734cf987c553\",\"startPoint\":{\"x\":520,\"y\":150},\"endPoint\":{\"x\":600,\"y\":280},\"properties\":{},\"pointsList\":[{\"x\":520,\"y\":150},{\"x\":620,\"y\":150},{\"x\":500,\"y\":280},{\"x\":600,\"y\":280}]},{\"id\":\"a6e740d6-50c9-4ca9-a547-bae18bc2f97d\",\"type\":\"flow-link\",\"sourceNodeId\":\"c0889832-ff85-43fc-83b3-930b9674e062\",\"targetNodeId\":\"70dabb08-d7fb-463a-9208-04aae985a5b1\",\"startPoint\":{\"x\":720,\"y\":100},\"endPoint\":{\"x\":810,\"y\":140},\"properties\":{},\"pointsList\":[{\"x\":720,\"y\":100},{\"x\":820,\"y\":100},{\"x\":710,\"y\":140},{\"x\":810,\"y\":140}]},{\"id\":\"1590a6f8-dba8-4808-b18a-e98d1055c504\",\"type\":\"flow-link\",\"sourceNodeId\":\"bf08e701-854a-4adf-aa8b-a02ab181cd99\",\"targetNodeId\":\"70dabb08-d7fb-463a-9208-04aae985a5b1\",\"startPoint\":{\"x\":720,\"y\":190},\"endPoint\":{\"x\":810,\"y\":140},\"properties\":{},\"pointsList\":[{\"x\":720,\"y\":190},{\"x\":820,\"y\":190},{\"x\":710,\"y\":140},{\"x\":810,\"y\":140}]},{\"id\":\"0ba38b5c-9e9b-4cc8-acea-ea1a313143cd\",\"type\":\"flow-link\",\"sourceNodeId\":\"70dabb08-d7fb-463a-9208-04aae985a5b1\",\"targetNodeId\":\"4624733c-4023-4e0a-8c20-2ca7d1c1ceb6\",\"startPoint\":{\"x\":930,\"y\":140},\"endPoint\":{\"x\":1020,\"y\":140},\"properties\":{},\"pointsList\":[{\"x\":930,\"y\":140},{\"x\":1030,\"y\":140},{\"x\":920,\"y\":140},{\"x\":1020,\"y\":140}]}]}', NULL);
INSERT INTO `ivy_el` VALUES (9, 'el7', '测试流程7', NULL, '{\"nodes\":[{\"id\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"type\":\"common\",\"properties\":{\"componentId\":\"aa\",\"componentName\":\"aa\",\"ui\":\"node-red\"},\"text\":\"aa\"},{\"id\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"type\":\"common\",\"properties\":{\"componentId\":\"bb\",\"componentName\":\"bb\",\"ui\":\"node-red\"},\"text\":\"bb\"},{\"id\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"type\":\"common\",\"properties\":{\"componentId\":\"cc\",\"componentName\":\"cc\",\"ui\":\"node-red\"},\"text\":\"cc\"},{\"id\":\"44faa8fe-16a2-4f09-b992-2d0234b01563\",\"type\":\"common\",\"properties\":{\"componentId\":\"dd\",\"componentName\":\"dd\",\"ui\":\"node-red\"},\"text\":\"dd\"},{\"id\":\"de6d9cee-1271-4c3d-b930-706891a151ff\",\"type\":\"common\",\"properties\":{\"componentId\":\"ee\",\"componentName\":\"ee\",\"ui\":\"node-red\"},\"text\":\"ee\"},{\"id\":\"42d4f738-e551-4694-99d3-c165c7336af7\",\"type\":\"common\",\"properties\":{\"componentId\":\"ff\",\"componentName\":\"ff\",\"ui\":\"node-red\"},\"text\":\"ff\"},{\"id\":\"57776b0b-c8a0-4868-a513-5489f0ece891\",\"type\":\"common\",\"properties\":{\"componentId\":\"gg\",\"componentName\":\"gg\",\"ui\":\"node-red\"},\"text\":\"gg\"},{\"id\":\"c858aab8-d7df-4a41-8726-3719cfa0f47c\",\"type\":\"common\",\"properties\":{\"componentId\":\"hh\",\"componentName\":\"hh\",\"ui\":\"node-red\"},\"text\":\"hh\"}],\"edges\":[{\"sourceNodeId\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"targetNodeId\":\"42d4f738-e551-4694-99d3-c165c7336af7\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"42d4f738-e551-4694-99d3-c165c7336af7\",\"targetNodeId\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"42d4f738-e551-4694-99d3-c165c7336af7\",\"targetNodeId\":\"44faa8fe-16a2-4f09-b992-2d0234b01563\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"57776b0b-c8a0-4868-a513-5489f0ece891\",\"targetNodeId\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"57776b0b-c8a0-4868-a513-5489f0ece891\",\"targetNodeId\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"targetNodeId\":\"de6d9cee-1271-4c3d-b930-706891a151ff\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"targetNodeId\":\"de6d9cee-1271-4c3d-b930-706891a151ff\",\"properties\":{},\"text\":\"\"},{\"sourceNodeId\":\"de6d9cee-1271-4c3d-b930-706891a151ff\",\"targetNodeId\":\"c858aab8-d7df-4a41-8726-3719cfa0f47c\",\"properties\":{},\"text\":\"\"}]}', '{\"nodes\":[{\"id\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"type\":\"common\",\"x\":420,\"y\":80,\"properties\":{\"componentId\":\"aa\",\"componentName\":\"aa\",\"ui\":\"node-red\"},\"text\":{\"x\":440,\"y\":82,\"value\":\"aa\"}},{\"id\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"type\":\"common\",\"x\":420,\"y\":180,\"properties\":{\"componentId\":\"bb\",\"componentName\":\"bb\",\"ui\":\"node-red\"},\"text\":{\"x\":440,\"y\":182,\"value\":\"bb\"}},{\"id\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"type\":\"common\",\"x\":820,\"y\":140,\"properties\":{\"componentId\":\"cc\",\"componentName\":\"cc\",\"ui\":\"node-red\"},\"text\":{\"x\":840,\"y\":142,\"value\":\"cc\"}},{\"id\":\"44faa8fe-16a2-4f09-b992-2d0234b01563\",\"type\":\"common\",\"x\":820,\"y\":220,\"properties\":{\"componentId\":\"dd\",\"componentName\":\"dd\",\"ui\":\"node-red\"},\"text\":{\"x\":840,\"y\":222,\"value\":\"dd\"}},{\"id\":\"de6d9cee-1271-4c3d-b930-706891a151ff\",\"type\":\"common\",\"x\":1010,\"y\":100,\"properties\":{\"componentId\":\"ee\",\"componentName\":\"ee\",\"ui\":\"node-red\"},\"text\":{\"x\":1030,\"y\":102,\"value\":\"ee\"}},{\"id\":\"42d4f738-e551-4694-99d3-c165c7336af7\",\"type\":\"common\",\"x\":620,\"y\":180,\"properties\":{\"componentId\":\"ff\",\"componentName\":\"ff\",\"ui\":\"node-red\"},\"text\":{\"x\":640,\"y\":182,\"value\":\"ff\"}},{\"id\":\"57776b0b-c8a0-4868-a513-5489f0ece891\",\"type\":\"common\",\"x\":230,\"y\":130,\"properties\":{\"componentId\":\"gg\",\"componentName\":\"gg\",\"ui\":\"node-red\"},\"text\":{\"x\":250,\"y\":132,\"value\":\"gg\"}},{\"id\":\"c858aab8-d7df-4a41-8726-3719cfa0f47c\",\"type\":\"common\",\"x\":1190,\"y\":100,\"properties\":{\"componentId\":\"hh\",\"componentName\":\"hh\",\"ui\":\"node-red\"},\"text\":{\"x\":1210,\"y\":102,\"value\":\"hh\"}}],\"edges\":[{\"id\":\"5dcf3284-f2ed-433e-81c4-57dfc6eeb612\",\"type\":\"flow-link\",\"sourceNodeId\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"targetNodeId\":\"42d4f738-e551-4694-99d3-c165c7336af7\",\"startPoint\":{\"x\":480,\"y\":180},\"endPoint\":{\"x\":560,\"y\":180},\"properties\":{},\"pointsList\":[{\"x\":480,\"y\":180},{\"x\":580,\"y\":180},{\"x\":460,\"y\":180},{\"x\":560,\"y\":180}]},{\"id\":\"9b2e6a2f-ce8d-4487-81dd-f8b7e24ba0a7\",\"type\":\"flow-link\",\"sourceNodeId\":\"42d4f738-e551-4694-99d3-c165c7336af7\",\"targetNodeId\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"startPoint\":{\"x\":680,\"y\":180},\"endPoint\":{\"x\":760,\"y\":140},\"properties\":{},\"pointsList\":[{\"x\":680,\"y\":180},{\"x\":780,\"y\":180},{\"x\":660,\"y\":140},{\"x\":760,\"y\":140}]},{\"id\":\"b3235fba-6202-4e1d-a29b-3a1b3d26f523\",\"type\":\"flow-link\",\"sourceNodeId\":\"42d4f738-e551-4694-99d3-c165c7336af7\",\"targetNodeId\":\"44faa8fe-16a2-4f09-b992-2d0234b01563\",\"startPoint\":{\"x\":680,\"y\":180},\"endPoint\":{\"x\":760,\"y\":220},\"properties\":{},\"pointsList\":[{\"x\":680,\"y\":180},{\"x\":780,\"y\":180},{\"x\":660,\"y\":220},{\"x\":760,\"y\":220}]},{\"id\":\"4dcaf8e0-76ac-4336-aa56-2f1b4388a076\",\"type\":\"flow-link\",\"sourceNodeId\":\"57776b0b-c8a0-4868-a513-5489f0ece891\",\"targetNodeId\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"startPoint\":{\"x\":290,\"y\":130},\"endPoint\":{\"x\":360,\"y\":80},\"properties\":{},\"pointsList\":[{\"x\":290,\"y\":130},{\"x\":390,\"y\":130},{\"x\":260,\"y\":80},{\"x\":360,\"y\":80}]},{\"id\":\"8709ed5a-ef16-4ba0-95ae-65ddc4335d90\",\"type\":\"flow-link\",\"sourceNodeId\":\"57776b0b-c8a0-4868-a513-5489f0ece891\",\"targetNodeId\":\"75927a55-9d56-45b6-8a74-6e43a142bc19\",\"startPoint\":{\"x\":290,\"y\":130},\"endPoint\":{\"x\":360,\"y\":180},\"properties\":{},\"pointsList\":[{\"x\":290,\"y\":130},{\"x\":390,\"y\":130},{\"x\":260,\"y\":180},{\"x\":360,\"y\":180}]},{\"id\":\"02882dfe-907a-4208-af51-a6a51030f471\",\"type\":\"flow-link\",\"sourceNodeId\":\"e3dffa91-f49d-44e0-8746-d8f95e029b3c\",\"targetNodeId\":\"de6d9cee-1271-4c3d-b930-706891a151ff\",\"startPoint\":{\"x\":480,\"y\":80},\"endPoint\":{\"x\":950,\"y\":100},\"properties\":{},\"pointsList\":[{\"x\":480,\"y\":80},{\"x\":580,\"y\":80},{\"x\":850,\"y\":100},{\"x\":950,\"y\":100}]},{\"id\":\"ae34bd15-cda0-4c1d-9c07-791632f0046b\",\"type\":\"flow-link\",\"sourceNodeId\":\"24a14b89-7067-448d-8178-0ff4c542deb5\",\"targetNodeId\":\"de6d9cee-1271-4c3d-b930-706891a151ff\",\"startPoint\":{\"x\":880,\"y\":140},\"endPoint\":{\"x\":950,\"y\":100},\"properties\":{},\"pointsList\":[{\"x\":880,\"y\":140},{\"x\":980,\"y\":140},{\"x\":850,\"y\":100},{\"x\":950,\"y\":100}]},{\"id\":\"d09ca788-dd9b-405e-b349-304c7d66c077\",\"type\":\"flow-link\",\"sourceNodeId\":\"de6d9cee-1271-4c3d-b930-706891a151ff\",\"targetNodeId\":\"c858aab8-d7df-4a41-8726-3719cfa0f47c\",\"startPoint\":{\"x\":1070,\"y\":100},\"endPoint\":{\"x\":1130,\"y\":100},\"properties\":{},\"pointsList\":[{\"x\":1070,\"y\":100},{\"x\":1170,\"y\":100},{\"x\":1030,\"y\":100},{\"x\":1130,\"y\":100}]}]}', NULL);

-- ----------------------------
-- Table structure for ivy_executor
-- ----------------------------
DROP TABLE IF EXISTS `ivy_executor`;
CREATE TABLE `ivy_executor`  (
                                 `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                 `executor_id` varchar(32)  NULL DEFAULT NULL COMMENT '执行器ID',
                                 `executor_name` varchar(32)  NULL DEFAULT NULL COMMENT '执行器名称',
                                 `ivy_config_id` bigint(0) NULL DEFAULT NULL COMMENT '执行器配置IvyConfig',
                                 `executor_type` varchar(20)  NULL DEFAULT NULL COMMENT '执行器类型【execute2Resp:execute2Resp,execute2Future:execute2Future】',
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 2  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of ivy_executor
-- ----------------------------
INSERT INTO `ivy_executor` VALUES (1, 'ivy_executor', '自定义执行器', 9, 'execute2Future');

-- ----------------------------
-- Table structure for ivy_lf_el_table
-- ----------------------------
DROP TABLE IF EXISTS `ivy_lf_el_table`;
CREATE TABLE `ivy_lf_el_table`  (
                                    `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                    `application_name` varchar(32)  NOT NULL,
                                    `chain_name` varchar(32)  NOT NULL,
                                    `el_data` varchar(1024)  NOT NULL,
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ivy_lf_script_node_table
-- ----------------------------
DROP TABLE IF EXISTS `ivy_lf_script_node_table`;
CREATE TABLE `ivy_lf_script_node_table`  (
                                             `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                             `application_name` varchar(32)  NOT NULL,
                                             `script_node_id` varchar(32)  NOT NULL,
                                             `script_node_name` varchar(32)  NOT NULL,
                                             `script_node_type` varchar(32)  NOT NULL,
                                             `script_node_data` varchar(1024)  NOT NULL,
                                             `script_language` varchar(1024)  NOT NULL,
                                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for ivy_task
-- ----------------------------
DROP TABLE IF EXISTS `ivy_task`;
CREATE TABLE `ivy_task`  (
                             `id` bigint(0) NOT NULL AUTO_INCREMENT,
                             `task_id` varchar(32)  NULL DEFAULT NULL COMMENT '任务ID',
                             `task_name` varchar(32)  NULL DEFAULT NULL COMMENT '链路名称',
                             PRIMARY KEY (`id`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for xxl_job_group
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_group`;
CREATE TABLE `xxl_job_group`  (
                                  `id` int(0) NOT NULL AUTO_INCREMENT,
                                  `app_name` varchar(64)  NOT NULL COMMENT '执行器AppName',
                                  `title` varchar(12)  NOT NULL COMMENT '执行器名称',
                                  `address_type` tinyint(0) NOT NULL DEFAULT 0 COMMENT '执行器地址类型：0=自动注册、1=手动录入',
                                  `address_list` text  NULL COMMENT '执行器地址列表，多地址逗号分隔',
                                  `update_time` datetime(0) NULL DEFAULT NULL,
                                  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xxl_job_group
-- ----------------------------
INSERT INTO `xxl_job_group` VALUES (1, 'xxl-job-executor-sample', '自动注册任务执行器', 0, NULL, '2024-01-29 09:53:17');
INSERT INTO `xxl_job_group` VALUES (2, 'xxl-job-executor-sample-1', '手动注册任务执行器', 1, '127.0.0.1:9090,127.0.0.1:8080', '2023-12-12 13:54:48');

-- ----------------------------
-- Table structure for xxl_job_info
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_info`;
CREATE TABLE `xxl_job_info`  (
                                 `id` int(0) NOT NULL AUTO_INCREMENT,
                                 `job_group` int(0) NOT NULL COMMENT '执行器主键ID',
                                 `job_desc` varchar(255)  NOT NULL,
                                 `add_time` datetime(0) NULL DEFAULT NULL,
                                 `update_time` datetime(0) NULL DEFAULT NULL,
                                 `author` varchar(64)  NULL DEFAULT NULL COMMENT '作者',
                                 `alarm_email` varchar(255)  NULL DEFAULT NULL COMMENT '报警邮件',
                                 `schedule_type` varchar(50)  NOT NULL DEFAULT 'NONE' COMMENT '调度类型',
                                 `schedule_conf` varchar(128)  NULL DEFAULT NULL COMMENT '调度配置，值含义取决于调度类型',
                                 `misfire_strategy` varchar(50)  NOT NULL DEFAULT 'DO_NOTHING' COMMENT '调度过期策略',
                                 `executor_route_strategy` varchar(50)  NULL DEFAULT NULL COMMENT '执行器路由策略',
                                 `executor_handler` varchar(255)  NULL DEFAULT NULL COMMENT '执行器任务handler',
                                 `executor_param` varchar(512)  NULL DEFAULT NULL COMMENT '执行器任务参数',
                                 `executor_block_strategy` varchar(50)  NULL DEFAULT NULL COMMENT '阻塞处理策略',
                                 `executor_timeout` int(0) NOT NULL DEFAULT 0 COMMENT '任务执行超时时间，单位秒',
                                 `executor_fail_retry_count` int(0) NOT NULL DEFAULT 0 COMMENT '失败重试次数',
                                 `glue_type` varchar(50)  NOT NULL COMMENT 'GLUE类型',
                                 `glue_source` mediumtext  NULL COMMENT 'GLUE源代码',
                                 `glue_remark` varchar(128)  NULL DEFAULT NULL COMMENT 'GLUE备注',
                                 `glue_updatetime` datetime(0) NULL DEFAULT NULL COMMENT 'GLUE更新时间',
                                 `child_jobid` varchar(255)  NULL DEFAULT NULL COMMENT '子任务ID，多个逗号分隔',
                                 `trigger_status` tinyint(0) NOT NULL DEFAULT 0 COMMENT '调度状态：0-停止，1-运行',
                                 `trigger_last_time` bigint(0) NOT NULL DEFAULT 0 COMMENT '上次调度时间',
                                 `trigger_next_time` bigint(0) NOT NULL DEFAULT 0 COMMENT '下次调度时间',
                                 `chain_id` varchar(32)  NULL DEFAULT NULL,
                                 PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xxl_job_info
-- ----------------------------
INSERT INTO `xxl_job_info` VALUES (1, 1, '测试任务1', '2023-12-13 10:44:48', '2023-12-26 18:01:48', 'XXL', 'xxx@qq.com', 'CRON', '0 0/10 * * * ?', 'DO_NOTHING', 'FIRST', 'ivyJobHandler', '', 'SERIAL_EXECUTION', 0, 0, 'LITE_FLOW', NULL, NULL, '2023-12-13 10:44:58', '', 0, 0, 0, 'ivy_chain');
INSERT INTO `xxl_job_info` VALUES (3, 1, '测试任务2', '2023-12-13 13:57:07', '2023-12-13 13:57:07', 'XXL', 'xxx@qq.com', 'CRON', '0 0/5 * * * ?', 'DO_NOTHING', 'FIRST', 'demoJobHandler', '', 'SERIAL_EXECUTION', 0, 0, 'BEAN', NULL, NULL, '2023-12-13 13:57:07', NULL, 0, 0, 0, NULL);
INSERT INTO `xxl_job_info` VALUES (4, 1, '测试任务3', '2023-12-13 13:58:20', '2023-12-13 17:35:58', 'XXL', 'xxx@qq.com', 'CRON', '0 0/5 * * * ?', 'DO_NOTHING', 'FIRST', '', '', 'SERIAL_EXECUTION', 0, 0, 'GLUE_GROOVY', 'package com.xxl.job.service.handler;\n\nimport com.xxl.job.core.context.XxlJobHelper;\nimport com.xxl.job.core.handler.IJobHandler;\n\npublic class DemoGlueJobHandler extends IJobHandler {\n\n    @Override\n    public void execute() throws Exception {\n        XxlJobHelper.log(\"XXL-JOB, Hello World1111.\");\n    }\n\n}', '备注1112', '2023-12-13 15:34:00', NULL, 0, 0, 0, NULL);

-- ----------------------------
-- Table structure for xxl_job_lock
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_lock`;
CREATE TABLE `xxl_job_lock`  (
                                 `lock_name` varchar(50)  NOT NULL COMMENT '锁名称',
                                 PRIMARY KEY (`lock_name`) USING BTREE
) ENGINE = InnoDB  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xxl_job_lock
-- ----------------------------
INSERT INTO `xxl_job_lock` VALUES ('schedule_lock');

-- ----------------------------
-- Table structure for xxl_job_log
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_log`;
CREATE TABLE `xxl_job_log`  (
                                `id` bigint(0) NOT NULL AUTO_INCREMENT,
                                `job_group` int(0) NOT NULL COMMENT '执行器主键ID',
                                `job_id` int(0) NOT NULL COMMENT '任务，主键ID',
                                `executor_address` varchar(255)  NULL DEFAULT NULL COMMENT '执行器地址，本次执行的地址',
                                `executor_handler` varchar(255)  NULL DEFAULT NULL COMMENT '执行器任务handler',
                                `executor_param` varchar(512)  NULL DEFAULT NULL COMMENT '执行器任务参数',
                                `executor_sharding_param` varchar(20)  NULL DEFAULT NULL COMMENT '执行器任务分片参数，格式如 1/2',
                                `executor_fail_retry_count` int(0) NOT NULL DEFAULT 0 COMMENT '失败重试次数',
                                `trigger_time` datetime(0) NULL DEFAULT NULL COMMENT '调度-时间',
                                `trigger_code` int(0) NOT NULL COMMENT '调度-结果',
                                `trigger_msg` text  NULL COMMENT '调度-日志',
                                `handle_time` datetime(0) NULL DEFAULT NULL COMMENT '执行-时间',
                                `handle_code` int(0) NOT NULL COMMENT '执行-状态',
                                `handle_msg` text  NULL COMMENT '执行-日志',
                                `alarm_status` tinyint(0) NOT NULL DEFAULT 0 COMMENT '告警状态：0-默认、1-无需告警、2-告警成功、3-告警失败',
                                PRIMARY KEY (`id`) USING BTREE,
                                INDEX `I_trigger_time`(`trigger_time`) USING BTREE,
                                INDEX `I_handle_code`(`handle_code`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 17  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for xxl_job_log_report
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_log_report`;
CREATE TABLE `xxl_job_log_report`  (
                                       `id` int(0) NOT NULL AUTO_INCREMENT,
                                       `trigger_day` datetime(0) NULL DEFAULT NULL COMMENT '调度-时间',
                                       `running_count` int(0) NOT NULL DEFAULT 0 COMMENT '运行中-日志数量',
                                       `suc_count` int(0) NOT NULL DEFAULT 0 COMMENT '执行成功-日志数量',
                                       `fail_count` int(0) NOT NULL DEFAULT 0 COMMENT '执行失败-日志数量',
                                       `update_time` datetime(0) NULL DEFAULT NULL,
                                       PRIMARY KEY (`id`) USING BTREE,
                                       UNIQUE INDEX `i_trigger_day`(`trigger_day`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 56  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xxl_job_log_report
-- ----------------------------
INSERT INTO `xxl_job_log_report` VALUES (1, '2023-12-04 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (2, '2023-12-03 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (3, '2023-12-02 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (4, '2023-12-05 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (5, '2023-12-06 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (6, '2023-12-07 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (7, '2023-12-08 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (8, '2023-12-11 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (9, '2023-12-10 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (10, '2023-12-09 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (11, '2023-12-12 00:00:00', 0, 0, 1, NULL);
INSERT INTO `xxl_job_log_report` VALUES (12, '2023-12-13 00:00:00', 0, 0, 3, NULL);
INSERT INTO `xxl_job_log_report` VALUES (13, '2023-12-14 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (14, '2023-12-15 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (15, '2023-12-16 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (16, '2023-12-18 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (17, '2023-12-17 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (18, '2023-12-19 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (19, '2023-12-21 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (20, '2023-12-20 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (21, '2023-12-25 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (22, '2023-12-24 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (23, '2023-12-23 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (24, '2023-12-26 00:00:00', 0, 2, 2, NULL);
INSERT INTO `xxl_job_log_report` VALUES (25, '2023-12-28 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (26, '2023-12-27 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (27, '2024-01-02 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (28, '2024-01-01 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (29, '2023-12-31 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (30, '2024-01-04 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (31, '2024-01-03 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (32, '2024-01-05 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (33, '2024-01-06 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (34, '2024-01-08 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (35, '2024-01-07 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (36, '2024-01-09 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (37, '2024-01-11 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (38, '2024-01-10 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (39, '2024-01-12 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (40, '2024-01-15 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (41, '2024-01-14 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (42, '2024-01-13 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (43, '2024-01-16 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (44, '2024-01-18 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (45, '2024-01-17 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (46, '2024-01-19 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (47, '2024-01-20 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (48, '2024-01-23 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (49, '2024-01-22 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (50, '2024-01-21 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (51, '2024-01-24 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (52, '2024-01-25 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (53, '2024-01-29 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (54, '2024-01-28 00:00:00', 0, 0, 0, NULL);
INSERT INTO `xxl_job_log_report` VALUES (55, '2024-01-27 00:00:00', 0, 0, 0, NULL);

-- ----------------------------
-- Table structure for xxl_job_logglue
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_logglue`;
CREATE TABLE `xxl_job_logglue`  (
                                    `id` int(0) NOT NULL AUTO_INCREMENT,
                                    `job_id` int(0) NOT NULL COMMENT '任务，主键ID',
                                    `glue_type` varchar(50)  NULL DEFAULT NULL COMMENT 'GLUE类型',
                                    `glue_source` mediumtext  NULL COMMENT 'GLUE源代码',
                                    `glue_remark` varchar(128)  NOT NULL COMMENT 'GLUE备注',
                                    `add_time` datetime(0) NULL DEFAULT NULL,
                                    `update_time` datetime(0) NULL DEFAULT NULL,
                                    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xxl_job_logglue
-- ----------------------------
INSERT INTO `xxl_job_logglue` VALUES (1, 4, 'GLUE_GROOVY', 'package com.xxl.job.service.handler;\n\nimport com.xxl.job.core.context.XxlJobHelper;\nimport com.xxl.job.core.handler.IJobHandler;\n\npublic class DemoGlueJobHandler extends IJobHandler {\n\n    @Override\n    public void execute() throws Exception {\n        XxlJobHelper.log(\"XXL-JOB, Hello World.\");\n    }\n\n}', '备注1111', '2023-12-13 15:25:04', '2023-12-13 15:25:04');
INSERT INTO `xxl_job_logglue` VALUES (2, 4, 'GLUE_GROOVY', 'package com.xxl.job.service.handler;\n\nimport com.xxl.job.core.context.XxlJobHelper;\nimport com.xxl.job.core.handler.IJobHandler;\n\npublic class DemoGlueJobHandler extends IJobHandler {\n\n    @Override\n    public void execute() throws Exception {\n        XxlJobHelper.log(\"XXL-JOB, Hello World1111.\");\n    }\n\n}', '备注1112', '2023-12-13 15:34:00', '2023-12-13 15:34:00');

-- ----------------------------
-- Table structure for xxl_job_registry
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_registry`;
CREATE TABLE `xxl_job_registry`  (
                                     `id` int(0) NOT NULL AUTO_INCREMENT,
                                     `registry_group` varchar(50)  NOT NULL,
                                     `registry_key` varchar(255)  NOT NULL,
                                     `registry_value` varchar(255)  NOT NULL,
                                     `update_time` datetime(0) NULL DEFAULT NULL,
                                     PRIMARY KEY (`id`) USING BTREE,
                                     INDEX `i_g_k_v`(`registry_group`, `registry_key`, `registry_value`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for xxl_job_user
-- ----------------------------
DROP TABLE IF EXISTS `xxl_job_user`;
CREATE TABLE `xxl_job_user`  (
                                 `id` int(0) NOT NULL AUTO_INCREMENT,
                                 `username` varchar(50)  NOT NULL COMMENT '账号',
                                 `password` varchar(50)  NOT NULL COMMENT '密码',
                                 `role` tinyint(0) NOT NULL COMMENT '角色：0-普通用户、1-管理员',
                                 `permission` varchar(255)  NULL DEFAULT NULL COMMENT '权限：执行器ID列表，多个逗号分割',
                                 PRIMARY KEY (`id`) USING BTREE,
                                 UNIQUE INDEX `i_username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1  ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of xxl_job_user
-- ----------------------------
INSERT INTO `xxl_job_user` VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 1, NULL);

SET FOREIGN_KEY_CHECKS = 1;
