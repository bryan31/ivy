-- 建表语句
CREATE TABLE `ivy_config` (
`id` BIGINT(20)  NOT NULL AUTO_INCREMENT ,
`config_id` VARCHAR(32)  DEFAULT NULL ,
`config_name` VARCHAR(32)  DEFAULT NULL ,
`rule_type` VARCHAR(12)  DEFAULT NULL  COMMENT '规则类型【ruleSource:本地规则文件配置,ruleSourceZk:ZK规则文件配置源,ruleSourceSql:SQL数据库配置源,ruleSourceNacos:Nacos配置源,ruleSourceEtcd:Etcd配置源,ruleSourceApollo:Apollo配置源,ruleSourceRedisPoll:Redis配置源-轮询模式,ruleSourceRedisSub:Redis配置源-订阅模式,ruleSourceCustom:自定义配置源】',
`config_type` INTEGER(1)  DEFAULT NULL  COMMENT '配置项类型【1:ruleSourceExtDataMap,2:ruleSourceExtData】',
`rule_source` TEXT  DEFAULT NULL ,
`rule_source_ext_data_map` TEXT  DEFAULT NULL ,
`rule_source_ext_data` TEXT  DEFAULT NULL ,
`enable` TINYINT(1)  DEFAULT NULL ,
`print_banner` TINYINT(1)  DEFAULT NULL ,
`zk_node` VARCHAR(64)  DEFAULT NULL ,
`slot_size` INTEGER(11)  DEFAULT NULL ,
`main_executor_works` INTEGER(11)  DEFAULT NULL ,
`main_executor_class` VARCHAR(128)  DEFAULT NULL ,
`request_id_generator_class` VARCHAR(128)  DEFAULT NULL ,
`thread_executor_class` VARCHAR(128)  DEFAULT NULL ,
`when_max_wait_time` INTEGER(11)  DEFAULT NULL ,
`when_max_wait_time_unit` VARCHAR(32)  DEFAULT NULL ,
`when_max_workers` INTEGER(11)  DEFAULT NULL ,
`when_queue_limit` INTEGER(11)  DEFAULT NULL ,
`when_thread_pool_isolate` TINYINT(1)  DEFAULT NULL ,
`parallel_loop_max_workers` INTEGER(11)  DEFAULT NULL ,
`parallel_loop_queue_limit` INTEGER(11)  DEFAULT NULL ,
`parallel_loop_executor_class` VARCHAR(128)  DEFAULT NULL ,
`parse_on_start` TINYINT(1)  DEFAULT NULL ,
`retry_count` INTEGER(11)  DEFAULT NULL ,
`support_multiple_type` TINYINT(1)  DEFAULT NULL ,
`node_executor_class` VARCHAR(128)  DEFAULT NULL ,
`print_execution_log` TINYINT(1)  DEFAULT NULL ,
`enable_monitor_file` TINYINT(1)  DEFAULT NULL ,
`fast_load` TINYINT(1)  DEFAULT NULL ,
`enable_log` TINYINT(1)  DEFAULT NULL ,
`queue_limit` INTEGER(11)  DEFAULT NULL ,
`delay` BIGINT(20)  DEFAULT NULL ,
`period` BIGINT(20)  DEFAULT NULL ,
`fallback_cmp_enable` TINYINT(1)  DEFAULT NULL ,
PRIMARY KEY (`id`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- 新增字段
ALTER TABLE `ivy_config` ADD COLUMN id BIGINT(20)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN config_id VARCHAR(32)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN config_name VARCHAR(32)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN rule_type VARCHAR(12)  DEFAULT NULL  COMMENT '规则类型【ruleSource:本地规则文件配置,ruleSourceZk:ZK规则文件配置源,ruleSourceSql:SQL数据库配置源,ruleSourceNacos:Nacos配置源,ruleSourceEtcd:Etcd配置源,ruleSourceApollo:Apollo配置源,ruleSourceRedisPoll:Redis配置源-轮询模式,ruleSourceRedisSub:Redis配置源-订阅模式,ruleSourceCustom:自定义配置源】';
ALTER TABLE `ivy_config` ADD COLUMN config_type INTEGER(1)  DEFAULT NULL  COMMENT '配置项类型【1:ruleSourceExtDataMap,2:ruleSourceExtData】';
ALTER TABLE `ivy_config` ADD COLUMN rule_source TEXT  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN rule_source_ext_data_map TEXT  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN rule_source_ext_data TEXT  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN enable TINYINT(1)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN print_banner TINYINT(1)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN zk_node VARCHAR(64)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN slot_size INTEGER(11)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN main_executor_works INTEGER(11)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN main_executor_class VARCHAR(128)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN request_id_generator_class VARCHAR(128)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN thread_executor_class VARCHAR(128)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN when_max_wait_time INTEGER(11)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN when_max_wait_time_unit VARCHAR(32)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN when_max_workers INTEGER(11)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN when_queue_limit INTEGER(11)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN when_thread_pool_isolate TINYINT(1)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN parallel_loop_max_workers INTEGER(11)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN parallel_loop_queue_limit INTEGER(11)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN parallel_loop_executor_class VARCHAR(128)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN parse_on_start TINYINT(1)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN retry_count INTEGER(11)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN support_multiple_type TINYINT(1)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN node_executor_class VARCHAR(128)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN print_execution_log TINYINT(1)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN enable_monitor_file TINYINT(1)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN fast_load TINYINT(1)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN enable_log TINYINT(1)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN queue_limit INTEGER(11)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN delay BIGINT(20)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN period BIGINT(20)  DEFAULT NULL ;
ALTER TABLE `ivy_config` ADD COLUMN fallback_cmp_enable TINYINT(1)  DEFAULT NULL ;

