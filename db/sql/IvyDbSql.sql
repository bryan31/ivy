-- 建表语句
CREATE TABLE `ivy_db_sql` (
`id` BIGINT(20)  NOT NULL AUTO_INCREMENT ,
`sql_manager_id` BIGINT(20)  DEFAULT NULL  COMMENT 'ivy_db_sql_manager表ID',
`sql_language` VARCHAR(20)  DEFAULT NULL  COMMENT 'SQL语言【DDL:数据定义语言,DML:数据操作语言,DCL:数据控制语言,TCL:事务控制语言】',
`table_name` VARCHAR(32)  DEFAULT NULL  COMMENT '表名',
`table_type` VARCHAR(20)  DEFAULT NULL  COMMENT '操作类型【single:单表操作,multiple:多表操作】',
`sql_id` VARCHAR(32)  DEFAULT NULL  COMMENT 'sqlId,如：selectUserById',
`sql_name` VARCHAR(32)  DEFAULT NULL  COMMENT 'sql名称',
`sql_desc` VARCHAR(255)  DEFAULT NULL  COMMENT '描述信息',
`sql_type` VARCHAR(12)  DEFAULT NULL  COMMENT 'SQL类型【select:select,insert:insert,update:update,delete:delete】',
`sql_template` TEXT  DEFAULT NULL  COMMENT 'sql模板',
`sql_json` TEXT  DEFAULT NULL  COMMENT 'sql配置',
`disabled` TINYINT(1)  DEFAULT NULL  COMMENT '是否禁用【0:启用,1:禁用】',
PRIMARY KEY (`id`)
)ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

-- 新增字段
ALTER TABLE `ivy_db_sql` ADD COLUMN id BIGINT(20)  DEFAULT NULL ;
ALTER TABLE `ivy_db_sql` ADD COLUMN sql_manager_id BIGINT(20)  DEFAULT NULL  COMMENT 'ivy_db_sql_manager表ID';
ALTER TABLE `ivy_db_sql` ADD COLUMN sql_language VARCHAR(20)  DEFAULT NULL  COMMENT 'SQL语言【DDL:数据定义语言,DML:数据操作语言,DCL:数据控制语言,TCL:事务控制语言】';
ALTER TABLE `ivy_db_sql` ADD COLUMN table_name VARCHAR(32)  DEFAULT NULL  COMMENT '表名';
ALTER TABLE `ivy_db_sql` ADD COLUMN table_type VARCHAR(20)  DEFAULT NULL  COMMENT '操作类型【single:单表操作,multiple:多表操作】';
ALTER TABLE `ivy_db_sql` ADD COLUMN sql_id VARCHAR(32)  DEFAULT NULL  COMMENT 'sqlId,如：selectUserById';
ALTER TABLE `ivy_db_sql` ADD COLUMN sql_name VARCHAR(32)  DEFAULT NULL  COMMENT 'sql名称';
ALTER TABLE `ivy_db_sql` ADD COLUMN sql_desc VARCHAR(255)  DEFAULT NULL  COMMENT '描述信息';
ALTER TABLE `ivy_db_sql` ADD COLUMN sql_type VARCHAR(12)  DEFAULT NULL  COMMENT 'SQL类型【select:select,insert:insert,update:update,delete:delete】';
ALTER TABLE `ivy_db_sql` ADD COLUMN sql_template TEXT  DEFAULT NULL  COMMENT 'sql模板';
ALTER TABLE `ivy_db_sql` ADD COLUMN sql_json TEXT  DEFAULT NULL  COMMENT 'sql配置';
ALTER TABLE `ivy_db_sql` ADD COLUMN disabled TINYINT(1)  DEFAULT NULL  COMMENT '是否禁用【0:启用,1:禁用】';

