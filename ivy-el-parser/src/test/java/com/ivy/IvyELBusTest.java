package com.ivy;

import com.ivy.builder.graph.IvyCmp;
import com.ivy.parser.bus.*;

public class IvyELBusTest {

    public static void main(String[] args) {
        //testELBusNode();
        //testELBusAnd();
        //testELBusOr();
        //testELBusNot();
        testELBusCatch();
    }

    public static void testELBusCatch(){
        String el1 = ELBusCatch.catchException("a").doOpt("b").id("id").tag("tag").maxWaitSeconds(100).toEL();
        System.out.println(el1);
    }

    public static void testELBusNot(){
        String el1 = ELBusNot.node("not").id("id").tag("tag").maxWaitSeconds(100).toEL();
        System.out.println(el1);
    }

    public static void testELBusOr(){
        String el1 = ELBusOr.node(ELBusNode.node("a"), ELBusNode.node("b")).id("id").tag("tag").maxWaitSeconds(100).toEL();
        System.out.println(el1);
    }

    public static void testELBusAnd(){
        String el1 = ELBusAnd.node(ELBusNode.node("a"), ELBusNode.node("b")).id("id").tag("tag").maxWaitSeconds(100).toEL();
        System.out.println(el1);
    }

    public static void testELBusNode(){
        String el = ELBusNode.node("aa").id("id").tag("tag").maxWaitSeconds(100)
                .data("dataName", "{'a':a}")
                .toEL();
        System.out.println(el);

        IvyCmp cmp = new IvyCmp();
        cmp.setComponentId("aa");
        cmp.setCmpId("id");
        cmp.setCmpTag("tag");
        cmp.setCmpMaxWaitSeconds(100);
        cmp.setCmpDataName("dataName");
        cmp.setCmpData("{'a':a}");
        String el2 = ELBusNode.node(cmp).toEL();
        System.out.println(el2);
    }
}
