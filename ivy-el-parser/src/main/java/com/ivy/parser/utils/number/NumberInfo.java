package com.ivy.parser.utils.number;

public class NumberInfo {

    private String str;
    private String n;
    private String e;

    private NumberInfo(){}

    private NumberInfo(String num){
        this.str = num;
        if(num.contains("e")){
            String[] arr = num.split("e");
            this.n = arr[0];
            this.e = arr[1];
        }else{
            this.n = num;
        }
    }

    public static NumberInfo NEW(String num){
        return new NumberInfo(num);
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }

    public String getN() {
        return n;
    }

    public void setN(String n) {
        this.n = n;
    }

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }
}
