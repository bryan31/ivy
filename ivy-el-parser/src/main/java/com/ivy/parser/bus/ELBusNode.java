package com.ivy.parser.bus;

import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.NodeELWrapper;

public class ELBusNode extends BaseELBus {

    public static NodeELWrapper node(String nodeId){
        return ELBus.node(nodeId);
    }

    public static NodeELWrapper node(IvyCmp cmp){
        NodeELWrapper wrapper = ELBus.node(cmp.getComponentId());
//        setId(wrapper, cmp);
//        setTag(wrapper, cmp);
//        setMaxWaitSeconds(wrapper, cmp);
        setData(wrapper, cmp);
        return wrapper;
    }

}
