package com.ivy.parser.bus;

import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.ForELWrapper;
import com.yomahub.liteflow.builder.el.NodeELWrapper;

public class ELBusFor extends BaseELBus {

    public static ForELWrapper node(NodeELWrapper nodeElWrapper) {
        return ELBus.forOpt(nodeElWrapper);
    }

    public static ForELWrapper node(String node) {
        return ELBus.forOpt(node);
    }

    public static ForELWrapper node(IvyCmp cmp){
        Object doOpt = cmp.getCmpDoOptEL() != null ? cmp.getCmpDoOptEL() : cmp.getCmpDoOpt();
        Object breakOpt = cmp.getCmpBreakOptEL() != null ? cmp.getCmpBreakOptEL() : cmp.getCmpBreakOpt();
        ForELWrapper wrapper = ELBus.forOpt(ELBusNode.node(cmp));
        wrapper.doOpt(doOpt);
        wrapper.breakOpt(breakOpt);
        wrapper.parallel(cmp.getCmpParallel());
        setId(wrapper, cmp);
        setTag(wrapper, cmp);
        setMaxWaitSeconds(wrapper, cmp);
        return wrapper;
    }

}
