package com.ivy.parser.bus;

import cn.hutool.core.util.StrUtil;
import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.ThenELWrapper;

public class ELBusThen extends BaseELBus {

    public static ThenELWrapper node(Object... objects){
        return ELBus.then(objects);
    }

    public static ThenELWrapper node(IvyCmp cmp){
        ThenELWrapper wrapper = ELBus.then(ELBusNode.node(cmp));
        if(cmp.getPreEL() != null){
            wrapper.pre(cmp.getPreEL());
        }else if(StrUtil.isNotBlank(cmp.getCmpPre())){
            wrapper.pre(cmp.getCmpPre());
        }
        if(cmp.getFinallyEL() != null) {
            wrapper.finallyOpt(cmp.getFinallyEL());
        }else if(StrUtil.isNotBlank(cmp.getCmpFinallyOpt())){
            wrapper.finallyOpt(cmp.getCmpFinallyOpt());
        }
        setId(wrapper, cmp);
        setTag(wrapper, cmp);
        setMaxWaitSeconds(wrapper, cmp);
        return wrapper;
    }

}
