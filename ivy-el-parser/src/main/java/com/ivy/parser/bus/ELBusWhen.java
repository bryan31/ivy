package com.ivy.parser.bus;

import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.WhenELWrapper;

public class ELBusWhen extends BaseELBus {

    public static WhenELWrapper node(Object... objects){
        return ELBus.when(objects);
    }

    public static WhenELWrapper node(IvyCmp cmp){
        WhenELWrapper wrapper = ELBus.when();
        wrapper.must(cmp.getWhenMust());
        wrapper.any(cmp.getWhenAny());
        wrapper.ignoreError(cmp.getWhenIgnoreError());
        //wrapper.customThreadExecutor();
        setId(wrapper, cmp);
        setTag(wrapper, cmp);
        setMaxWaitSeconds(wrapper, cmp);
        return wrapper;
    }

}
