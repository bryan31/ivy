package com.ivy.parser.bus;

import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.*;

public class ELBusWhile extends BaseELBus {

    public static WhileELWrapper node(NodeELWrapper nodeElWrapper) {
        return ELBus.whileOpt(nodeElWrapper);
    }

    public static WhileELWrapper node(String nodeElWrapper) {
        return ELBus.whileOpt(nodeElWrapper);
    }

    public static WhileELWrapper node(AndELWrapper andElWrapper) {
        return ELBus.whileOpt(andElWrapper);
    }

    public static WhileELWrapper node(OrELWrapper orElWrapper) {
        return ELBus.whileOpt(orElWrapper);
    }

    public static WhileELWrapper node(NotELWrapper notElWrapper) {
        return ELBus.whileOpt(notElWrapper);
    }

    public static WhileELWrapper node(IvyCmp cmp){
        Object doOpt = cmp.getCmpDoOptEL() != null ? cmp.getCmpDoOptEL() : cmp.getCmpDoOpt();
        Object breakOpt = cmp.getCmpBreakOptEL() != null ? cmp.getCmpBreakOptEL() : cmp.getCmpBreakOpt();
        WhileELWrapper wrapper = ELBus.whileOpt(ELBusNode.node(cmp));
        wrapper.doOpt(doOpt);
        wrapper.breakOpt(breakOpt);
        wrapper.parallel(cmp.getCmpParallel());
        setId(wrapper, cmp);
        setTag(wrapper, cmp);
        setMaxWaitSeconds(wrapper, cmp);
        return wrapper;
    }

}
