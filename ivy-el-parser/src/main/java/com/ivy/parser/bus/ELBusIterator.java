package com.ivy.parser.bus;

import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.IteratorELWrapper;
import com.yomahub.liteflow.builder.el.NodeELWrapper;

public class ELBusIterator extends BaseELBus {

    public static IteratorELWrapper node(NodeELWrapper nodeElWrapper) {
        return ELBus.iteratorOpt(nodeElWrapper);
    }

    public static IteratorELWrapper node(String nodeElWrapper) {
        return ELBus.iteratorOpt(nodeElWrapper);
    }

    public static IteratorELWrapper node(IvyCmp cmp){
        Object doOpt = cmp.getCmpDoOptEL() != null ? cmp.getCmpDoOptEL() : cmp.getCmpDoOpt();
        IteratorELWrapper wrapper = ELBus.iteratorOpt(ELBusNode.node(cmp));
        wrapper.doOpt(doOpt);
        wrapper.parallel(cmp.getCmpParallel());
        setId(wrapper, cmp);
        setTag(wrapper, cmp);
        setMaxWaitSeconds(wrapper, cmp);
        return wrapper;
    }

}
