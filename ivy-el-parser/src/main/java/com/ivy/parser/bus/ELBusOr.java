package com.ivy.parser.bus;

import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.OrELWrapper;

public class ELBusOr extends BaseELBus {

    public static OrELWrapper node(Object... objects){
        return ELBus.or(objects);
    }

    public static OrELWrapper node(IvyCmp cmp){
        OrELWrapper wrapper = ELBus.or(cmp.getOrEL());
        setId(wrapper, cmp);
        setTag(wrapper, cmp);
        setMaxWaitSeconds(wrapper, cmp);
        return wrapper;
    }

}
