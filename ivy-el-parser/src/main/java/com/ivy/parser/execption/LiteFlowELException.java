package com.ivy.parser.execption;

public class LiteFlowELException extends Exception {

    public LiteFlowELException(String message) {
        super(message);
    }

}
