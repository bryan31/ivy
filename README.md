使用文档：https://gitee.com/yunzy/ivy/wikis/Home

ivy前端项目：[https://gitee.com/yunzy/ivy-web-app](https://gitee.com/yunzy/ivy-web-app)


## 后端
```
ivy
 |-- db
   |-- ivy_init_all.sql 初始化SQL只需要执行这一个即可
   |-- sql/**           其他相关初始化SQL
 |-- ivy-web         通用模块
 |-- ivy-el-parser      EL解析器模块
 |-- ivy-modules  
   |-- liteflow         规则编排模块
   |-- job              xxl-job后台管理系统
   |-- monitor          监控模块
 |-- ivy-system         项目启动类IvyApplication
 |-- ivy-samples        任务执行器示例

```
数据库管理：
- [x] 动态数据源管理
- [x] SQLManager管理
- [ ] 动态SQL管理

动态类管理：
- [x] 普通类
- [x] 组件类

组件管理：
- [x] 动态组件
- [x] 脚本组件
- [x] 降级组件
- [x] 动态类组件

流程编排：
- [x] 配置项管理
- [x] 执行器管理
- [x] 表达式管理
- [x] 链路管理

任务调度：
- [x] 运行报表
- [x] 任务管理
- [x] 调度日志
- [x] 任务执行器

链路追踪：

支持的编排组件：

- [x] 基础组件
- [x] 选择组件
- [x] 条件组件
- [x] 次数循环组件
- [x] 条件循环组件
- [x] 迭代循环组件
- [x] 前置组件
- [x] 后置组件
- [x] 降级组件
- [x] 脚本组件
- [x] 并行分组

支持的编排方式：
- [x] 串行编排 - 完全串行编排【THEN(A,B,C,D)】
- [x] 并行编排 - 完全并行编排【WHEN(A,B,C,D)】
- [x] 嵌套编排 - 单起点单结束点编排【THEN(A,WHEN(B,C),D)】【强烈推荐】
- [x] 嵌套编排 - 单起点多结束点编排【THEN(A,WHEN(B,C))】
- [x] 嵌套编排 - 多起点路径有交集编排【THEN(WHEN(A,B),C,D)】
- [x] 嵌套编排 - 多起点路径无交集编排【WHEN(THEN(A,B),THEN(C,D))】
- [x] 选择编排
- [x] 条件编排
- [x] 次数循环编排
- [x] 条件循环编排
- [x] 迭代循环编排

其他：
- [ ] 捕获异常表达式
- [ ] 与或非表达式
- [ ] 使用子流程
- [ ] 使用子变量
- [ ] EL规则注释
- [ ] 验证规则

集成框架：
- [ ] sa-token
- [x] SMS4J
- [ ] x-file-storage
- [ ] PowerJob


## 截图
![ivy-app/src/assets/jt/ivy-3.png](/ivy-app/src/assets/jt/ivy-3.png)



![ivy-app/src/assets/jt/ivy-1.png](/ivy-app/src/assets/jt/ivy-1.png)



![ivy-app/src/assets/jt/ivy-2.png](/ivy-app/src/assets/jt/ivy-2.png)



Ivy交流Q群：908863074

