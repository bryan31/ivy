package com.ming.common;

import com.ming.common.dynamic.loader.MethodUtils;
import com.ming.common.dynamic.loader.bean_loader.DynamicBean;
import com.ming.common.dynamic.loader.class_loader.DynamicClass;
import com.ming.common.dynamic.loader.jar_class_loader.DynamicJarClass;
import com.ming.common.dynamic.loader.jar_loader.DynamicJar;
import org.junit.Test;

public class DynamicTest {

    //@Test
    public String testAspect() throws InstantiationException, IllegalAccessException {
        String javaSourceCode = "package cn.util;\n" +
                "public class TestClass {\n" +
                "    public String testMethod(String name){\n" +
                "        return String.format(\"Hello,%s!\",name);\n" +
                "    }\n" +
                "}";
        String fullClassName = "cn.util.TestClass";
        String methodName = "testMethod";
        DynamicClass dynamicClass = DynamicClass.init(javaSourceCode, fullClassName).compiler();
        Class<?> clasz = dynamicClass.load();
        Object obj = MethodUtils.proxy(clasz.newInstance());
        return (String) MethodUtils.invokeClass(obj, methodName, "world");
    }

    //@Test
    public static String testJar(){
        Class<?> clasz = DynamicJar.init("Z:\\app\\repo\\cn\\hutool\\hutool-all\\5.8.15\\hutool-all-5.8.15.jar").load("cn.hutool.core.util.IdUtil");
        return (String) MethodUtils.invokeClass(clasz, "randomUUID");
    }

    //@Test
    public static String testJarClass() throws ClassNotFoundException {
        String javaSourceCode = "package cn.util;\n" +
                "import cn.hutool.core.util.IdUtil;\n" +
                "public class TestClass {\n" +
                "    public String testMethod(){\n" +
                "        return IdUtil.randomUUID();\n" +
                "    }\n" +
                "}";
        String fullClassName = "cn.util.TestClass";
        String methodName = "testMethod";

        DynamicJarClass dynamicJarClass = DynamicJarClass.init("Z:\\app\\repo\\cn\\hutool\\hutool-all\\5.8.15\\hutool-all-5.8.15.jar")
                .javaSourceCode(javaSourceCode)
                .fullClassName(fullClassName);
        Class<?> clasz = dynamicJarClass.load();


        return (String) MethodUtils.invokeClass(clasz, methodName, dynamicJarClass.getMyClassLoader());
    }

    //@Test
    public static String testBean(){
        String javaSourceCode = "package cn.util;\n" +
                "public class TestClass {\n" +
                "    public String testMethod(String name){\n" +
                "        return String.format(\"Hello,%s!\",name);\n" +
                "    }\n" +
                "}";
        String fullClassName = "cn.util.TestClass";
        String methodName = "testMethod";
        String beanName = DynamicBean.init(DynamicClass.init(javaSourceCode,fullClassName)).load();
        return (String) MethodUtils.invokeBean(beanName,methodName,"world");
    }

    //@Test
    public static String testClass(){
        String javaSourceCode = "package cn.util;\n" +
                "import org.springframework.context.annotation.ComponentScan;\n" +
                "public class TestClass {\n" +
                "    public String testMethod(String name){\n" +
                "        return String.format(\"Hello,%s!\",name);\n" +
                "    }\n" +
                "}";
        String fullClassName = "cn.util.TestClass";
        String methodName = "testMethod";
        DynamicClass dynamicClass = DynamicClass.init(javaSourceCode, fullClassName).compiler();
        Class<?> clasz = dynamicClass.load();
        return (String) MethodUtils.invokeClass(clasz, methodName, "world");
    }
}
