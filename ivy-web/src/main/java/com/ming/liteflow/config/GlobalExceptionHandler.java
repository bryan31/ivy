package com.ming.liteflow.config;

import com.ivy.parser.execption.LiteFlowELException;
import com.ming.common.beetl.util.Result;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

@ControllerAdvice
public class GlobalExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @InitBinder
    public void initBinder(WebDataBinder binder){}

    @ModelAttribute
    public void attribute(Model model){}

    @ResponseBody
    @ExceptionHandler(Exception.class)
    public Result<?> handleException(Exception e){
        if(e instanceof LiteFlowELException){
            System.err.println("LiteFlowELException："+e.getMessage());
        }else{
            e.printStackTrace();
        }
        return Result.error(e.getMessage());
    }

}
