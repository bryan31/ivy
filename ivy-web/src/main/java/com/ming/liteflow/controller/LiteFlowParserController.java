package com.ming.liteflow.controller;

import com.ming.common.beetl.util.Result;
import com.ming.common.xxljob.annotation.PermissionLimit;
import com.ming.core.parser.entity.FlowData;
import com.ming.core.parser.graph.Graph;
import com.yomahub.liteflow.builder.el.ELWrapper;
import com.yomahub.liteflow.core.FlowExecutor;
import org.beetl.sql.core.SQLManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/liteflow/parser")
public class LiteFlowParserController {

    @Resource
    private SQLManager sqlManager;

    @Resource
    private FlowExecutor flowExecutor;

    @PostMapping("/build")
    @PermissionLimit(limit = false)
    public Result<?> buildNew(@RequestBody FlowData data) throws Exception {
        Graph graph = new Graph(data);
        ELWrapper elWrapper = graph.toEL();
        String el = elWrapper.toEL(data.getFormat());
        System.out.println(el);
        return Result.OK(el);
    }

}
