package com.ming.liteflow.cmp.node;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeIfComponent;

@LiteflowComponent("IfCmp")
public class IfCmp  extends NodeIfComponent {
    @Override
    public boolean processIf() throws Exception {
        System.out.println("IfCmp executed!");
        return true;
    }
}