package com.ming.liteflow.cmp.node;

import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeWhileComponent;

@LiteflowComponent("WhileCmp")
public class WhileCmp extends NodeWhileComponent {
    @Override
    public boolean processWhile() throws Exception {
        System.out.println("WhileCmp executed!");
        return false;
    }
}