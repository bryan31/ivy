package com.ming.liteflow.cmp.fallback;

import com.yomahub.liteflow.annotation.FallbackCmp;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeIfComponent;
@FallbackCmp
@LiteflowComponent("IfFallbackCmp")
public class IfFallbackCmp  extends NodeIfComponent {
    @Override
    public boolean processIf() throws Exception {
        System.out.println("IfFallbackCmp executed!");
        return true;
    }
}