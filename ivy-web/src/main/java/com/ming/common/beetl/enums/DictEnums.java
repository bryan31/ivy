package com.ming.common.beetl.enums;

import com.ming.common.beetl.config.MySqlStyle;
import com.ming.common.beetl.enums.core.IDictItem;
import com.ming.common.beetl.enums.core.StaticDictPool;
import org.beetl.sql.core.db.DBStyle;

public interface DictEnums {

    enum TableInfo implements IDictItem {
        ivy_db_datasource("ivy_db_datasource","id"),
        ivy_db_sql_manager("ivy_db_sql_manager","id"),
        ivy_db_sql_exec("ivy_db_sql_exec","id"),
        ;
        TableInfo(String value,String label){
            StaticDictPool.putDictItem(this,value,label);
        }

    }

    enum DbStyle implements IDictItem {
        SqlType_mysql("mysql","mysql",new MySqlStyle()),
        ;

        private org.beetl.sql.core.db.DBStyle dbStyle;
        DbStyle(String value,String label,org.beetl.sql.core.db.DBStyle dbStyle){
            this.dbStyle = dbStyle;
            StaticDictPool.putDictItem(this,value,label);
        }

        public static DBStyle getDbStyle(String dbStyle) {
            for (DbStyle item : DbStyle.values()){
                if(item.value().equalsIgnoreCase(dbStyle)){
                    return item.dbStyle;
                }
            }
            return null;
        }
    }

}
