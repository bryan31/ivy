package com.ming.common.beetl.cache;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.ming.core.dynamic.beetlsql.BaseEntity;

import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

public class CacheSqlExecUtil {

    private static final Cache<String, BaseEntity> caffeineCache = Caffeine.newBuilder().initialCapacity(10).expireAfterWrite(365, TimeUnit.DAYS).build();

    public static Cache<String, BaseEntity> getCacheList(){
        return caffeineCache;
    }

    public static <T> T getCache(String key,Class<T> t){
        caffeineCache.getIfPresent(key);
        return (T) caffeineCache.asMap().get(key);
    }

    public static BaseEntity getCache(String key){
        caffeineCache.getIfPresent(key);
        return caffeineCache.asMap().get(key);
    }

    private void putAndUpdateCache(String key,BaseEntity value){
        caffeineCache.put(key,value);
    }

    private void removeCache(String key){
        caffeineCache.asMap().remove(key);
    }

    private void removesCache(String... keys){
        for (String key : keys){
            caffeineCache.asMap().remove(key);
        }
    }

    private String key;
    private Class<?> classType;
    private Object result;

    public static CacheSqlExecUtil NEW(){
        return new CacheSqlExecUtil();
    }

    public CacheSqlExecUtil key(Object... keys){
        if(keys != null && keys.length == 1){
            this.key = String.valueOf(keys[0]);
        }else{
            this.key = getKey(keys);
        }
        return this;
    }

    public CacheSqlExecUtil classType(Class<?> classType){
        this.classType = classType;
        return this;
    }

    public CacheSqlExecUtil put(ResultConsumer<CacheSqlExecUtil> action){
        BaseEntity o = (BaseEntity) getCache(key, classType);
        if(o == null){
            this.result = action.accept(this);
            putAndUpdateCache(key, (BaseEntity) this.result);
        }else{
            this.result = o;
        }
        return this;
    }

    public <T> T get(){
        return (T) result;
    }

    public <T> T get(T defaultVal){
        if(result == null){
            return defaultVal;
        }
        return (T) result;
    }

    public static String getKey(Object... args){
        StringBuilder sb = new StringBuilder();
        for (Object arg : args){
            sb.append(arg.toString()).append("_");
        }
        return sb.append("KEY").toString();
    }

    public int clear() {
        ConcurrentMap<String, BaseEntity> map = caffeineCache.asMap();
        int size1 = map.size();
        map.clear();
        int size2 = map.size();
        return size1 - size2;
    }

/*    public static void main(String[] args) {
        //List<Map<String,Object>> resultList = CacheUtil.NEW().caffeine(cache).key("tzxx",vo).put(m -> homeGxcService.tzxx(vo)).get();
    }*/
}
