package com.ming.common.beetl.entity;

import com.ming.core.anno.Describe;
import com.ming.core.anno.DescribeItem;
import com.ming.core.generate.template.annotation.Generate;
import com.ming.core.generate.template.annotation.database.Column;
import com.ming.core.generate.template.annotation.database.PrimaryKey;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

@Data
@Table(name = "ivy_db_datasource")
@Generate(isEffective = true, moduleName = "db", desc = "数据源")
public class IvyDbDatasource {

    @Column
    @PrimaryKey
    private Long id;

    @Column(len = 255)
    @Describe(value = "Bean名称，如：ds0")
    private String dsBeanName;

    @Column(len = 255)
    @Describe(value = "数据源中文名称")
    private String dsName;

    @Column(len = 255)
    @Describe(value = "描述信息")
    private String dsDesc;

    @Column(len = 255)
    @Describe(value = "数据库驱动路径")
    private String driverClassName;

    @Column(len = 255)
    @Describe(value = "数据库连接路径")
    private String jdbcUrl;

    @Column(len = 32)
    @Describe(value = "用户名")
    private String username;

    @Column(len = 255)
    @Describe(value = "密码")
    private String password;

    @Column(len = 1)
    @Describe(value = "是否禁用",items = {
        @DescribeItem(value = "0",desc = "启用"),
        @DescribeItem(value = "1",desc = "禁用"),
    })
    private Boolean disabled;

    @Column
    @Describe(value = "连接池的最大连接数")
    private Integer maximumPoolSize;

    @Column
    @Describe(value = "连接池的最小空闲连接数")
    private Integer minimumIdle;

    @Column
    @Describe(value = "连接在池中保持空闲的最大时间，超过这个时间会被释放")
    private Long idleTimeout;

    @Column
    @Describe(value = "获取连接的超时时间")
    private Long connectionTimeout;

    @Column
    @Describe(value = "在连接池中进行连接有效性验证的超时时间")
    private Long validationTimeout;

    @Column
    @Describe(value = "连接在池中允许存活的最长时间")
    private Long maxLifetime;

    @Column
    @Describe(value = "用于测试连接是否有效的SQL查询语句")
    private String connectionTestQuery;

    @Column
    @Describe(value = "在连接被添加到连接池之前执行的SQL语句")
    private String connectionInitSql;

    @Column
    @Describe(value = "连接池的名称")
    private String poolName;

    @Column
    @Describe(value = "是否允许连接池暂停")
    private Boolean allowPoolSuspension;

    @Column
    @Describe(value = "连接是否只读")
    private Boolean readOnly;

    @Column
    @Describe(value = "是否自动提交")
    private Boolean autoCommit;

    @Column
    @Describe(value = "是否注册MBeans")
    private Boolean registerMbeans;

}
