package com.ming.common.beetl.vo;

import com.ming.core.dynamic.beetlsql.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.beetl.sql.core.page.PageResult;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class IvyPageResult {


    private PageResult<? extends BaseEntity> pageResult;

    private List<IvyPageHeader> columnList;

}
