package com.ming.common.beetl.service;


import com.ming.core.dynamic.beetlsql.BaseEntity;

import java.util.List;
import java.util.Map;

public interface MdaoSQLExecService {

    public List<? extends BaseEntity> selectAll();

    public int insert(Map<String,Object> data);

    public int updateById(Map<String,Object> data);

    public int deleteById(Map<String,Object> data);

}
