package com.ming.common.beetl.service.impl;

import com.ming.common.beetl.enums.DictEnums;
import com.ming.common.beetl.service.MdaoSQLManagerService;
import com.ming.common.beetl.util.QueryUtil;
import com.ming.core.dynamic.beetlsql.BaseEntity;
import org.beetl.sql.core.SQLManager;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

@Service
public class MdaoSQLManagerServiceImpl implements MdaoSQLManagerService {

    private static final String tableName = DictEnums.TableInfo.ivy_db_sql_manager.value();

    private String key = "id";

    @Resource
    private SQLManager sqlManager;

    public List<? extends BaseEntity> selectAll(){
        return QueryUtil.selectAll(sqlManager,tableName);
    }

    public int insert(Map<String,Object> data) {
        return QueryUtil.insertSelective(sqlManager,tableName,data);
    }

    public int updateById(Map<String,Object> data) {
        return QueryUtil.updateSelectiveById(sqlManager,tableName,data,key);
    }

    public int deleteById(Map<String,Object> data) {
        return QueryUtil.deleteById(sqlManager,tableName,data,key);
    }

}
