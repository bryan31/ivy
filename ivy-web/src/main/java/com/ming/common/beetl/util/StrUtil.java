package com.ming.common.beetl.util;

public class StrUtil {

    /**
     * 判断是否为空字符串最优代码
     * @param str
     * @return 如果为空，则返回true
     */
    public static boolean isEmpty(String str){
        return str == null || "".equals(str) || str.trim().length() == 0;
    }

    /**
     * 判断字符串是否非空
     * @param str 如果不为空，则返回true
     * @return
     */
    public static boolean isNotEmpty(String str){
        return !isEmpty(str);
    }

    public static final String regex = "([a-z])([A-Z]+)";
    public static final String replacement = "$1_$2";
    // 驼峰转下划线
    public static String camelToSnake(String input) {
        return input.replaceAll(regex, replacement).toLowerCase();
    }

    // 下划线转驼峰
    public static String snakeToCamel(String input) {
        StringBuilder result = new StringBuilder();
        String[] words = input.split("_");
        for (int i = 0; i < words.length; i++) {
            String word = words[i];
            if (i == 0) {
                result.append(word);
            } else {
                result.append(Character.toUpperCase(word.charAt(0))).append(word.substring(1));
            }
        }
        return result.toString();
    }

    public static String[] split(String strs) {
        if(isNotEmpty(strs)){
            return strs.split(",");
        }
        return null;
    }

//    public static void main(String[] args) {
//        String camelCase = "thisIsCamelCase";
//        String snakeCase = "this_is_snake_case";
//
//        String camelToSnakeResult = camelToSnake(camelCase);
//        String snakeToCamelResult = snakeToCamel(snakeCase);
//
//        System.out.println("Camel to Snake: " + camelCase+" -> "+camelToSnakeResult); // 输出: this_is_camel_case
//        System.out.println("Snake to Camel: " + snakeCase+" -> "+snakeToCamelResult); // 输出: thisIsSnakeCase
//    }
}
