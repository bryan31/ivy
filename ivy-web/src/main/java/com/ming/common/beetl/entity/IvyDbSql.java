package com.ming.common.beetl.entity;

import com.ming.core.anno.Describe;
import com.ming.core.anno.DescribeItem;
import com.ming.core.generate.template.annotation.Generate;
import com.ming.core.generate.template.annotation.database.Column;
import com.ming.core.generate.template.annotation.database.PrimaryKey;
import com.ming.core.generate.template.annotation.database.Text;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

@Data
@Table(name = "ivy_db_sql")
@Generate(isEffective = false, moduleName = "db", desc = "SQL管理")
public class IvyDbSql {

    @Column
    @PrimaryKey
    private Long id;

    @Column
    @Describe(value = "ivy_db_sql_manager表ID")
    private Long sqlManagerId;

    @Column(len = 20)
    @Describe(value = "SQL语言",items = {
            @DescribeItem(value = "DDL",desc = "数据定义语言(DDL)"),
            @DescribeItem(value = "DML",desc = "数据操作语言(DML)"),
            @DescribeItem(value = "DCL",desc = "数据控制语言(DCL)"),
            @DescribeItem(value = "TCL",desc = "事务控制语言(TCL)"),
    })
    private String sqlLanguage;

    @Column(len = 32)
    @Describe(value = "表名")
    private String tableName;

    @Column(len = 20)
    @Describe(value = "操作类型",items = {
            @DescribeItem(value = "single",desc = "单表操作"),
//            @DescribeItem(value = "multiple",desc = "多表操作"),
    })
    private String tableType;

    @Column(len = 32)
    @Describe(value = "sqlId,如：selectUserById")
    private String sqlId;

    @Column(len = 32)
    @Describe(value = "sql名称")
    private String sqlName;

    @Column(len = 255)
    @Describe(value = "描述信息")
    private String sqlDesc;

    @Column(len = 12)
    @Describe(value = "SQL类型",items = {
        @DescribeItem(value = "select",desc = "select"),
        @DescribeItem(value = "insert",desc = "insert"),
        @DescribeItem(value = "update",desc = "update"),
        @DescribeItem(value = "delete",desc = "delete"),
    })
    private String sqlType;

    @Text
    @Column
    @Describe(value = "sql模板")
    private String sqlTemplate;

    @Text
    @Column
    @Describe(value = "sql配置")
    private String sqlJson;

    @Column(len = 1)
    @Describe(value = "是否禁用",items = {
        @DescribeItem(value = "0",desc = "启用"),
        @DescribeItem(value = "1",desc = "禁用"),
    })
    private Boolean disabled;
}
