package com.ming.common.ivy.controller;

import com.ming.common.beetl.util.Result;
import com.ming.common.ivy.IvyDict;
import com.ming.common.liteflow.core.execption.LiteFlowELException;
import com.ming.common.xxljob.annotation.PermissionLimit;
import org.beetl.sql.core.SQLManager;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/ivy/dict")
public class IvyDictController {

    @Resource
    private SQLManager sqlManager;

    @PostMapping("/selectByCode")
    @PermissionLimit(limit = false)
    public Result<?> selectByCode(@RequestBody IvyDict ivyDict) throws LiteFlowELException {
        return Result.OK(sqlManager.lambdaQuery(IvyDict.class).andEq(IvyDict::getCode, ivyDict.getCode()).single());
    }

    @PostMapping("/saveByCode")
    @PermissionLimit(limit = false)
    public Result<?> saveByCode(@RequestBody IvyDict ivyDict) throws LiteFlowELException {
        IvyDict dict = sqlManager.lambdaQuery(IvyDict.class).andEq(IvyDict::getCode, ivyDict.getCode()).single();
        if(dict != null){
            int i = sqlManager.lambdaQuery(IvyDict.class).andEq(IvyDict::getCode, ivyDict.getCode()).updateSelective(ivyDict);
            return Result.update(i);
        }else{
            int i = sqlManager.lambdaQuery(IvyDict.class).andEq(IvyDict::getCode, ivyDict.getCode()).insertSelective(ivyDict);
            return Result.insert(i);
        }
    }

    @PostMapping("/insertByCode")
    @PermissionLimit(limit = false)
    public Result<?> insertByCode(@RequestBody IvyDict ivyDict) throws LiteFlowELException {
        int i = sqlManager.lambdaQuery(IvyDict.class).andEq(IvyDict::getCode, ivyDict.getCode()).insertSelective(ivyDict);
        return Result.insert(i);
    }

    @PostMapping("/updateByCode")
    @PermissionLimit(limit = false)
    public Result<?> updateByCode(@RequestBody IvyDict ivyDict) throws LiteFlowELException {
        int i = sqlManager.lambdaQuery(IvyDict.class).andEq(IvyDict::getCode, ivyDict.getCode()).updateSelective(ivyDict);
        return Result.update(i);
    }
}
