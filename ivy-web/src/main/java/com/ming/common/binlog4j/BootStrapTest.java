//package com.ming.common.binlog4j;
//
//import com.gitee.Jmysy.binlog4j.core.*;
//
//public class BootStrapTest {
//
//    public static void main(String[] args) {
//        connect();
//    }
//
//    public static void connect(){
//        BinlogClientConfig clientConfig = new BinlogClientConfig();
//        clientConfig.setHost("127.0.0.1");
//        clientConfig.setPort(3306);
//        clientConfig.setUsername("root");
//        clientConfig.setPassword("123456");
//        clientConfig.setServerId(1990);
//
////        RedisConfig redisConfig = new RedisConfig();
////        redisConfig.setHost("127.0.0.1");
////        redisConfig.setPort(6379);
////        clientConfig.setMode(BinlogClientMode.cluster); // 默认为 standalone（单机）,cluster为集群模式,需要配置redis
////        clientConfig.setRedisConfig(redisConfig); // Redis 配置
////        clientConfig.setPersistence(true); // 开启续读,防止宕机期间的数据丢失
//
//        IBinlogClient binlogClient = new BinlogClient(clientConfig);
//
//        binlogClient.registerEventHandler(new IBinlogEventHandler() {
//
//            @Override
//            public void onInsert(BinlogEvent event) {
//                System.out.println("插入数据:{}"+ event.getData());
//            }
//
//            @Override
//            public void onUpdate(BinlogEvent event) {
//                System.out.println("修改数据:{}"+ event.getData());
//            }
//
//            @Override
//            public void onDelete(BinlogEvent event) {
//                System.out.println("删除数据:{}"+ event.getData());
//            }
//
//            @Override
//            public boolean isHandle(String database, String table) {
//                return database.equals("mframework") && table.equals("test");
//            }
//        });
//
//        binlogClient.connect();
//    }
//
//}
