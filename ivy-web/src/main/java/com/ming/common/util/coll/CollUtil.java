package com.ming.common.util.coll;

import com.ming.common.util.number.BigNumberUtil;

import java.util.List;

public class CollUtil {

    private volatile static CollUtil instance;

    private CollUtil(){}

    public static CollUtil NEW(){
        if(instance == null){
            synchronized (CollUtil.class){
                if(instance == null){
                    instance = new CollUtil();
                }
            }
        }
        return instance;
    }

    public <T> boolean isEmpty(List<T> list) {
        return list == null || cn.hutool.core.collection.CollUtil.isEmpty(list);
    }

    public <T> boolean isNotEmpty(List<T> list) {
        return !isEmpty(list);
    }
}
