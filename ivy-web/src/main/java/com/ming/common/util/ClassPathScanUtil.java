package com.ming.common.util;

import cn.hutool.core.lang.ClassScanner;
import cn.hutool.core.util.ClassUtil;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ClassPathScanUtil {

    public static List<String> scan(Class<? extends Annotation> annotationType,String basePackage){
        ClassPathScanningCandidateComponentProvider scanner =
                new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AnnotationTypeFilter(annotationType));
        List<String> list = new ArrayList<>();
        for (BeanDefinition beanDefinition : scanner.findCandidateComponents(basePackage)) {
            String className = beanDefinition.getBeanClassName();
            // 处理带有 @LiteflowComponent 注解的类
            list.add(className);
        }
        return list;
    }

    public static Set<Class<?>> scanByAnno(Class<? extends Annotation> annotationType,String basePackage){
        // 使用 ClassScanner 获取带有 @LiteflowComponent 注解的类
        Set<Class<?>> classes = ClassScanner.scanPackageByAnnotation(basePackage,annotationType);
        return classes;
    }

    public static Set<Class<?>> scanBySuper(Class<?> superClass,String basePackage){
        // 使用 ClassScanner 获取带有 @LiteflowComponent 注解的类
        Set<Class<?>> classes = ClassScanner.scanPackageBySuper(basePackage,superClass);
        return classes;
    }
}
