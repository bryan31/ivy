package com.ming.common.util;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TemplateUtils {

    private String template;
    private String regex;
    private Map<String,String> paramsMap;

    public static TemplateUtils NEW(){
        return new TemplateUtils();
    }

    public TemplateUtils template(String template){
        this.template = template;
        return this;
    }

    public TemplateUtils regex(String regex){
        this.regex = regex;
        return this;
    }

    public TemplateUtils params(Map<String,String> paramsMap){
        this.paramsMap = paramsMap;
        return this;
    }

    public String compile(){
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(template);
        while (matcher.find()) {
            String key = matcher.group();
            template = template.replace(key, paramsMap.get(matcher.group(1)));
        }
        return template;
    }

//    public static void main(String[] args) {
//        String template = "import com.yomahub.liteflow.script.ScriptExecuteWrap;\n" +
//                "import com.yomahub.liteflow.script.body.{{interfaceImpl}};\n" +
//                "\n" +
//                "public class ScriptNode{{index}} implements {{interfaceImpl}} {\n" +
//                "    @Override\n" +
//                "    public {{returnType}} body(ScriptExecuteWrap scriptExecuteWrap) {\n" +
//                "        return {{returnVal}};\n" +
//                "    }\n" +
//                "}";
//        String regex = "\\{\\{(.*?)\\}\\}";
//        String compile = TemplateUtils.NEW().template(template).regex(regex).params(null).compile();
//    }

}
