package com.ming.common.util;

import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class ResourceUtil {

    public static String getResourceFtl(ResourceLoader resourceLoader,String file){
        return getResourceFtl(resourceLoader,"classpath:template/", file);
    }

    public static String getResourceFtl(ResourceLoader resourceLoader,String root,String file){
        List<String> list = new ArrayList<>();
        Resource resource = resourceLoader.getResource(root+file);
        try (InputStream inputStream = resource.getInputStream();
             BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            String line;
            while ((line = reader.readLine()) != null) {
                list.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return String.join(System.lineSeparator(),list);
    }
}
