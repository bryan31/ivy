package com.ming.common.sms4j;

import org.dromara.email.api.MailClient;
import org.dromara.email.api.Monitor;
import org.dromara.email.comm.config.MailImapConfig;
import org.dromara.email.comm.config.MailSmtpConfig;
import org.dromara.email.comm.entity.MonitorMessage;
import org.dromara.email.core.factory.MailFactory;
import org.dromara.email.core.factory.MonitorFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Configuration
public class EmailConfig {

    @Value("${sms.email.enable:false}")
    private Boolean enable;

    //端口号
    @Value("${sms.email.port:}")
    private String port;

    //发件人地址
    @Value("${sms.email.fromAddress:}")
    private String fromAddress;

    //服务器地址
    @Value("${sms.email.smtpServer:}")
    private String smtpServer;

    //账号
    @Value("${sms.email.username:}")
    private String username;

    //密码   注意的是如果你用的是QQ或者网易之类的邮箱需要的不是登录密码，而是授权码
    @Value("${sms.email.password:}")
    private String password;

    //是否开启ssl 默认开启 QQ之类的邮箱默认都需要ssl
    @Value("${sms.email.isSSL:true}")
    private String isSSL = "true";

    //是否开启验证 默认开启
    @Value("${sms.email.isAuth:true}")
    private String isAuth = "true";

    //重试间隔（单位：秒），默认为5秒
    @Value("${sms.email.retryInterval:5}")
    private int retryInterval = 5;

    //重试次数，默认为1次
    @Value("${sms.email.maxRetries:1}")
    private int maxRetries = 1;

    //是否监听邮件
    @Value("${sms.email.monitor.enable:false}")
    private Boolean monitorEnable;

    //imap服务地址
    @Value("${sms.email.monitor.imapServer:}")
    private String imapServer;

    //要监听的邮箱授权码或密码
    @Value("${sms.email.monitor.accessToken:}")
    private String accessToken;

    //监听周期（秒）默认5秒
    @Value("${sms.email.monitor.cycle:5}")
    private Integer cycle;

    @Bean("qqMailClient")
    public MailClient qqMailClient(){
        if (!enable){
            return null;
        }
        MailSmtpConfig config = MailSmtpConfig.builder()
                .smtpServer(smtpServer)
                .port(port)
                .username(username)
                .password(password)
                .fromAddress(fromAddress)
                .isAuth(isAuth)
                .isSSL(isSSL)
                .maxRetries(maxRetries)
                .retryInterval(retryInterval)
                .build();
        MailFactory.put("qq",config);

        if(monitorEnable){
            MailImapConfig mailImapConfig = new MailImapConfig();
            mailImapConfig.setImapServer(imapServer);
            mailImapConfig.setUsername(username);
            mailImapConfig.setAccessToken(accessToken);
            mailImapConfig.setCycle(cycle);
            MonitorFactory.put("qq", mailImapConfig, new Monitor() {
                @Override
                public boolean monitor(MonitorMessage monitorMessage) {
                    return false;
                }
            });
        }

        return MailFactory.createMailClient("qq", () -> {
            List<String> blackList = new ArrayList<>();
            blackList.add("111111111@qq.com");
            return blackList;
        });
    }

}
