package com.ming.common.context;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.ming.common.liteflow.context.IvyLoadCmp;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;
import java.util.concurrent.TimeUnit;

@Configuration
public class IvyCache {

    // 动态类缓存
    @Bean(name = "dynamicClassCache")
    public LoadingCache<String, Map<String, Class<?>>> dynamicClassCache() {
        // 初始化缓存，设置了10分钟的写过期，100的缓存最大个数
        return Caffeine.newBuilder()
            .expireAfterWrite(10, TimeUnit.MINUTES)
            .maximumSize(10000)
            .build(key -> IvyLoadCmp.loadClassAll());
    }

}
