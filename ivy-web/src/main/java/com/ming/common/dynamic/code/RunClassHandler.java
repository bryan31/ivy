/*
 * Copyright (c) zhg2yqq Corp.
 * All Rights Reserved.
 */
package com.ming.common.dynamic.code;

import com.ming.common.dynamic.code.config.BaseProperties;
import com.ming.common.dynamic.code.dto.*;
import com.ming.common.dynamic.code.exception.BaseDynamicException;
import com.ming.common.dynamic.code.exception.ClassLoadException;
import com.ming.common.dynamic.code.exception.CompileException;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 执行Java代码，如果系统只需编译一次代码则推荐使用该执行器
 * 
 * @version zhg2yqq v1.0
 * @author 周海刚, 2022年7月8日
 */
public class RunClassHandler extends AbstractRunHandler<ExecuteResult, ClassBean> {
    /**
     * 缓存类
     */
    private Map<String, ClassBean> cacheClasses = new ConcurrentHashMap<>();

    public RunClassHandler(IStringCompiler compiler, IClassExecuter<ExecuteResult> executer, BaseProperties properties) {
        this(compiler, executer, properties, null);
    }

    /**
     * 类处理程序
     * 
     * @param compiler 编译器
     * @param executer 执行器
     * @param properties 配置
     * @param hackers
     *            安全替换（key:待替换的类名,例如:java/lang/System，value:替换成的类名,例如:com/zhg2yqq/wheels/dynamic/code/hack/HackSystem）
     */
    public RunClassHandler(IStringCompiler compiler, IClassExecuter<ExecuteResult> executer, BaseProperties properties,
            Map<String, String> hackers) {
        super(compiler, executer, properties, hackers);
    }

    /**
     * 必须提前预加载类
     * 
     * @param sourceStrs 源码
     * @throws ClassLoadException .
     * @throws CompileException .
     */
    public void loadClassFromSources(List<String> sourceStrs) throws CompileException, ClassLoadException {
        for (String sourceStr : sourceStrs) {
            this.loadClassFromSource(sourceStr);
        }
    }

    /**
     * 执行Java方法
     * 
     * @param fullClassName 类全名，例如java.util.Date
     * @param methodName 方法名，例如getTime
     * @param parameters 方法参数
     * @param singleton 是否单例执行
     * @return 方法执行结果
     * @throws BaseDynamicException .
     */
    @Override
    public ExecuteResult runMethod(String fullClassName, String methodName, Parameters parameters,
                                   boolean singleton)
        throws BaseDynamicException {
        ClassBean classBean = this.getClassCache().get(fullClassName);
        if (classBean == null) {
            throw new BaseDynamicException(fullClassName + " 尚未编译源码");
        }
        ExecuteParameter<ClassBean> parameter = new ExecuteParameter<>(classBean, methodName,
                parameters);
        ExecuteCondition condition = new ExecuteCondition(singleton,
                getProperties().isCalExecuteTime(), getProperties().getExecuteTimeOut());
        return getExecuter().runMethod(parameter, condition);
    }

    @Override
    protected Map<String, ClassBean> getClassCache() {
        return cacheClasses;
    }

    @Override
    protected ClassBean buildClassBean(Class<?> clazz) {
        return new ClassBean(clazz);
    }
}