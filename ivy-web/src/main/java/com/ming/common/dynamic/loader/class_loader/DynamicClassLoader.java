package com.ming.common.dynamic.loader.class_loader;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLClassLoader;
import java.security.SecureClassLoader;
import java.util.HashMap;
import java.util.Map;

/**
 * 动态编译加载器
 */
public class DynamicClassLoader extends SecureClassLoader {

    /**
     * 编译的时候返回的class字节数组-支持内部类
     */
    private byte[] classData;
    private final Map<String, byte[]> classBytes = new HashMap<>();

    public DynamicClassLoader(byte[] classData) {
        super();
        this.classData = classData;
    }

    public DynamicClassLoader(ClassLoader parent) {
        super(parent);
    }

    /**
     * 添加类信息
     *
     * @param fullClassName 类的完全限定名
     * @param classData     类的数据
     */
    public void addClass(String fullClassName, byte[] classData) {
        classBytes.put(fullClassName, classData);
    }




/*    @Override
    protected Class<?> findClass(String fullClassName) throws ClassNotFoundException {
        // 1.编译的class字节数组为null，则说明已经编译过了，是后续的调用，不用编译
        if (classData == null || classData.length == 0) {
            throw new ClassNotFoundException("[动态编译]classdata不存在");
        }
        try {
            //System.out.println(fullClassName);
            //Class.forName(fullClassName, false, this.getClass().getClassLoader());
            // 2.加载
            return defineClass(fullClassName, classData, 0, classData.length);
        } catch (Exception e) {
            System.out.println("loader error: "+fullClassName);
        }
        return null;
    }*/

    /**
     * 重写父类方法，用于查找指定的类。
     *
     * @param fullClassName 指定的类名
     * @return 查找到的类对象
     * @throws ClassNotFoundException 如果找不到指定的类，则抛出该异常
     */
    @Override
    protected Class<?> findClass(String fullClassName) throws ClassNotFoundException {
        // 获取指定类的字节码数据
        byte[] classData = classBytes.get(fullClassName);
        if (classData == null) {
            // 如果找不到指定的类，则抛出ClassNotFoundException异常
            throw new ClassNotFoundException("[动态编译]找不到类: " + fullClassName);
        }
        // 定义并返回指定的类对象
        return defineClass(fullClassName, classData, 0, classData.length);
    }

    public boolean loadClassFromURLClassLoader(URLClassLoader urlClassLoader, String className) {
        try {
            Class<?> loadedClass = urlClassLoader.loadClass(className);
            InputStream classStream = loadedClass.getResourceAsStream("/" + className.replace('.', '/') + ".class");
            byte[] classBytes = readStreamToBytes(classStream);
            defineClass(className, classBytes, 0, classBytes.length);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    private byte[] readStreamToBytes(InputStream in) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = in.read(buffer)) != -1) {
            out.write(buffer, 0, bytesRead);
        }
        return out.toByteArray();
    }
}

