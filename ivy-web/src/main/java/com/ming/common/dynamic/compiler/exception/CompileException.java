/*
 * Copyright (c) zhg2yqq Corp.
 * All Rights Reserved.
 */
package com.ming.common.dynamic.compiler.exception;

/**
 * 编译异常
 * @version zhg2yqq v1.0
 * @author 周海刚, 2022年7月27日
 */
public abstract class CompileException extends Exception {

    private static final long serialVersionUID = 1L;

}
