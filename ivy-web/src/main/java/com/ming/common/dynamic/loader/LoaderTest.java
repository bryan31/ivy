package com.ming.common.dynamic.loader;

import com.ming.common.dynamic.loader.bean_loader.DynamicBean;
import com.ming.common.dynamic.loader.class_loader.DynamicClass;
import com.ming.common.dynamic.loader.jar_loader.DynamicJar;

public class LoaderTest {

    public static void main(String[] args) {
        testBean();
    }

    public static String testBean(){
        String javaSourceCode = "package cn.wubo.loader.util;\n" +
                "\n" +
                "public class TestClass {\n" +
                "    \n" +
                "    public String testMethod(String name){\n" +
                "        return String.format(\"Hello,%s!\",name);\n" +
                "    }\n" +
                "}";
        String fullClassName = "cn.wubo.loader.util.TestClass";
        String methodName = "testMethod";
        String beanName = DynamicBean.init(DynamicClass.init(javaSourceCode,fullClassName)).load();
        return (String) MethodUtils.invokeBean(beanName,methodName,"world");
    }

    public String testClass(){
        String javaSourceCode = "package cn.wubo.loader.util;\n" +
                "\n" +
                "public class TestClass {\n" +
                "    \n" +
                "    public String testMethod(String name){\n" +
                "        return String.format(\"Hello,%s!\",name);\n" +
                "    }\n" +
                "}";
        String fullClassName = "cn.wubo.loader.util.TestClass";
        String methodName = "testMethod";
        DynamicClass dynamicClass = DynamicClass.init(javaSourceCode, fullClassName).compiler();
        Class<?> clasz = dynamicClass.load();
        return (String) MethodUtils.invokeClass(clasz, methodName, "world");
    }

    public String testJar(){
        Class<?> clasz = DynamicJar.init("D:\\maven-repository\\repository\\cn\\hutool\\hutool-all\\5.3.2\\hutool-all-5.3.2.jar").load("cn.hutool.core.util.IdUtil");
        return (String) MethodUtils.invokeClass(clasz, "randomUUID");
    }

//    public String loadAndInvokeGroovy() {
//        String javaSourceCode = "package cn.wubo.loader.util;\n" +
//                "\n" +
//                "public class TestClass {\n" +
//                "    \n" +
//                "    public String testMethod(String name){\n" +
//                "        return String.format(\"Hello,%s!\",name);\n" +
//                "    }\n" +
//                "}";
//        String methodName = "testMethod";
//        Class<?> clasz = DynamicGroovy.init(javaSourceCode).load();
//        return (String) MethodUtils.invokeClass(clasz, methodName, "world");
//    }

//    public String loadController() {
//        String fullClassName = "cn.wubo.loaderutiltest.DemoController";
//        String javaSourceCode = "package cn.wubo.loaderutiltest;\n" +
//                "\n" +
//                "            import lombok.AllArgsConstructor;\n" +
//                "            import lombok.Data;\n" +
//                "            import lombok.NoArgsConstructor;\n" +
//                "            import org.springframework.web.bind.annotation.GetMapping;\n" +
//                "            import org.springframework.web.bind.annotation.RequestMapping;\n" +
//                "            import org.springframework.web.bind.annotation.RequestParam;\n" +
//                "            import org.springframework.web.bind.annotation.RestController;\n" +
//                "\n" +
//                "            @RestController\n" +
//                "            @RequestMapping(value = \"test\")\n" +
//                "            public class DemoController {\n" +
//                "\n" +
//                "                @GetMapping(value = \"hello\")\n" +
//                "                public User hello(@RequestParam(value = \"name\") String name) {\n" +
//                "                    return new User(name);\n" +
//                "                }\n" +
//                "\n" +
//                "                @Data\n" +
//                "                @AllArgsConstructor\n" +
//                "                @NoArgsConstructor\n" +
//                "                public static class User {\n" +
//                "                    private String name;\n" +
//                "                }\n" +
//                "            }";
//        return DynamicController.init(DynamicClass.init(javaSourceCode, fullClassName)).load();
//    }

    public String testAspect() throws InstantiationException, IllegalAccessException {
        String javaSourceCode = "package cn.wubo.loader.util;\n" +
                "\n" +
                "public class TestClass {\n" +
                "    \n" +
                "    public String testMethod(String name){\n" +
                "        return String.format(\"Hello,%s!\",name);\n" +
                "    }\n" +
                "}";
        String fullClassName = "cn.wubo.loader.util.TestClass";
        String methodName = "testMethod";
        DynamicClass dynamicClass = DynamicClass.init(javaSourceCode, fullClassName).compiler();
        Class<?> clasz = dynamicClass.load();
        Object obj = MethodUtils.proxy(clasz.newInstance());
        return (String) MethodUtils.invokeClass(obj, methodName, "world");
    }
}
