package com.ming.common.liteflow.vo;

import com.ming.core.query.Options;
import com.ming.common.liteflow.core.task.IvyTask;
import lombok.Data;

@Data
public class IvyTaskVo extends IvyTask {

    private Options options;

}
