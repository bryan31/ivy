package com.ming.common.liteflow.vo;

import com.ming.core.query.Options;
import com.ming.core.liteflow.entity.IvyConfig;
import lombok.Data;

@Data
public class IvyConfigVo extends IvyConfig {

    private Options options;

}
