package com.ming.common.liteflow.core.el.prop;

import lombok.Data;

@Data
public class ForProperties extends Properties {

    private Integer loopNumber;
    private Boolean parallel;
    private String doOpt;
    private String breakOpt;

}
