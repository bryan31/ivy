package com.ming.common.liteflow.core.el.bus;

import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.ELWrapper;
import com.yomahub.liteflow.builder.el.ThenELWrapper;

public class ELBusBreak {

    private ELWrapper wrapper;

    private boolean isNodeEL = false;

    public static ELBusBreak NEW(){
        return new ELBusBreak();
    }

    public ELBusBreak nodeEL(){
        this.isNodeEL = true;
        return this;
    }

    public ELBusBreak thenEL(){
        this.isNodeEL = false;
        return this;
    }

    public ELBusBreak node(IvyCmp info){
        if(isNodeEL){
            ELWrapper elWrapper = ELBusNode.NEW().node(info).toELWrapper();
            wrapper = elWrapper;
        }else{
            ThenELWrapper thenELWrapper = ELBus.then(ELBusNode.NEW().node(info).toELWrapper());
            if(com.ming.common.beetl.util.StrUtil.isNotEmpty(info.getCmpPre())){
                thenELWrapper.pre(info.getCmpPre());
            }
            if(com.ming.common.beetl.util.StrUtil.isNotEmpty(info.getCmpFinallyOpt())){
                thenELWrapper.finallyOpt(info.getCmpFinallyOpt());
            }
            if(com.ming.common.beetl.util.StrUtil.isNotEmpty(info.getCmpId())){
                thenELWrapper.id(info.getCmpId());
            }
            if(com.ming.common.beetl.util.StrUtil.isNotEmpty(info.getCmpTag())){
                thenELWrapper.tag(info.getCmpTag());
            }
            if(info.getCmpMaxWaitSeconds() != null){
                thenELWrapper.maxWaitSeconds(info.getCmpMaxWaitSeconds());
            }
            wrapper = thenELWrapper;
        }
        return this;
    }

    public String toEL(){
        return wrapper.toEL();
    }

    public String toEL(boolean format){
        return wrapper.toEL(format);
    }

    public ELWrapper toELWrapper(){
        return wrapper;
    }

}
