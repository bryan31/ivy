package com.ming.common.liteflow.core.el;

import com.ivy.builder.graph.IvyCmp;
import com.ming.common.beetl.util.StrUtil;
import com.ming.common.liteflow.core.el.bus.*;
import com.ming.common.liteflow.core.execption.LiteFlowELException;
import com.ming.common.liteflow.core.node.NodeInfoWrapper;
import com.yomahub.liteflow.builder.el.ELWrapper;

public class NodeInfoToELUtil {
    public static ELWrapper buildELWrapper(NodeInfoWrapper nodeInfoWrapper) throws LiteFlowELException {
        return buildELWrapper(nodeInfoWrapper, false);
    }

    public static ELWrapper buildELWrapper(NodeInfoWrapper nodeInfoWrapper, boolean isSimple) throws LiteFlowELException {
        String componentId = nodeInfoWrapper.getComponentId();
        if(cn.hutool.core.util.StrUtil.isBlank(componentId)){
            throw new LiteFlowELException("节点组件ID为空");
        }
        return toELWrapper(nodeInfoWrapper,isSimple);
    }

    public static ELWrapper toELWrapper(IvyCmp info){
        return toELWrapper(info,false);
    }

    public static ELWrapper toELWrapper(IvyCmp info, boolean isSimple){
        handler(info);
        ELWrapper el = null;
        switch (info.getType()){
            case "common":
                if(isSimple){
                    el = ELBusNode.NEW().node(info).toELWrapper();
                }else{
                    el = ELBusThen.NEW().node(info).toELWrapper();
                }
                break;
            case "switch": el = ELBusSwitch.NEW().node(info).toELWrapper();break;
            case "if": el = ELBusIf.NEW().node(info).toELWrapper();break;
            case "for": el = ELBusFor.NEW().node(info).toELWrapper();break;
            case "while": el = ELBusWhile.NEW().node(info).toELWrapper();break;
            case "iterator": el = ELBusIterator.NEW().node(info).toELWrapper();break;
            case "break": el = ELBusBreak.NEW().node(info).toELWrapper();break;
            case "script":
                if(isSimple) {
                    el = ELBusNode.NEW().node(info).toELWrapper();
                }else{
                    el = ELBusThen.NEW().node(info).toELWrapper();
                }
                break;
            case "switch_script": el = ELBusSwitch.NEW().node(info).toELWrapper();break;
            case "if_script": el = ELBusIf.NEW().node(info).toELWrapper();break;
            case "for_script": el = ELBusFor.NEW().node(info).toELWrapper();break;
            case "while_script": el = ELBusWhile.NEW().node(info).toELWrapper();break;
            case "break_script": el = ELBusBreak.NEW().node(info).toELWrapper();break;
            case "fallback":
            default:
        }
        return el;
    }

    public static String toEL(IvyCmp info, boolean format){
        return toELWrapper(info).toEL(format);
    }

    public static void handler(IvyCmp info) {
        if(StrUtil.isEmpty(info.getScript())){info.setScript(null);}
        if(StrUtil.isEmpty(info.getLanguage())){info.setLanguage(null);}
        if(StrUtil.isEmpty(info.getClazz())){info.setClazz(null);}
        if(StrUtil.isEmpty(info.getCmpPre())){info.setCmpPre(null);}
        if(StrUtil.isEmpty(info.getCmpFinallyOpt())){info.setCmpFinallyOpt(null);}
        if(StrUtil.isEmpty(info.getCmpId())){info.setCmpId(null);}
        if(StrUtil.isEmpty(info.getCmpTag())){info.setCmpTag(null);}
        if(StrUtil.isEmpty(info.getCmpTo())){info.setCmpTo(null);}
        if(StrUtil.isEmpty(info.getCmpDefaultOpt())){info.setCmpDefaultOpt(null);}
        if(StrUtil.isEmpty(info.getCmpTrueOpt())){info.setCmpTrueOpt(null);}
        if(StrUtil.isEmpty(info.getCmpFalseOpt())){info.setCmpFalseOpt(null);}
        if(StrUtil.isEmpty(info.getCmpDoOpt())){info.setCmpDoOpt(null);}
        if(StrUtil.isEmpty(info.getCmpBreakOpt())){info.setCmpBreakOpt(null);}
        if(StrUtil.isEmpty(info.getCmpDataName())){info.setCmpDataName(null);}
        if(StrUtil.isEmpty(info.getCmpData())){info.setCmpData(null);}
    }

}
