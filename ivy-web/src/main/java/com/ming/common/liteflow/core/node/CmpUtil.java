package com.ming.common.liteflow.core.node;

public class CmpUtil {

//    public static LiteFlowNodeBuilder getLiteFlowNodeBuilder(NodeInfo info){
//        String code = info.getNodeType().getCode();
//        switch (code){
//            case "switch": return LiteFlowNodeBuilder.createSwitchNode();
//            case "if": return LiteFlowNodeBuilder.createIfNode();
//            case "for": return LiteFlowNodeBuilder.createForNode();
//            case "while": return LiteFlowNodeBuilder.createWhileNode();
//            case "break": return LiteFlowNodeBuilder.createBreakNode();
//            case "iterator": return LiteFlowNodeBuilder.createIteratorNode();
//            case "script": return LiteFlowNodeBuilder.createScriptNode();
//            case "switch_script": return LiteFlowNodeBuilder.createScriptSwitchNode();
//            case "if_script": return LiteFlowNodeBuilder.createScriptIfNode();
//            case "for_script": return LiteFlowNodeBuilder.createScriptForNode();
//            case "while_script": return LiteFlowNodeBuilder.createScriptWhileNode();
//            case "break_script": return LiteFlowNodeBuilder.createScriptBreakNode();
//            case "fallback": return null;//每种组件类型 只能定义一个降级组件
//            default: return LiteFlowNodeBuilder.createCommonNode();
//        }
//    }

//    public static LiteFlowNodeBuilder createNode(NodeInfo info){
//        LiteFlowNodeBuilder builder = getLiteFlowNodeBuilder(info);
//        if(builder != null){
//            builder.setId(info.getComponentId());
//            builder.setName(info.getComponentName());
//        }
//        return builder;
//    }
//
//    //构建一个普通组件
//    public static void createCommonNode(NodeInfo info){
//        addNode(info);
//    }
//
//    public static void createSwitchNode(NodeInfo info){
//        LiteFlowNodeBuilder.createSwitchNode().setId(info.getComponentId())
//                .setName(info.getComponentName())
//                .setClazz(info.getClazz())
//                .build();
//    }
//
//    public static void createIfNode(String componentId){
//
//    }
//
//    public static void createForNode(String componentId){
//
//    }
//
//    public static void createWhileNode(String componentId){
//
//    }
//
//    public static void createBreakNode(String componentId){
//
//    }
//
//    private static void addNode(NodeInfo info) {
//        checkBuild(info);
//        try {
//            if (info.getNodeType().isScript()) {
//                FlowBus.addScriptNode(info.getComponentId(), info.getComponentName(), info.getNodeType(), info.getScript(), info.getLanguage());
//            } else {
//                addNode(info.getComponentId(), info.getComponentName(), info.getNodeType(), info.getClazz(), (String)null, (String)null);
//            }
//        } catch (Exception var3) {
//            String errMsg = StrUtil.format("An exception occurred while building the node[{}],{}", new Object[]{info.getComponentId(), var3.getMessage()});
//            System.out.println(errMsg+ var3);
//            throw new NodeBuildException(errMsg);
//        }
//
//    }
//
//    private static void addNode(String nodeId, String name, NodeTypeEnum type, Class<?> cmpClazz, String script, String language) {
//        try {
//            // 判断此类是否是声明式的组件，如果是声明式的组件，就用动态代理生成实例
//            // 如果不是声明式的，就用传统的方式进行判断
//            List<NodeComponent> cmpInstances = new ArrayList<>();
//            if (/*LiteFlowProxyUtil.isDeclareCmp(cmpClazz)*/true) {
//                // 这里的逻辑要仔细看下
//                // 如果是spring体系，把原始的类往spring上下文中进行注册，那么会走到ComponentScanner中
//                // 由于ComponentScanner中已经对原始类进行了动态代理，出来的对象已经变成了动态代理类，所以这时候的bean已经是NodeComponent的子类了
//                // 所以spring体系下，无需再对这个bean做二次代理
//                // 但是在非spring体系下，这个bean依旧是原来那个bean，所以需要对这个bean做一次代理
//                // 这里用ContextAware的spi机制来判断是否spring体系
//                ContextAware contextAware = ContextAwareHolder.loadContextAware();
//                Object bean = ContextAwareHolder.loadContextAware().registerBean(nodeId, cmpClazz);
//                if (LocalContextAware.class.isAssignableFrom(contextAware.getClass())) {
//                    cmpInstances = LiteFlowProxyUtil.proxy2NodeComponent(bean, nodeId);
//                }
//                else {
//                    cmpInstances = ListUtil.toList((NodeComponent) bean);
//                }
//            }
//            else {
//                // 以node方式配置，本质上是为了适配无spring的环境，如果有spring环境，其实不用这么配置
//                // 这里的逻辑是判断是否能从spring上下文中取到，如果没有spring，则就是new instance了
//                // 如果是script类型的节点，因为class只有一个，所以也不能注册进spring上下文，注册的时候需要new Instance
//                if (!type.isScript()) {
//                    cmpInstances = ListUtil
//                            .toList((NodeComponent) ContextAwareHolder.loadContextAware().registerOrGet(nodeId, cmpClazz));
//                }
//                // 去除null元素
//                cmpInstances.remove(null);
//                // 如果为空
//                if (cmpInstances.isEmpty()) {
//                    NodeComponent cmpInstance = (NodeComponent) cmpClazz.newInstance();
//                    cmpInstances.add(cmpInstance);
//                }
//            }
//            // 进行初始化component
//            cmpInstances = cmpInstances.stream()
//                    .map(cmpInstance -> ComponentInitializer.loadInstance()
//                            .initComponent(cmpInstance, type, name,
//                                    cmpInstance.getNodeId() == null ? nodeId : cmpInstance.getNodeId()))
//                    .collect(Collectors.toList());
//
//            // 初始化Node，把component放到Node里去
//            List<Node> nodes = cmpInstances.stream().map(Node::new).collect(Collectors.toList());
//
//            for (int i = 0; i < nodes.size(); i++) {
//                Node node = nodes.get(i);
//                NodeComponent cmpInstance = cmpInstances.get(i);
//                // 如果是脚本节点，则还要加载script脚本
//                if (type.isScript()) {
//                    if (StrUtil.isNotBlank(script)) {
//                        node.setScript(script);
//                        node.setLanguage(language);
//                        ((ScriptComponent) cmpInstance).loadScript(script, language);
//                    }
//                    else {
//                        String errorMsg = StrUtil.format("script for node[{}] is empty", nodeId);
//                        throw new ScriptLoadException(errorMsg);
//                    }
//                }
//
//                String activeNodeId = StrUtil.isEmpty(cmpInstance.getNodeId()) ? nodeId : cmpInstance.getNodeId();
//                FlowBus.getNodeMap().put(activeNodeId, node);
//                addFallbackNode(node);
//            }
//
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//            String error = StrUtil.format("component[{}] register error",
//                    StrUtil.isEmpty(name) ? nodeId : StrUtil.format("{}({})", nodeId, name));
//            System.out.println(e.getMessage());
//            throw new ComponentCannotRegisterException(StrUtil.format("{} {}", error, e.getMessage()));
//        }
//    }
//
//    private static void addFallbackNode(Node node) {
//        NodeComponent nodeComponent = node.getInstance();
//        FallbackCmp fallbackCmp = (FallbackCmp) AnnoUtil.getAnnotation(nodeComponent.getClass(), FallbackCmp.class);
//        if (fallbackCmp != null) {
//            NodeTypeEnum nodeType = node.getType();
//            Node fallBackNode = FlowBus.getFallBackNode(nodeType);
//            fallBackNode.setId(node.getId());
//            fallBackNode.setTag(node.getTag());
//            fallBackNode.setName(node.getName());
//            fallBackNode.setType(node.getType());
//            fallBackNode.setInstance(node.getInstance());
//            fallBackNode.setScript(node.getScript());
//            fallBackNode.setClazz(node.getClazz());
//            fallBackNode.setCmpData(node.getCmpData());
//            fallBackNode.setCurrChainId(node.getCurrChainId());
//            fallBackNode.setLoopIndex(node.getLoopIndex());
//            fallBackNode.setCurrLoopObject(node.getCurrLoopObject());
//            fallBackNode.setLanguage(node.getLanguage());
//        }
//    }
//
//    private static void checkBuild(NodeInfo nodeInfo) {
//        List<String> errorList = new ArrayList();
//        if (StrUtil.isBlank(nodeInfo.getComponentId())) {
//            errorList.add("id is blank");
//        }
//        if (Objects.isNull(nodeInfo.getNodeType())) {
//            errorList.add("type is null");
//        }
//        if (CollUtil.isNotEmpty(errorList)) {
//            throw new NodeBuildException(CollUtil.join(errorList, ",", "[", "]"));
//        }
//    }
}
