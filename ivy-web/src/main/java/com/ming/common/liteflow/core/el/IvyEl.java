package com.ming.common.liteflow.core.el;

import com.ming.core.anno.Describe;
import com.ming.core.generate.template.annotation.Generate;
import com.ming.core.generate.template.annotation.database.Column;
import com.ming.core.generate.template.annotation.database.PrimaryKey;
import com.ming.core.generate.template.annotation.database.Text;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

@Data
@Table(name = "ivy_el")
@Generate(isEffective = true, moduleName = "db", desc = "EL表达式信息")
public class IvyEl {

    @Column
    @PrimaryKey
    private Long id;

    @Column
    @Describe(value = "EL表达式ID")
    private String elId;

    @Column
    @Describe(value = "EL表达式名称")
    private String elName;

    @Column
    @Describe(value = "Executor执行器ID")
    private Long executorId;

    @Text
    @Column
    @Describe(value = "EL表达式")
    private String el;

    @Text
    @Column
    @Describe(value = "流程数据")
    private String flowJson;

    @Text
    @Column
    @Describe(value = "原始数据")
    private String sourceJson;

}
