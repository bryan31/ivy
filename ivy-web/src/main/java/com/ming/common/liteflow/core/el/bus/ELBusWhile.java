package com.ming.common.liteflow.core.el.bus;

import cn.hutool.core.util.StrUtil;
import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.ELWrapper;
import com.yomahub.liteflow.builder.el.WhileELWrapper;

public class ELBusWhile {

    private ELWrapper wrapper;

    public static ELBusWhile NEW(){
        return new ELBusWhile();
    }

    public ELBusWhile node(IvyCmp info){
        Object doOpt = info.getCmpDoOptEL() != null ? info.getCmpDoOptEL() : info.getCmpDoOpt();
        Object breakOpt = info.getCmpBreakOptEL() != null ? info.getCmpBreakOptEL() : info.getCmpBreakOpt();
        WhileELWrapper whileELWrapper = ELBus.whileOpt(ELBusNode.NEW().node(info).toELWrapper());
        if(StrUtil.isNotBlank(info.getCmpId())){
            whileELWrapper.id(info.getCmpId());
        }
        if(StrUtil.isNotBlank(info.getCmpTag())){
            whileELWrapper.tag(info.getCmpTag());
        }
        if(info.getCmpParallel() != null){
            whileELWrapper.parallel(info.getCmpParallel());
        }
        if(doOpt != null){
            whileELWrapper.doOpt(doOpt);
        }
        if(breakOpt != null){
            whileELWrapper.breakOpt(breakOpt);
        }
        if(info.getCmpMaxWaitSeconds() != null){
            whileELWrapper.maxWaitSeconds(info.getCmpMaxWaitSeconds());
        }
        this.wrapper = whileELWrapper;
        return this;
    }

    public String toEL(){
        return wrapper.toEL();
    }

    public String toEL(boolean format){
        return wrapper.toEL(format);
    }

    public ELWrapper toELWrapper(){
        return wrapper;
    }

}
