package com.ming.common.liteflow.core.graph;

import com.ming.common.liteflow.core.el.prop.*;
import lombok.Data;

@Data
public class NodeProperties {
    // 定义节点和边的属性，根据实际情况添加字段
    private Long id;
    private String componentId;
    private String componentName;

    private ForProperties forProperties;
    private WhileProperties whileProperties;
    private IteratorProperties iteratorProperties;
    private IfProperties ifProperties;
    private SwitchProperties switchProperties;

}
