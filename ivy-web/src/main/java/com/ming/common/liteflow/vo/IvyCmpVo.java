package com.ming.common.liteflow.vo;

import com.ivy.builder.graph.IvyCmp;
import com.ming.core.query.Options;
import lombok.Data;

@Data
public class IvyCmpVo extends IvyCmp {

    private Options options;

}
