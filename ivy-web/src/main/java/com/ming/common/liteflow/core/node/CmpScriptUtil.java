package com.ming.common.liteflow.core.node;

import cn.hutool.core.util.StrUtil;
import com.ivy.builder.graph.IvyCmp;
import com.ming.common.liteflow.core.enums.LiteFlowEnums;
import com.yomahub.liteflow.builder.LiteFlowNodeBuilder;
import com.yomahub.liteflow.enums.NodeTypeEnum;

public class CmpScriptUtil {

    public static LiteFlowNodeBuilder getLiteFlowNodeBuilder(String type){
        if(type == null){
            type = "";
        }
        switch (type){
            case "switch": return LiteFlowNodeBuilder.createSwitchNode();
            case "if": return LiteFlowNodeBuilder.createIfNode();
            case "for": return LiteFlowNodeBuilder.createForNode();
            case "while": return LiteFlowNodeBuilder.createWhileNode();
            case "break": return LiteFlowNodeBuilder.createBreakNode();
            case "iterator": return LiteFlowNodeBuilder.createIteratorNode();
            case "script": return LiteFlowNodeBuilder.createScriptNode();
            case "switch_script": return LiteFlowNodeBuilder.createScriptSwitchNode();
            case "if_script": return LiteFlowNodeBuilder.createScriptIfNode();
            case "for_script": return LiteFlowNodeBuilder.createScriptForNode();
            case "while_script": return LiteFlowNodeBuilder.createScriptWhileNode();
            case "break_script": return LiteFlowNodeBuilder.createScriptBreakNode();
            case "fallback": return null;//每种组件类型 只能定义一个降级组件
            default: return LiteFlowNodeBuilder.createCommonNode();
        }
    }

    public static LiteFlowEnums.SCRIPT_JAVA getScriptEnum(String type){
        if(type == null){
            type = "";
        }
        switch (type){
            case "switch": return LiteFlowEnums.SCRIPT_JAVA.switch_script;
            case "if": return LiteFlowEnums.SCRIPT_JAVA.if_script;
            case "for": return LiteFlowEnums.SCRIPT_JAVA.for_script;
            case "while": return LiteFlowEnums.SCRIPT_JAVA.while_script;
            case "break": return LiteFlowEnums.SCRIPT_JAVA.break_script;
            case "iterator": return null;
            case "script": return LiteFlowEnums.SCRIPT_JAVA.script;
            case "switch_script": return LiteFlowEnums.SCRIPT_JAVA.switch_script;
            case "if_script": return LiteFlowEnums.SCRIPT_JAVA.if_script;
            case "for_script": return LiteFlowEnums.SCRIPT_JAVA.for_script;
            case "while_script": return LiteFlowEnums.SCRIPT_JAVA.while_script;
            case "break_script": return LiteFlowEnums.SCRIPT_JAVA.break_script;
            case "fallback": return null;//每种组件类型 只能定义一个降级组件
            default: return LiteFlowEnums.SCRIPT_JAVA.script;
        }
    }

    public static void createNode(String componentId){
        IvyCmp nodeInfo = new IvyCmp();
        nodeInfo.setComponentId(componentId);
        nodeInfo.setComponentName(componentId);
        nodeInfo.setType(NodeTypeEnum.SCRIPT.getCode());
        nodeInfo.setLanguage("java");
        createNode(nodeInfo, LiteFlowEnums.SCRIPT_JAVA.script);
    }

    public static void createNode(IvyCmp nodeInfo, LiteFlowEnums.SCRIPT_JAVA script){
        LiteFlowNodeBuilder builder = getLiteFlowNodeBuilder(nodeInfo.getType());
        if(builder != null){
            createNode(builder, nodeInfo, script);
        }else{
            builder = getLiteFlowNodeBuilder(nodeInfo.getFallbackType());
            if(builder != null){
                createNode(builder, nodeInfo, script);
            }
        }
    }
    public static void createNode(IvyCmp nodeInfo){
        LiteFlowNodeBuilder builder = getLiteFlowNodeBuilder(nodeInfo.getType());
        if(builder != null){
            createNode(builder, nodeInfo);
        }else{
            builder = getLiteFlowNodeBuilder(nodeInfo.getFallbackType());
            if(builder != null){
                createNode(builder, nodeInfo);
            }
        }
    }

    public static void createNode(LiteFlowNodeBuilder builder, IvyCmp nodeInfo){
        if(StrUtil.isNotBlank(nodeInfo.getClazz())){
            builder.setId(nodeInfo.getComponentId())
                    .setName(nodeInfo.getComponentName())
                    .setClazz(nodeInfo.getClazz())
                    .build();
        }else{
            LiteFlowEnums.SCRIPT_JAVA script = getScriptEnum(nodeInfo.getType());
            createNode(builder, nodeInfo, script);
        }
    }

    public static void createNode(LiteFlowNodeBuilder builder, IvyCmp nodeInfo, LiteFlowEnums.SCRIPT_JAVA script){
        builder.setId(nodeInfo.getComponentId())
                .setName(nodeInfo.getComponentName())
                .setLanguage(nodeInfo.getLanguage() == null ? script.getLanguage() : nodeInfo.getLanguage())
                .setClazz(nodeInfo.getClazz())
                .setScript(nodeInfo.getScript() == null ? script.getScriptStr() : nodeInfo.getScript())
                .setType(script.getNodeTypeEnum())
                .build();
    }
}
