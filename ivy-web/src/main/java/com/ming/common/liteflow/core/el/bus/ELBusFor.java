package com.ming.common.liteflow.core.el.bus;

import cn.hutool.core.util.StrUtil;
import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.ELWrapper;
import com.yomahub.liteflow.builder.el.ForELWrapper;

public class ELBusFor {

    private ELWrapper wrapper;

    public static ELBusFor NEW(){
        return new ELBusFor();
    }

    public ELBusFor node(IvyCmp info){
        Object doOpt = info.getCmpDoOptEL() != null ? info.getCmpDoOptEL() : info.getCmpDoOpt();
        Object breakOpt = info.getCmpBreakOptEL() != null ? info.getCmpBreakOptEL() : info.getCmpBreakOpt();
        ForELWrapper forELWrapper = ELBus.forOpt(ELBusNode.NEW().node(info).toELWrapper());
        if(StrUtil.isNotBlank(info.getCmpId())){
            forELWrapper.id(info.getCmpId());
        }
        if(StrUtil.isNotBlank(info.getCmpTag())){
            forELWrapper.tag(info.getCmpTag());
        }
        if(info.getCmpParallel() != null){
            forELWrapper.parallel(info.getCmpParallel());
        }
        if(doOpt != null){
            forELWrapper.doOpt(doOpt);
        }
        if(breakOpt != null){
            forELWrapper.breakOpt(breakOpt);
        }
        if(info.getCmpMaxWaitSeconds() != null){
            forELWrapper.maxWaitSeconds(info.getCmpMaxWaitSeconds());
        }
        this.wrapper = forELWrapper;
        return this;
    }

    public String toEL(){
        return wrapper.toEL();
    }

    public String toEL(boolean format){
        return wrapper.toEL(format);
    }

    public ELWrapper toELWrapper(){
        return wrapper;
    }

}
