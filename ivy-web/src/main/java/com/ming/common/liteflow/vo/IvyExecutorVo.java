package com.ming.common.liteflow.vo;

import com.ming.core.query.Options;
import com.ming.core.liteflow.entity.IvyExecutor;
import lombok.Data;

@Data
public class IvyExecutorVo extends IvyExecutor {

    private Options options;

}
