package com.ming.common.liteflow.core.task;

import com.ming.core.anno.Describe;
import com.ming.core.generate.template.annotation.Generate;
import com.ming.core.generate.template.annotation.database.Column;
import com.ming.core.generate.template.annotation.database.PrimaryKey;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;

@Data
@Table(name = "ivy_task")
@Generate(isEffective = true, moduleName = "db", desc = "任务")
public class IvyTask {

    @Column
    @PrimaryKey
    private Long id;

    @Column
    @Describe(value = "任务ID")
    private String taskId;

    @Column
    @Describe(value = "任务名称")
    private String taskName;

}
