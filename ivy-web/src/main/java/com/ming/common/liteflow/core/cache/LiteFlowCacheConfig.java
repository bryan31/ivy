//package com.ming.common.liteflow.core.cache;
//
//import com.github.benmanes.caffeine.cache.Cache;
//import com.github.benmanes.caffeine.cache.Caffeine;
//import org.beetl.sql.core.SQLManager;
//import org.checkerframework.checker.nullness.qual.NonNull;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import javax.annotation.Resource;
//import java.util.concurrent.TimeUnit;
//
//@Configuration
//public class LiteFlowCacheConfig {
//
//    @Resource
//    private SQLManager sqlManager;
//
//    @Bean
//    public @NonNull Cache<Object, Object> caffeineConfig() {
//        return Caffeine.newBuilder().initialCapacity(10).expireAfterWrite(30, TimeUnit.MINUTES).build();
//    }
//
//}
