package com.ming.common.liteflow.core.el.bus;

import cn.hutool.core.util.StrUtil;
import com.ivy.builder.graph.IvyCmp;
import com.yomahub.liteflow.builder.el.ELBus;
import com.yomahub.liteflow.builder.el.ELWrapper;
import com.yomahub.liteflow.builder.el.IteratorELWrapper;

public class ELBusIterator {

    private ELWrapper wrapper;

    public static ELBusIterator NEW(){
        return new ELBusIterator();
    }

    public ELBusIterator node(IvyCmp info){
        Object doOpt = info.getCmpDoOptEL() != null ? info.getCmpDoOptEL() : info.getCmpDoOpt();
        IteratorELWrapper iteratorELWrapper = ELBus.iteratorOpt(ELBusNode.NEW().node(info).toELWrapper());
        if(StrUtil.isNotEmpty(info.getCmpId())){
            iteratorELWrapper.id(info.getCmpId());
        }
        if(StrUtil.isNotEmpty(info.getCmpTag())){
            iteratorELWrapper.tag(info.getCmpTag());
        }
        if(info.getCmpParallel() != null){
            iteratorELWrapper.parallel(info.getCmpParallel());
        }
        if(doOpt != null){
            iteratorELWrapper.doOpt(doOpt);
        }
        if(info.getCmpMaxWaitSeconds() != null){
            iteratorELWrapper.maxWaitSeconds(info.getCmpMaxWaitSeconds());
        }
        this.wrapper = iteratorELWrapper;
        return this;
    }

    public String toEL(){
        return wrapper.toEL();
    }

    public String toEL(boolean format){
        return wrapper.toEL(format);
    }

    public ELWrapper toELWrapper(){
        return wrapper;
    }

}
