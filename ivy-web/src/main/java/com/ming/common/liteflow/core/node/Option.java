package com.ming.common.liteflow.core.node;

import lombok.Data;

@Data
public class Option {

    private String title;

    private String value;

    public Option() { }

    public Option(String title, String value) {
        this.title = title;
        this.value = value;
    }

}
