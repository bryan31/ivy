package com.ming.common.liteflow.core.enums;

import com.ming.common.beetl.enums.core.IDictItem;
import com.ming.common.beetl.enums.core.StaticDictPool;
import com.ming.common.util.TemplateUtils;
import com.ming.core.util.NumberUtil;
import com.yomahub.liteflow.enums.NodeTypeEnum;

import java.util.HashMap;
import java.util.Map;

public interface LiteFlowEnums {
    public static final String script_regex = "\\{\\{(.*?)\\}\\}";
    public static final String script_template = "import com.yomahub.liteflow.script.ScriptExecuteWrap;\n" +
            "import com.yomahub.liteflow.script.body.{{interfaceImpl}};\n" +
            "\n" +
            "public class ScriptNode{{index}} implements {{interfaceImpl}} {\n" +
            "    @Override\n" +
            "    public {{returnType}} body(ScriptExecuteWrap scriptExecuteWrap) {\n" +
            "        return {{returnVal}};\n" +
            "    }\n" +
            "}";

    enum SCRIPT_JAVA implements IDictItem {
        script("script","普通脚本","java",NodeTypeEnum.SCRIPT,"JaninoCommonScriptBody","Void","null"),
        switch_script("switch_script","选择脚本","java",NodeTypeEnum.SWITCH_SCRIPT,"JaninoSwitchScriptBody","String",null),
        if_script("if_script","条件脚本","java",NodeTypeEnum.IF_SCRIPT,"JaninoIfScriptBody","Boolean","true"),
        for_script("for_script","次数循环脚本","java",NodeTypeEnum.FOR_SCRIPT,"JaninoForScriptBody","Integer","5"),
        while_script("while_script","条件循环脚本","java",NodeTypeEnum.WHILE_SCRIPT,"JaninoWhileScriptBody","Boolean","true"),
        break_script("break_script","退出循环脚本","java",NodeTypeEnum.BREAK_SCRIPT,"JaninoBreakScriptBody","Boolean","true"),
        ;

        private String language;
        private NodeTypeEnum nodeTypeEnum;
        private String interfaceImpl;
        private String returnType;
        private String returnVal;

        SCRIPT_JAVA(String value, String label, String language,NodeTypeEnum nodeTypeEnum, String interfaceImpl, String returnType, String returnVal){
            this.language = language;
            this.nodeTypeEnum = nodeTypeEnum;
            this.interfaceImpl = interfaceImpl;
            this.returnType = returnType;
            this.returnVal = returnVal;
            StaticDictPool.putDictItem(this,value,label);
        }

        public String getLanguage() {
            return language;
        }

        public String getInterfaceImpl() {
            return interfaceImpl;
        }

        public String getReturnType() {
            return returnType;
        }

        public String getReturnVal() {
            return returnVal;
        }

        public NodeTypeEnum getNodeTypeEnum() {
            return nodeTypeEnum;
        }

        public String getScriptStr(String val) {
            Map<String,String> params = new HashMap<>();
            params.put("index",NumberUtil.getNextIndex()+"");
            params.put("interfaceImpl", this.getInterfaceImpl());
            params.put("returnType", this.getReturnType());
            params.put("returnVal", val);
            return TemplateUtils.NEW().template(script_template).regex(script_regex).params(params).compile();
        }

        public String getScriptStr() {
            Map<String,String> params = new HashMap<>();
            params.put("index",NumberUtil.getNextIndex()+"");
            params.put("interfaceImpl", this.getInterfaceImpl());
            params.put("returnType", this.getReturnType());
            params.put("returnVal", this.getReturnVal());
            return TemplateUtils.NEW().template(script_template).regex(script_regex).params(params).compile();
        }
    }


//    enum NODE_TYPE implements IDictItem {
//        common_node("common_node","普通组件"),
//        switch_node("switch_node","选择脚本"),
//        if_node("if_node","条件脚本"),
//        for_node("for_node","次数循环脚本"),
//        while_node("while_node","条件循环脚本"),
//        break_node("break_node","退出循环脚本"),
//        ;
//
//        NODE_TYPE(String value, String label){
//            StaticDictPool.putDictItem(this,value,label);
//        }
//
//    }

}
