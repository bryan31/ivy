package com.ming;

import com.ming.common.beetl.util.BeetlSqlUtil;
import com.ming.common.util.FileUtil;
import com.ming.core.generate.template.enable.EnableGenerate;
import com.ming.common.ivy.IvyDict;
import org.beetl.sql.core.SQLManager;
import org.python.util.PythonInterpreter;
import org.redisson.spring.starter.RedissonAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.loader.jar.JarFile;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Enumeration;
import java.util.Properties;
import java.util.jar.JarEntry;

@EnableGenerate(basePackages = {"com.ming"})
@ComponentScans(@ComponentScan(basePackages = {"com"}))
@SpringBootApplication(exclude = {RedissonAutoConfiguration.class})
public class IvyApplication {

    public static void main(String[] args) {
        SpringApplication.run(IvyApplication.class, args);
        // Jython环境初始化
        jython();

        // 创建一个关闭钩子
        //Thread shutdownHook = new Thread(() -> {
            // 在 JVM 即将关闭时执行的操作
            // destroyClass(SpringBeanUtil.getBean("sqlManager"));
        //});
        // 将关闭钩子添加到 JVM
        //Runtime.getRuntime().addShutdownHook(shutdownHook);
        // 模拟 JVM 关闭
        // simulateJvmShutdown();


    }

    private static void jython(){
        SQLManager defaultSQLManager = BeetlSqlUtil.NEW().getDefaultSQLManager();
        IvyDict ivyDict = defaultSQLManager.lambdaQuery(IvyDict.class).andEq(IvyDict::getCode,"ivy_jython_path").single();
        if(ivyDict != null){
            Properties props = new Properties();
            Properties preprops = System.getProperties();
            props.put("python.home", ivyDict.getValue());//jython安装目录
            PythonInterpreter.initialize (preprops, props, new String[]{});
        }else{
            Properties props = new Properties();
            Properties preprops = System.getProperties();
            String path = IvyApplication.class.getClassLoader().getResource("lib/jython-installer-2.7.3.jar").getPath();

            String homePath = FileUtil.getRootPath(path);
            try {
                String jarPath = jythonJar(homePath);
                if(jarPath != null){
                    props.put("python.home", jarPath);//jython安装目录
                }else{
                    props.put("python.home", homePath);//jython安装目录
                }
                PythonInterpreter.initialize (preprops, props, new String[]{});
            } catch (IOException e) {
                throw new RuntimeException(e);
            }


        }
    }

    private static String jythonJar(String path) throws IOException {
        String jarPath = null;
        String nestedJarPath = null;
        if(path.contains("!/")){
            String[] split = path.split("!/");
            jarPath = split[0];
            nestedJarPath = split[1]+"/"+split[2];

            JarFile jarFile = new JarFile(new File(jarPath));
            Enumeration<JarEntry> entries = jarFile.entries();

            while (entries.hasMoreElements()) {
                java.util.jar.JarEntry entry = entries.nextElement();
                if (entry.getName().equals(nestedJarPath)) {
                    Path outputPath = Paths.get("temp", entry.getName());
                    try {
                        Files.createDirectories(outputPath.getParent());
                        Files.copy(jarFile.getInputStream(entry), outputPath);
                        return outputPath.toAbsolutePath().toString();
                    } catch (FileAlreadyExistsException e) {
                        return outputPath.toAbsolutePath().toString();
                    }

                }
            }
        }
        return null;
    }

    private static void simulateJvmShutdown() {
        // 模拟 JVM 关闭前的清理工作
        // 例如：关闭数据库连接、保存状态等

        // JVM 关闭
        System.exit(0);
    }

}
