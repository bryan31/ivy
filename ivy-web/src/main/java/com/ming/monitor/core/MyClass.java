package com.ming.monitor.core;//package com.ming.monitor.core;
//
//public class MyClass {
//
//    public void myMethod() {
//        System.out.println("Executing myMethod");
//    }
//
//    public static void main(String[] args) throws InstantiationException, IllegalAccessException {
//        // 使用 Byte Buddy 创建一个代理对象
//        MyClass proxy = IvyMethodMonitorAspect.monitor(MyClass.class);
//        // 调用代理对象的方法，实际上会调用被监控的方法
//        //proxy.myMethod();
//        Class<? extends MyClass> aClass = proxy.getClass();
//        MyClass myClass = aClass.newInstance();
//        myClass.myMethod();
//    }
//}