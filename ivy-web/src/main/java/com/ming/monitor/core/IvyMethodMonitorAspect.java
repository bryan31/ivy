package com.ming.monitor.core;//package com.ming.monitor.core;
//
//import net.bytebuddy.ByteBuddy;
//import net.bytebuddy.implementation.MethodDelegation;
//import net.bytebuddy.implementation.bind.annotation.AllArguments;
//import net.bytebuddy.implementation.bind.annotation.Origin;
//import net.bytebuddy.implementation.bind.annotation.RuntimeType;
//import net.bytebuddy.implementation.bind.annotation.SuperCall;
//import java.lang.reflect.Method;
//import java.util.concurrent.Callable;
//
//import static net.bytebuddy.matcher.ElementMatchers.isDeclaredBy;
//
//public class IvyMethodMonitorAspect {
//
//    public static <T> T monitor(Class<T> clazz) {
//        try {
//            return new ByteBuddy()
//                    .subclass(clazz)
//                    .method(isDeclaredBy(clazz))
//                    .intercept(MethodDelegation.to(IvyMethodMonitorAspect.class))
//                    .make()
//                    .load(clazz.getClassLoader())
//                    .getLoaded()
//                    .newInstance();
//        } catch (InstantiationException | IllegalAccessException e) {
//            throw new RuntimeException("Failed to create proxy", e);
//        }
//    }
//
//    @RuntimeType
//    public static Object intercept(@Origin Method method, @AllArguments Object[] args, @SuperCall Callable<Object> callable) throws Exception {
//        // 在方法调用前插入监控代码
//        System.out.println("Method " + method.getName() + " is about to be called");
//
//        try {
//            // 调用原始方法
//            return callable.call();
//        } finally {
//            // 在方法调用后插入监控代码
//            System.out.println("Method " + method.getName() + " has been called");
//        }
//    }
//}
