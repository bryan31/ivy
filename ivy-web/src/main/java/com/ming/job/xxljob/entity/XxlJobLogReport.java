package com.ming.job.xxljob.entity;

import org.beetl.sql.annotation.entity.Table;

import java.util.Date;

@Table(name = "xxl_job_log_report")
public class XxlJobLogReport {

    private Integer id;

    private Date triggerDay;

    private Integer runningCount;
    private Integer sucCount;
    private Integer failCount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getTriggerDay() {
        return triggerDay;
    }

    public void setTriggerDay(Date triggerDay) {
        this.triggerDay = triggerDay;
    }

    public int getRunningCount() {
        return runningCount;
    }

    public void setRunningCount(int runningCount) {
        this.runningCount = runningCount;
    }

    public int getSucCount() {
        return sucCount;
    }

    public void setSucCount(int sucCount) {
        this.sucCount = sucCount;
    }

    public int getFailCount() {
        return failCount;
    }

    public void setFailCount(int failCount) {
        this.failCount = failCount;
    }
}
