package com.ming.job.xxljob.vo;

import com.ming.core.query.Options;
import com.ming.job.xxljob.entity.XxlJobLog;
import lombok.Data;

import java.util.Date;

@Data
public class XxlJobLogVo extends XxlJobLog {

    private Options options;
    private Integer logStatus;// -1全部，1成功，2失败，3进行中
    private String filterTime;
    private Integer fromLineNum;// 日志行号

    private Date startDate;
    private Date endDate;

}
