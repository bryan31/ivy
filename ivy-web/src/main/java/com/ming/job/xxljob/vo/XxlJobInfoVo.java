package com.ming.job.xxljob.vo;

import com.ming.core.query.Options;
import com.ming.job.xxljob.entity.XxlJobInfo;
import lombok.Data;

@Data
public class XxlJobInfoVo extends XxlJobInfo {

    private Options options;

    private String addressList;

}
