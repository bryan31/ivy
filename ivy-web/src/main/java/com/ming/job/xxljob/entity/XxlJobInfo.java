package com.ming.job.xxljob.entity;

import com.ming.core.anno.Describe;
import com.ming.core.anno.DescribeItem;
import lombok.Data;
import org.beetl.sql.annotation.entity.Table;
import org.beetl.sql.fetch.annotation.Fetch;
import org.beetl.sql.fetch.annotation.FetchSql;

import java.util.Date;
import java.util.List;

@Data
@Fetch(level = 2)
@Table(name = "xxl_job_info")
public class XxlJobInfo {
	
	private Integer id;				// 主键ID
	
	private Integer jobGroup;		// 执行器主键ID
	private String jobDesc;    // 任务名称
	
	private Date addTime;
	private Date updateTime;
	
	private String author;		// 负责人
	private String alarmEmail;	// 报警邮件

	@Describe(value = "调度类型",items = {
		@DescribeItem(value = "NONE",desc = "无"),
		@DescribeItem(value = "CRON",desc = "CRON"),
		@DescribeItem(value = "FIX_RATE",desc = "固定速度"),
	})
	private String scheduleType;			// 调度类型
	private String scheduleConf;			// 调度配置，值含义取决于调度类型

	@Describe(value = "调度过期策略",items = {
		@DescribeItem(value = "DO_NOTHING",desc = "忽略"),
		@DescribeItem(value = "FIRE_ONCE_NOW",desc = "立即执行一次"),
	})
	private String misfireStrategy;

	@Describe(value = "路由策略",items = {
		@DescribeItem(value = "FIRST",desc = "第一个"),
		@DescribeItem(value = "LAST",desc = "最后一个"),
		@DescribeItem(value = "ROUND",desc = "轮询"),
		@DescribeItem(value = "RANDOM",desc = "随机"),
		@DescribeItem(value = "CONSISTENT_HASH",desc = "一致性HASH"),
		@DescribeItem(value = "LEAST_FREQUENTLY_USED",desc = "最不经常使用"),
		@DescribeItem(value = "LEAST_RECENTLY_USED",desc = "最近最久未使用"),
		@DescribeItem(value = "FAILOVER",desc = "故障转移"),
		@DescribeItem(value = "BUSYOVER",desc = "忙碌转移"),
		@DescribeItem(value = "SHARDING_BROADCAST",desc = "分片广播"),
	})
	private String executorRouteStrategy;	// 执行器路由策略
	private String executorHandler;		    // 执行器，任务Handler名称
	private String executorParam;		    // 执行器，任务参数
	private String chainId;		    // ivy_chain表的chain_id字段

	@Describe(value = "阻塞处理策略",items = {
		@DescribeItem(value = "SERIAL_EXECUTION",desc = "单机串行"),
		@DescribeItem(value = "DISCARD_LATER",desc = "丢弃后续调度"),
		@DescribeItem(value = "COVER_EARLY",desc = "覆盖之前调度"),
	})
	private String executorBlockStrategy;	// 阻塞处理策略
	private int executorTimeout;     		// 任务执行超时时间，单位秒
	private int executorFailRetryCount;		// 失败重试次数

	@Describe(value = "GLUE类型",items = {
		@DescribeItem(value = "LITE_FLOW",desc = "LITE_FLOW"),
		@DescribeItem(value = "BEAN",desc = "BEAN"),
		@DescribeItem(value = "GLUE_GROOVY",desc = "GLUE(Java)"),
		@DescribeItem(value = "GLUE_SHELL",desc = "GLUE(Shell)"),
		@DescribeItem(value = "GLUE_PYTHON",desc = "GLUE(Python)"),
		@DescribeItem(value = "GLUE_PHP",desc = "GLUE(PHP)"),
		@DescribeItem(value = "GLUE_NODEJS",desc = "GLUE(Nodejs)"),
		@DescribeItem(value = "GLUE_POWERSHELL",desc = "GLUE(PowerShell)"),
	})
	private String glueType;		// GLUE类型	#com.xxl.job.core.glue.GlueTypeEnum
	private String glueSource;		// GLUE源代码
	private String glueRemark;		// GLUE备注
	private Date glueUpdatetime;	// GLUE更新时间

	private String childJobId;		// 子任务ID，多个逗号分隔

	private Integer triggerStatus;		// 调度状态：0-停止，1-运行
	private long triggerLastTime;	// 上次调度时间
	private long triggerNextTime;	// 下次调度时间

	@FetchSql("select title,address_list from xxl_job_group where id =#{jobGroup}")
	private XxlJobGroup xxlJobGroup;

	private List<XxlJobLogGlue> xxlJobLogGlueList;
}
