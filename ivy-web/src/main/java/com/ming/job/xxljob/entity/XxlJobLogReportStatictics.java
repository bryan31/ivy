package com.ming.job.xxljob.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class XxlJobLogReportStatictics {

    private String icon = "tabler-user";
    private String color;
    private String title;
    private String subtitle;
    private String stats;
    private String percentage;//增长

    public XxlJobLogReportStatictics(String color, String title, String subtitle, String stats) {
        this.color = color;
        this.title = title;
        this.subtitle = subtitle;
        this.stats = stats;
    }
}
