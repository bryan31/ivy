package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeIfComponent;

@LiteflowComponent("IvyDynamicCmpIf")
public class IvyDynamicCmpIf extends NodeIfComponent {

    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpIf.class);

    @Override
    public boolean processIf() throws Exception {
        LOG.info("IvyDynamicCmpIf executed!");
        System.out.println("IvyDynamicCmpIf executed!");
        return true;
    }
}