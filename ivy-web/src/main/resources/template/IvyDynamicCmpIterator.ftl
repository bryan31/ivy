package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeIteratorComponent;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@LiteflowComponent("IvyDynamicCmpIterator")
public class IvyDynamicCmpIterator extends NodeIteratorComponent {

    private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpIterator.class);

    @Override
    public Iterator<?> processIterator() throws Exception {
        LOG.info("IvyDynamicCmpIterator executed!");
        System.out.println("IvyDynamicCmpIterator executed!");
        List<String> list = new ArrayList<String>(){{
            add("jack");add("mary");add("tom");
        }};
        return list.iterator();
    }
}