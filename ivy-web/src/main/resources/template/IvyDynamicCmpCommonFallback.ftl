package com.ivy.cla;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.yomahub.liteflow.annotation.FallbackCmp;
import com.yomahub.liteflow.annotation.LiteflowComponent;
import com.yomahub.liteflow.core.NodeComponent;

@FallbackCmp
@LiteflowComponent("IvyDynamicCmpCommonFallback")
public class IvyDynamicCmpCommonFallback extends NodeComponent {

	private static final Logger LOG = LoggerFactory.getLogger(IvyDynamicCmpCommonFallback.class);

	@Override
	public void process() {
		LOG.info("IvyDynamicCmpCommonFallback executed!");
		System.out.println("IvyDynamicCmpCommonFallback executed!");
	}

}
